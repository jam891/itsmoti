//
//  Endpoint.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 10/11/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Moya

let Provider = MoyaProvider<Endpoint>()

enum Endpoint {
    case signIn(username: String, password: String)
    case signUp(username: String, email: String, phoneNumber: String, password: String)
    case usernameAvailable(username: String)
    case resetPassword(email: String)
    case registerDevice(deviceToken: String)
    
    case moties
    
    case create(moti: Moti)
    case update(moti: Moti)
    case delete(moti: Moti)
    
    case search(query: String)
    case share(moti: Moti, users: [User])
    
    case shared
    case appendShared(moties: [MotiModel])
    case removeShared(moti: MotiModel)
    
    case clearAll
    
    case subscription
    case confirmation(payment: String)
}

extension Endpoint: TargetType {
    var baseURL: URL { return URL(string: "https://itsmoti.com/index.php?option=com_api")! }
    var path: String { return "" }
    var method: Method { return .post }
    var headers: [String: String]? {
        return nil
    }
    var sampleData: Data { return "".data(using: .utf8)! }
    var task: Task {
        var parameters: [String: Any] = [:]
        switch self {
        case let .signIn(username, password):
            parameters = [
                "task"    : "user.login",
                "username": username,
                "password": password,
            ]
        case let .signUp(username, email, phoneNumber, password):
            parameters = [
                "task"       : "user.register",
                "username"   : username,
                "password"   : password,
                "email"      : email,
                "phoneNumber": phoneNumber
            ]
        case let .usernameAvailable(username):
            parameters = [
                "task"  : "user.validation",
                "field" : username
            ]
        case .shared:
            parameters = [
                "task"  : "moti.getShared",
                "accessToken": User.accessToken ?? ""
            ]
        case let .registerDevice(deviceToken):
            parameters = [
                "task"  : "device.register",
                "deviceToken": deviceToken,
                "deviceType" : "iOS",
                "accessToken": User.accessToken ?? ""
            ]
        case let .resetPassword(email):
            parameters = [
                "task"  : "user.resetPassword",
                "email" : email
            ]
        case let .search(query):
            parameters = [
                "task"  : "user.searchUsers",
                "search": query,
                "accessToken": User.accessToken ?? ""
            ]
        case let .share(moti, users):
            let userIds = users.map { $0.id }.joined(separator: ", ")
            
            print("user ids", userIds)
            
            parameters = [
                "task" : "moti.share",
                "motiId": moti.id ?? "",
                "userIds": userIds,
                "accessToken": User.accessToken ?? ""
            ]
        case .subscription:
            parameters = [
                "task": "moti.subscription",
                "accessToken": User.accessToken ?? ""
            ]
        case let .confirmation(payment):
            parameters = [
                "task": "subscription.purchase",
                "payment": payment,
                "accessToken": User.accessToken ?? ""
            ]
        case let .appendShared(moties):
            let motiIds = moties.map { $0.id }.joined(separator: ", ")
            
            print("moti ids", motiIds)
            
            parameters = [
                "task" : "moti.appendShared",
                "motiIds": motiIds,
                "accessToken": User.accessToken ?? ""
            ]
        case let .removeShared(moti):
            parameters = [
                "task"   : "moti.removeShared",
                "motiIds": moti.id,
                "accessToken": User.accessToken ?? ""
            ]
        case .clearAll:
            parameters = [
                "task": "moti.deleteAll",
                "accessToken": User.accessToken ?? ""
            ]
        case .moties:
            parameters = [
                "task": "moti.getMoties",
                "accessToken": User.accessToken ?? ""
            ]
        case let .delete(moti):
            parameters = [
                "task"  : "moti.delete",
                "motiId": moti.id!,
                "accessToken": User.accessToken ?? "",
                "deviceToken": User.deviceToken ?? ""
            ]
        case let .update(moti):
            let multipartFormData = multipartForm(for: moti)
            return .uploadCompositeMultipart(multipartFormData, urlParameters: ["task": "moti.update"])
        case let .create(moti):
            let multipartFormData = multipartForm(for: moti)
            return .uploadCompositeMultipart(multipartFormData, urlParameters: ["task": "moti.create"])
    }
        return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
}

// MARK: - Private

private extension Endpoint {
    
    func multipartForm(for moti: Moti) -> [MultipartFormData] {
        var multipartFormData = [
            MultipartFormData(provider: .data((moti.id ?? "").data(using: .utf8)!), name: "id"),
            MultipartFormData(provider: .data((moti.title ?? "").data(using: .utf8)!), name: "title"),
            MultipartFormData(provider: .data((moti.body ?? "").data(using: .utf8)!), name: "body"),
            
            MultipartFormData(provider: .data(moti.startDate!.dataValue), name: "startDate"),
            MultipartFormData(provider: .data((moti.recurrenceRule ?? "").data(using: .utf8)!), name: "recurrenceRule"),
            MultipartFormData(provider: .data(moti.created!.dataValue), name: "created"),
            MultipartFormData(provider: .data(moti.modified!.dataValue), name: "modified"),
            
            MultipartFormData(provider: .data((moti.snooze ?? "").data(using: .utf8)!), name: "snooze"),
            
            MultipartFormData(provider: .data((moti.location?.title ?? "").data(using: .utf8)!), name: "location[title]"),
            MultipartFormData(provider: .data((moti.location?.subtitle ?? "").data(using: .utf8)!), name: "location[subtitle]"),
            
            MultipartFormData(provider: .data(User.accessToken.data(using: .utf8)!), name: "accessToken"),
            MultipartFormData(provider: .data(User.deviceToken.data(using: .utf8)!), name: "deviceToken")
        ]
      
        var index: Int = 0
        moti.todoList!.forEach {
            if let todo = $0 as? Todo {
                let name       = "todoList[\(index)][name]"
                let createDate = "todoList[\(index)][createDate]"
                let completed  = "todoList[\(index)][completed]"
                
                multipartFormData.append(MultipartFormData(provider: .data(todo.name!.data(using: .utf8)!), name: name))
                multipartFormData.append(MultipartFormData(provider: .data(todo.createDate!.dataValue), name: createDate))
                multipartFormData.append(MultipartFormData(provider: .data(todo.completed.dataValue), name: completed))
            }
            index += 1
        }
        
        if let media = moti.media {
            if media.isUpdated || media.isInserted {
                multipartFormData.append(MultipartFormData(provider: .data(media.type!.data(using: .utf8)!), name: "media[type]"))
                
                if let resource = media.resource {
                    var mimeType: String {
                        switch media.type! {
                        case MediaType.video.rawValue: return "video/mp4"
                        case MediaType.photo.rawValue: return "image/jpeg"
                        default:                       return "audio/m4a"
                        }
                    }
                    
                    if let type = media.type, let directory = Directory(rawValue: type) {
                        if let url = AppManager.shared.url(for: resource, from: directory) {
                            do {
                                let data = try Data(contentsOf: url)
                                multipartFormData.append(MultipartFormData(provider: .data(data),
                                                                           name: "resource",
                                                                           fileName: resource,
                                                                           mimeType: mimeType))
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    }
                }
                
                if let thumbnail = media.thumbnail {
                    let mimeType = "image/jpeg"
                    if let url = AppManager.shared.url(for: thumbnail, from: .photo) {
                        do {
                            let data = try Data(contentsOf: url)
                            multipartFormData.append(MultipartFormData(provider: .data(data),
                                                                       name: "thumbnail",
                                                                       fileName: thumbnail,
                                                                       mimeType: mimeType))
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                }
            }
        }
        return multipartFormData
    }

}
