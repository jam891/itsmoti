//
//  API.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct API {
    typealias auth = Auth
    typealias post = Post
    
    private init() {}
}
