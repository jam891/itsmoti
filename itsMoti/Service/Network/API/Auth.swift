//
//  Auth.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/7/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct Auth {
    private init() {}
}

extension Auth {
    
    static func signIn(username: String, password: String, _ callback: @escaping (_ error: String?) -> Void) {
        Provider.request(.signIn(username: username, password: password)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    print(try! response.mapJSON())
                    User.accessToken = try! JSONDecoder().decode(Token.self, from: response.data).accessToken
                    User.current = try! JSONDecoder().decode(User.self, from: response.data)
                    callback(nil)
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error.localizedDescription)
                }
            case let .failure(error):
                callback(error.localizedDescription)
            }
        }
    }
    
    static func signUp(username: String, password: String, email: String, phoneNumber: String, _ callback: @escaping (_ error: String?) -> Void) {
        Provider.request(.signUp(username: username, email: email, phoneNumber: phoneNumber, password: password)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    print(try! response.mapJSON())
                    callback(nil)
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error.localizedDescription)
                }
            case let .failure(error):
                callback(error.localizedDescription)
            }
        }
    }
    
    static func resetPassword(_ email: String, _ callback: @escaping (_ error: String?) -> Void) {
        Provider.request(.resetPassword(email: email)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    callback(nil)
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error.localizedDescription)
                }
            case let .failure(error):
                callback(error.localizedDescription)
            }
        }
    }
    
    static func validate(_ field: String, _ callback: @escaping (_ success: Bool, _ error: String?) -> Void) {
        Provider.request(.usernameAvailable(username: field)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    callback(true, nil)
                default:
                    callback(false, nil)
                }
            case let .failure(error):
                callback(false, error.localizedDescription)
            }
        }
    }
    
}
