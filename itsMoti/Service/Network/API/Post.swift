//
//  Post.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/7/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct Post {
    private init() {}
}

extension Post {
    
    static func moties(_ callback: @escaping (_ moties: [MotiModel], _ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.moties) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    do {
                        print("\nmoties response:", try! response.mapJSON())
                        
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .formatted(.iso8601)
                        let moties = try decoder.decode([MotiModel].self, from: response.data)
                       
                        callback(moties, nil)
                    } catch {
                        callback([], error)
                    }
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback([], error)
                }
            case let .failure(error):
                callback([], error)
            }
        }
    }
    
    static func create(_ moti: Moti, _ callback: @escaping (_ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.create(moti: moti)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    print("\nresponse:", try! response.mapJSON())
                    
                    let dict = try! JSONSerialization.jsonObject(with: response.data, options: []) as? [String: Any]
                    let id = dict!["motiId"] as! Int
                    moti.id = String(id)
                    
                    callback(nil)
                default:
                    print("response", response)
                    
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error)
                }
            case let .failure(error):
                callback(error)
            }
        }
    }
    
    static func update(_ moti: Moti, _ callback: @escaping (_ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.update(moti: moti)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    
                    callback(nil)
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error)
                }
            case let .failure(error):
                callback(error)
            }
        }
    }
    
    static func delete(_ moti: Moti, _ callback: @escaping (_ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.delete(moti: moti)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    StorageManager.shared.delete(moti)
                    callback(nil)
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error)
                }
            case let .failure(error):
                callback(error)
            }
        }
    }
    
    static func register(_ deviceToken: String, _ callback: @escaping (_ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.registerDevice(deviceToken: deviceToken)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    callback(nil)
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error)
                }
            case let .failure(error):
                callback(error)
            }
        }
    }
    
}

// MARK: - Share

extension Post {
    
    static func search(_ query: String, _ callback: @escaping (_ users: [User], _ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.search(query: query)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    do {
                        let users = try JSONDecoder().decode([User].self, from: response.data)
                        callback(users, nil)
                    } catch {
                        print(error.localizedDescription)
                        callback([], error)
                    }

                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback([], error)
                }
            case let .failure(error):
                callback([], error)
            }
        }
    }
    
    static func share(_ moti: Moti, for users: [User], _ callback: @escaping (_ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.share(moti: moti, users: users)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    
                    print("\nshare response:", try! response.mapJSON())
                        
                    callback(nil)
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error)
                }
            case let .failure(error):
                callback(error)
            }
        }
    }

    static func shared(_ callback: @escaping (_ moties: [MotiModel], _ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.shared) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    do {
                        print("\nshared response:", try! response.mapJSON())
                        
                        let decoder = JSONDecoder()
                        decoder.dateDecodingStrategy = .formatted(.iso8601)
                        let moties = try decoder.decode([MotiModel].self, from: response.data)
                        
                        callback(moties, nil)
                    } catch {
                        callback([], error)
                    }
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback([], error)
                }
            case let .failure(error):
                callback([], error)
            }
        }
    }
    
    static func removeShared(_ moti: MotiModel, _ callback: @escaping (_ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.removeShared(moti: moti)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    do {
                        print("\nremoveShared response:", try response.mapJSON())
                        callback(nil)
                    } catch {
                        callback(error)
                    }
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error)
                }
            case let .failure(error):
                callback(error)
            }
        }
    }
    
    static func appendShared(_ moties: [MotiModel], _ callback: @escaping (_ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.appendShared(moties: moties)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    do {
                        print("\nresponse:", try response.mapJSON())
                        
                        callback(nil)
                    } catch {
                        callback(error)
                    }
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error)
                }
            case let .failure(error):
                callback(error)
            }
        }
    }
    
}

// MARK: - Payment

extension Post {
    
    static func subscription(_ callback: @escaping (_ subscription: SubscriptionModel?, _ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.subscription) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    do {
                        print("\nsubscription response:", try! response.mapJSON())
                        
                        let subscription = try JSONDecoder().decode(SubscriptionModel.self, from: response.data)
                        
                        callback(subscription, nil)
                    } catch {
                        callback(nil, error)
                    }
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(nil, error)
                }
            case let .failure(error):
                callback(nil, error)
            }
        }
    }
    
    static func confirmation(_ payment: String, _ callback: @escaping (_ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.confirmation(payment: payment)) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    do {
                        print("\nconfirmation response:", try response.mapJSON())
                        
                        callback(nil)
                    } catch {
                        callback(error)
                    }
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error)
                }
            case let .failure(error):
                callback(error)
            }
        }
    }
    
}


// MARK: - Testing

extension Post {
    
    static func clearAll(_ callback: @escaping (_ error: Error?) -> Void) {
        print(#function)
        
        Provider.request(.clearAll) { result in
            switch result {
            case let .success(response):
                switch response.statusCode {
                case 200...299:
                    callback(nil)
                default:
                    let error = try! JSONDecoder().decode(APIError.self, from: response.data)
                    callback(error)
                }
            case let .failure(error):
                callback(error)
            }
        }
    }
    
}
