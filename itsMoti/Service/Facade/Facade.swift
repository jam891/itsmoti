//
//  Facade.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 10/15/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class Facade {
    private static let shared = Facade()
    private init() {}
}

// MARK: - Representable

extension Facade {
    
    static func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) -> Bool {
        return Facade.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    static func applicationWillEnterForeground(_ application: UIApplication) {
        NetworkManager.shared.applicationWillEnterForeground(application)
    }
    
    static func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        NetworkManager.shared.applicationDidBecomeActive(application)
    }
    
    static func applicationWillTerminate(_ application: UIApplication) {
        StorageManager.shared.applicationWillTerminate(application)
    }
    
    static func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return Facade.shared.application(application, supportedInterfaceOrientationsFor: window)
    }
    
    static func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        PushManager.shared.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    
    static func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        PushManager.shared.application(application, didFailToRegisterForRemoteNotificationsWithError: error)
    }
    
    static func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        PushManager.shared.application(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
    }
    
    static func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        DownloadManager.shared.application(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }

}

private extension Facade {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) -> Bool {
        RunLoop.current.run(until: Date(timeIntervalSinceNow: 1))
        Theme.default.apply()
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        if let rootViewController = topViewController(with: window?.rootViewController) {
            if rootViewController.responds(to: Selector(("canRotate"))) {
                return .allButUpsideDown
            }
        }
        return .portrait
    }
    
}

// MARK: -

private extension Facade {
    private func topViewController(with rootViewController: UIViewController!) -> UIViewController? {
        guard rootViewController != nil else { return nil }
        if rootViewController.isKind(of: UITabBarController.self) {
            return topViewController(with: (rootViewController as! UITabBarController).selectedViewController)
        } else if rootViewController.isKind(of: UINavigationController.self) {
            return topViewController(with: (rootViewController as! UINavigationController).visibleViewController)
        } else if rootViewController.presentedViewController != nil {
            return topViewController(with: rootViewController.presentedViewController)
        }
        return rootViewController
    }
}
