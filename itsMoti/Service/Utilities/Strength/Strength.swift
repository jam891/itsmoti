//
//  Strength.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

enum Strength: CustomStringConvertible, Comparable {
    case empty
    case veryWeak
    case weak
    case reasonable
    case strong
    case veryStrong
    
    var intValue: Int {
        switch self {
        case .empty:      return -1
        case .veryWeak:   return 0
        case .weak:       return 1
        case .reasonable: return 2
        case .strong:     return 3
        case .veryStrong: return 4
        }
    }
    
    var description: String {
        switch self {
        case .empty:
            return NSLocalizedString("Empty", tableName: "Moti", comment: "Empty")
        case .veryWeak:
            return NSLocalizedString("Very weak password", tableName: "Moti", comment: "very weak password")
        case .weak:
            return NSLocalizedString("Weak password", tableName: "Moti", comment: "weak password")
        case .reasonable:
            return NSLocalizedString("Reasonable password", tableName: "Moti", comment: "reasonable password")
        case .strong:
            return NSLocalizedString("Strong password", tableName: "Moti", comment: "strong password")
        case .veryStrong:
            return NSLocalizedString("Very strong password", tableName: "Moti", comment: "very strong password")
        }
    }
    
    init(password: String) {
        self.init(entropy: Strength.entropy(password))
    }
    
    private init(entropy: Float) {
        switch entropy {
        case -(Float.greatestFiniteMagnitude) ..< 0.0:
            self = .empty
        case 0.0 ..< 28.0:
            self = .veryWeak
        case 28.0 ..< 36.0:
            self = .weak
        case 36.0 ..< 60.0:
            self = .reasonable
        case 60.0 ..< 128.0:
            self = .strong
        default:
            self = .veryStrong
        }
    }
    
    private static func entropy(_ string: String) -> Float {
        guard !string.isEmpty else {
            return -Float.greatestFiniteMagnitude
        }
        
        var includesLowercaseCharacter    = false
        var includesUppercaseCharacter    = false
        var includesDecimalDigitCharacter = false
        var includesPunctuationCharacter  = false
        var includesSymbolCharacter       = false
        var includesWhitespaceCharacter   = false
        var includesNonBaseCharacter      = false
        
        var sizeOfCharacters: UInt = 0
        
        string.utf16.forEach { c in
            if !includesLowercaseCharacter && CharacterSet.lowercaseLetters.contains(UnicodeScalar(c)!) {
                includesLowercaseCharacter = true
                sizeOfCharacters += 26
            }
            if !includesUppercaseCharacter && CharacterSet.uppercaseLetters.contains(UnicodeScalar(c)!) {
                includesUppercaseCharacter = true
                sizeOfCharacters += 26
            }
            if !includesDecimalDigitCharacter && CharacterSet.decimalDigits.contains(UnicodeScalar(c)!) {
                includesDecimalDigitCharacter = true
                sizeOfCharacters += 10
            }
            if !includesPunctuationCharacter && CharacterSet.punctuationCharacters.contains(UnicodeScalar(c)!) {
                includesPunctuationCharacter = true
                sizeOfCharacters += 20
            }
            if !includesSymbolCharacter && CharacterSet.symbols.contains(UnicodeScalar(c)!) {
                includesSymbolCharacter = true
                sizeOfCharacters += 10
            }
            if !includesWhitespaceCharacter && CharacterSet.whitespaces.contains(UnicodeScalar(c)!) {
                includesWhitespaceCharacter = true
                sizeOfCharacters += 1
            }
            if !includesNonBaseCharacter && CharacterSet.nonBaseCharacters.contains(UnicodeScalar(c)!) {
                includesNonBaseCharacter = true
                sizeOfCharacters += 32 + 128
            }
        }
        let entropyPerCharacter = log2(Float(sizeOfCharacters))
        return entropyPerCharacter * Float(string.utf16.count)
    }
    
}

func <(lhs: Strength, rhs: Strength) -> Bool {
    return lhs.intValue < rhs.intValue
}
