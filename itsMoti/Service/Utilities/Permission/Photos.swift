//
//  Photos.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Photos

extension Permission {
    var statusPhotos: PermissionStatus {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:          return .authorized
        case .denied, .restricted: return .denied
        case .notDetermined:       return .notDetermined
        }
    }
    
    func requestPhotos(_ callback: @escaping Callback) {
        PHPhotoLibrary.requestAuthorization { _ in
            callback(self.statusPhotos)
        }
    }
}
