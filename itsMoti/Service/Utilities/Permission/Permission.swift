//
//  Permission.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class Permission: NSObject {
    typealias Callback = (PermissionStatus) -> Void
    
    static let contacts          = Permission(type: .contacts)
    static let locationWhenInUse = Permission(type: .locationWhenInUse)
    static let microphone        = Permission(type: .microphone)
    static let camera            = Permission(type: .camera)
    static let photos            = Permission(type: .photos)
    static let events            = Permission(type: .events)
    static let mediaLibrary      = Permission(type: .mediaLibrary)
    
    let type: PermissionType
    
    var status: PermissionStatus {
        if case .contacts          = type { return statusContacts }
        if case .locationWhenInUse = type { return statusLocationWhenInUse }
        if case .microphone        = type { return statusMicrophone }
        if case .camera            = type { return statusCamera }
        if case .photos            = type { return statusPhotos }
        if case .events            = type { return statusEvents }
        if case .mediaLibrary      = type { return statusMediaLibrary }
        fatalError()
    }
    
    var presentPrePermissionAlert = false
    lazy var prePermissionAlert: PermissionAlert = {
        return PrePermissionAlert(permission: self)
    }()
    
    var presentDeniedAlert = true
    lazy var deniedAlert: PermissionAlert = {
        return DeniedAlert(permission: self)
    }()
   
    var presentDisabledAlert = true
    lazy var disabledAlert: PermissionAlert = {
        return DisabledAlert(permission: self)
    }()
    
    var callback: Callback?
    
    
    private init(type: PermissionType) {
        self.type = type
    }
    
    func request(_ callback: @escaping Callback) {
        self.callback = callback
        switch status {
        case .authorized:    callbacks(status)
        case .notDetermined: presentPrePermissionAlert ? prePermissionAlert.present() : requestAuthorization(callbacks)
        case .denied:        presentDeniedAlert ? deniedAlert.present() : callbacks(status)
        case .disabled:      presentDisabledAlert ? disabledAlert.present() : callbacks(status)
        }
    }
    
    func requestAuthorization(_ callback: @escaping Callback) {
        if case .contacts = type {
            requestContacts(callback)
            return
        }
        if case .locationWhenInUse = type {
            requestLocationWhenInUse(callback)
            return
        }
        if case .microphone = type {
            requestMicrophone(callback)
            return
        }
        if case .camera = type {
            requestCamera(callback)
            return
        }
        if case .photos = type {
            requestPhotos(callback)
            return
        }
        if case .events = type {
            requestEvents(callback)
            return
        }
        if case .mediaLibrary = type {
            requestMediaLibrary(callback)
            return
        }
        fatalError()
    }
    
    func callbacks(_ with: PermissionStatus) {
        DispatchQueue.main.async {
            self.callback?(self.status)
        }
    }
    
}

extension Permission {
    override var description: String {
        return type.description
    }
    override var debugDescription: String {
        return "\(type): \(status)"
    }
}


