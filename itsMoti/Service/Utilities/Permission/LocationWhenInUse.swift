//
//  LocationWhenInUse.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import CoreLocation

private let manager = CLLocationManager()
private var requestedLocation = false
private var triggerCallbacks  = false

extension Permission: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch (requestedLocation, triggerCallbacks) {
        case (true, false):
            triggerCallbacks = true
        case (true, true):
            requestedLocation = false
            triggerCallbacks  = false
            callbacks(self.status)
        default:
            break
        }
    }
}

extension CLLocationManager {
    func request(_ permission: Permission) {
        delegate = permission
        
        requestedLocation = true
        
        if case .locationWhenInUse = permission.type {
            requestWhenInUseAuthorization()
            return
        }
    }
}

extension Permission {
    var statusLocationWhenInUse: PermissionStatus {
        guard CLLocationManager.locationServicesEnabled() else { return .disabled }
        
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedWhenInUse, .authorizedAlways: return .authorized
        case .restricted, .denied:                    return .denied
        case .notDetermined:                          return .notDetermined
        }
    }
    
    func requestLocationWhenInUse(_ callback: Callback) {
        manager.request(self)
    }
}




