//
//  Microphone.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import AVFoundation

extension Permission {
    var statusMicrophone: PermissionStatus {
        let status = AVAudioSession.sharedInstance().recordPermission
        switch status {
        case AVAudioSession.RecordPermission.denied:  return .denied
        case AVAudioSession.RecordPermission.granted: return .authorized
        default:                                      return .notDetermined
        }
    }
    
    func requestMicrophone(_ callback: @escaping Callback) {
        AVAudioSession.sharedInstance().requestRecordPermission { _ in
            callback(self.statusMicrophone)
        }
    }
}

