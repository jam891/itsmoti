//
//  Contacts.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Contacts

extension Permission {
    var statusContacts: PermissionStatus {
        let status = CNContactStore.authorizationStatus(for: .contacts)
        switch status {
        case .authorized:          return .authorized
        case .restricted, .denied: return .denied
        case .notDetermined:       return .notDetermined
        }
    }
    
    func requestContacts(_ callback: @escaping Callback) {
        CNContactStore().requestAccess(for: .contacts) { _, _ in
            callback(self.statusContacts)
        }
    }
}
