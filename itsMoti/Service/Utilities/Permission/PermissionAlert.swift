//
//  PermissionAlert.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class PermissionAlert {
    let permission: Permission
    
    var status: PermissionStatus { return permission.status }
    var type: PermissionType { return permission.type }
    var callbacks: Permission.Callback { return permission.callbacks }
    
    var title: String?
    var message: String?
    
    var cancel: String? {
        get { return cancelActionTitle }
        set { cancelActionTitle = newValue }
    }
    
    var settings: String? {
        get { return defaultActionTitle }
        set { defaultActionTitle = newValue }
    }
    
    var confirm: String? {
        get { return defaultActionTitle }
        set { defaultActionTitle = newValue }
    }
    
    var cancelActionTitle: String?
    var defaultActionTitle: String?
    
    var alert: UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelActionTitle, style: .cancel, handler: cancelHandler))
        alert.view.layoutIfNeeded()
        return alert
    }
    
    init(permission: Permission) {
        self.permission = permission
    }
    
    func present() {
        DispatchQueue.main.async {
            UIApplication.shared.topViewController?.present(self.alert, animated: true)
        }
    }
    
    private func cancelHandler(_ action: UIAlertAction) {
        callbacks(status)
    }
}

class DisabledAlert: PermissionAlert {
    override init(permission: Permission) {
        super.init(permission: permission)
        title   = "\(permission) is currently disabled"
        message = "Please enable access to \(permission) in the Settings app."
        cancel  = "OK"
    }
}

class DeniedAlert: PermissionAlert {
    override var alert: UIAlertController {
        let alert = super.alert
        let action = UIAlertAction(title: defaultActionTitle, style: .default, handler: settings)
        alert.addAction(action)
        alert.view.layoutIfNeeded()
        alert.preferredAction = action
        return alert
    }
    
    override init(permission: Permission) {
        super.init(permission: permission)
        title    = "Permission for \(permission) was denied"
        message  = "Please enable access to \(permission) in the Settings app."
        cancel   = "Cancel"
        settings = "Settings"
    }
    
    @objc func settingsHandler() {
        NotificationCenter.removeObserver(self, name: UIApplication.didBecomeActiveNotification)
        callbacks(status)
    }
    
    private func settings(_ action: UIAlertAction) {
        NotificationCenter.addObserver(self, selector: #selector(settingsHandler), name: UIApplication.didBecomeActiveNotification)
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url)
        }
    }
}

class PrePermissionAlert: PermissionAlert {
    override var alert: UIAlertController {
        let alert = super.alert
        let action = UIAlertAction(title: defaultActionTitle, style: .default, handler: confirmHandler)
        alert.addAction(action)
        alert.view.layoutIfNeeded()
        alert.preferredAction = action
        return alert
    }

    override init(permission: Permission) {
        super.init(permission: permission)
        title   = "\(Bundle.main.name) would like to access your \(permission)"
        message = "Please enable access to \(permission)."
        cancel  = "Cancel"
        confirm = "Confirm"
    }

    private func confirmHandler(_ action: UIAlertAction) {
        permission.requestAuthorization(callbacks)
    }
}
