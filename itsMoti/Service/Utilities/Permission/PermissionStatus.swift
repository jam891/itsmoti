//
//  PermissionStatus.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

enum PermissionStatus: String {
    case authorized    = "Authorized"
    case denied        = "Denied"
    case disabled      = "Disabled"
    case notDetermined = "Not Determined"
    
    internal init?(string: String?) {
        guard let string = string else { return nil }
        self.init(rawValue: string)
    }
}

extension PermissionStatus: CustomStringConvertible {
    var description: String {
        return rawValue
    }
}
