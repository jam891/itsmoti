//
//  MediaLibrary.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import MediaPlayer

extension Permission {
    var statusMediaLibrary: PermissionStatus {
        let status = MPMediaLibrary.authorizationStatus()
        switch status {
        case .authorized:          return .authorized
        case .restricted, .denied: return .denied
        case .notDetermined:       return .notDetermined
        }
    }
    
    func requestMediaLibrary(_ callback: @escaping Callback) {
        MPMediaLibrary.requestAuthorization { _ in
            callback(self.statusMediaLibrary)
        }
    }
}
