//
//  Camera.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import AVFoundation

extension Permission {
    var statusCamera: PermissionStatus {
        let status = AVCaptureDevice.authorizationStatus(for: .video)
        switch status {
        case .authorized:          return .authorized
        case .restricted, .denied: return .denied
        case .notDetermined:       return .notDetermined
        }
    }
    
    func requestCamera(_ callback: @escaping Callback) {
        AVCaptureDevice.requestAccess(for: .video) { _ in
            callback(self.statusCamera)
        }
    }
}
