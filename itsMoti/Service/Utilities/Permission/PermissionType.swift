//
//  PermissionType.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

enum PermissionType {
    case contacts
    case locationWhenInUse
    case microphone
    case camera
    case photos
    case events
    case mediaLibrary
}

extension PermissionType: CustomStringConvertible {
    var description: String {
        if case .contacts          = self { return "Contacts" }
        if case .locationWhenInUse = self { return "Location" }
        if case .microphone        = self { return "Microphone" }
        if case .camera            = self { return "Camera" }
        if case .photos            = self { return "Photos" }
        if case .events            = self { return "Events" }
        if case .mediaLibrary      = self { return "Media Library" }
        fatalError()
    }
}
