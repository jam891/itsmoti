//
//  Events.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import EventKit

internal extension Permission {
    var statusEvents: PermissionStatus {
        let status = EKEventStore.authorizationStatus(for: .event)
        switch status {
        case .authorized:          return .authorized
        case .restricted, .denied: return .denied
        case .notDetermined:       return .notDetermined
        }
    }
    
    func requestEvents(_ callback: @escaping Callback) {
        EKEventStore().requestAccess(to: .event) { _, _ in
            callback(self.statusEvents)
        }
    }
}
