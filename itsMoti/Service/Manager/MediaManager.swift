//
//  MediaManager.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/5/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import AVKit

class MediaManager {
    static let shared = MediaManager()
    private init() {}
    
    private let watermarkText = "itsMoti"
    private let font = UIFont(name: "PoetsenOne-Regular", size: 40)!
}

extension MediaManager {
    
    func trimAudio(_ asset: AVAsset, startTime: CMTime, endTime: CMTime, _ progress: @escaping (_ value: Float) -> Void, completion: @escaping (_ outputURL: URL) -> Void) {
        guard let session = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetAppleM4A) else { return }
        session.timeRange = CMTimeRangeFromTimeToTime(start: startTime, end: endTime)
        session.outputFileType = .m4a
        session.outputURL = AppManager.shared.temporaryDirectory
            .appendingPathComponent(UUID().uuidString)
            .appendingPathExtension(.m4a)
        
        export(session: session, progress, completion: completion)
    }
    
    func trimVideo(_ asset: AVAsset, startTime: CMTime, endTime: CMTime, _ progress: @escaping (_ value: Float) -> Void, completion: @escaping (_ outputURL: URL) -> Void) {
        
        let mixComposition = AVMutableComposition()
        
        let videoTrack = mixComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        let audioTrack = mixComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let videoAssetTrack = asset.tracks(withMediaType: .video)[0]
        let audioAssetTrack = asset.tracks(withMediaType: .audio)[0]
        
        do {
            let range = CMTimeRange(start: .zero, duration: asset.duration)

            try videoTrack?.insertTimeRange(range, of: videoAssetTrack, at: .zero)
            try audioTrack?.insertTimeRange(range, of: audioAssetTrack, at: .zero)
            
        } catch {
            print(error.localizedDescription)
        }
        
        let mainInstruction = AVMutableVideoCompositionInstruction()
        mainInstruction.timeRange = CMTimeRange(start: .zero, duration: asset.duration)

        let videolayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack!)
        
        var isVideoAssetPortrait = false
        
        let videoTransform = videoAssetTrack.preferredTransform
        
        if videoTransform.a == 0 && videoTransform.b == 1 && videoTransform.c == -1 && videoTransform.d == 0 {
            isVideoAssetPortrait = true
        }
        if videoTransform.a == 0 && videoTransform.b == -1 && videoTransform.c == 1 && videoTransform.d == 0 {
            isVideoAssetPortrait = true
        }
        if videoTransform.a == 1 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == 1 {
            
        }
        if videoTransform.a == -1 && videoTransform.b == 0 && videoTransform.c == 0 && videoTransform.d == -1 {

        }
        
        videolayerInstruction.setTransform(videoAssetTrack.preferredTransform, at: .zero)
        videolayerInstruction.setOpacity(0, at: asset.duration)
        
        mainInstruction.layerInstructions = [videolayerInstruction]
        
        let mainCompositionInst = AVMutableVideoComposition()
        
        var naturalSize: CGSize
        if isVideoAssetPortrait {
            naturalSize = CGSize(width: videoAssetTrack.naturalSize.height, height: videoAssetTrack.naturalSize.width)
        } else {
            naturalSize = videoAssetTrack.naturalSize
        }
    
        let renderWidth  = naturalSize.width
        let renderHeight = naturalSize.height
        
        mainCompositionInst.renderSize = CGSize(width: renderWidth, height: renderHeight)
        mainCompositionInst.instructions = [mainInstruction]
        mainCompositionInst.frameDuration = CMTime(value: 1, timescale: 30)
        
        addWatermark(to: mainCompositionInst, size: naturalSize)
        
        guard let session = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality) else { return }
        session.timeRange = CMTimeRangeFromTimeToTime(start: startTime, end: endTime)
        session.outputFileType = .mov
        session.shouldOptimizeForNetworkUse = true
        session.videoComposition = mainCompositionInst
        session.outputURL = AppManager.shared.temporaryDirectory
            .appendingPathComponent(UUID().uuidString)
            .appendingPathExtension(.mov)
        
        export(session: session, progress, completion: completion)
    }
    
    func thumbnail(from asset: AVAsset, _ completion: @escaping (_ url: URL?) -> Void) {
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMakeWithSeconds(1, preferredTimescale: 600)
        
        do {
            let cgImage = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if let data = UIImage(cgImage: cgImage).jpegData(compressionQuality: 0.9) {
                let url = AppManager.shared.writeToTemporaryDirectoryImageData(data)
                completion(url)
            } else {
                completion(nil)
            }
        } catch {
            completion(nil)
        }
    }
    
    func addWatermark(to image: UIImage) -> UIImage {
        UIGraphicsBeginImageContext(image.size)
        let shadow = NSShadow()
        shadow.shadowBlurRadius = 5
        
        let attributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),
            .shadow: shadow,
            .font: font
        ]
        
        let textSize = watermarkText.size(withAttributes: attributes)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        
        let rect = CGRect(x: image.size.width - textSize.width - 30,
                          y: image.size.height - textSize.height - 30,
                          width: textSize.width, height: textSize.height)
        
        watermarkText.draw(in: rect, withAttributes: attributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}

private extension MediaManager {
    
    func export(session: AVAssetExportSession, _ progress: @escaping (_ value: Float) -> Void, completion: @escaping (_ outputURL: URL) -> Void) {
        print(#function)
        
        AppManager.shared.removeItem(atPath: session.outputURL!.path)
        
        let timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { timer in
            progress(session.progress)
        }
        
        session.exportAsynchronously {
            switch session.status {
            case .failed:
                print("failed error: \(String(describing: session.error))")
            case  .completed:
                print("completed")
                completion(session.outputURL!)
            default:
                return
            }
            timer.invalidate()
        }
    }
    
    func addWatermark(to composition: AVMutableVideoComposition, size: CGSize) {
        print(#function)
        
        let textLayer = CATextLayer()
        textLayer.font = font
        textLayer.frame = CGRect(x: 30, y: 0, width: size.width - 60, height: 60)
        textLayer.string = watermarkText
        textLayer.shadowOpacity = 0.5
        textLayer.alignmentMode = .right
        textLayer.foregroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        let overlayLayer = CALayer()
        overlayLayer.addSublayer(textLayer)
        overlayLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        overlayLayer.masksToBounds = true
        
        let parentLayer = CALayer()
        let videoLayer  = CALayer()
        parentLayer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        videoLayer.frame  = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        parentLayer.addSublayer(videoLayer)
        parentLayer.addSublayer(overlayLayer)
        
        let tool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: parentLayer)
        composition.animationTool = tool
    }
    
}



