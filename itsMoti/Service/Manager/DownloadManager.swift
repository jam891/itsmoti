//
//  DownloadManager.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 1/10/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class DownloadManager: NSObject {
    static let shared = DownloadManager()
    private override init() {}
    
    lazy var session: URLSession! = {
        let configuration = URLSessionConfiguration.background(withIdentifier: NSUUID().uuidString)
        return URLSession(configuration: configuration, delegate: self, delegateQueue: .main)
    }()
    
    var downloadTask: URLSessionDownloadTask!
    var downloadData: Data!
    var isDownload: Bool = false

    var onFinished: ((_ location: URL) -> Void)?
    var onProgress: ((_ progress: Float, _ totalSize: String) -> Void)?
    var backgroundSessionCompletionHandler: (() -> Void)?
}

extension DownloadManager {
    
    func download(url: URL, to directory: Directory, _ callback: @escaping (_ error: Error?) -> Void) {
        URLSession.shared.downloadTask(with: url) { location, response, error in
            if let error = error {
                callback(error)
            }
            if let location = location {
                let toPath = AppManager.shared
                    .url(for: directory)
                    .appendingPathComponent(url.lastPathComponent).path
                AppManager.shared.moveItem(atPath: location.path, toPath: toPath)
                callback(nil)
            }
            }.resume()
    }
    
    func startDownload(_ url: URL, _ callback: @escaping (_ video: Video?, _ error: Error?) -> Void) {
        if downloadTask != nil {
            cancel()
        }
        
        ParseManager.shared.parse(url: url) { video, error in
            if let error = error {
                callback(nil, error)
            } else {
                callback(video, nil)
                self.downloadTask = self.session.downloadTask(with: video!.url)
                self.downloadTask.resume()
                self.isDownload = true
            }
        }
    }
    
    func cancel() {
        downloadTask?.cancel()
        downloadData = nil
        isDownload = false
        downloadTask = nil
    }
    
    func resume() {
        if downloadTask != nil && isDownload {
            downloadTask!.cancel(byProducingResumeData: { resumeData in
                if let resumeData = resumeData {
                    self.downloadData = resumeData
                }
            })
            isDownload = false
            downloadTask = nil
        }
        
        if !isDownload && downloadData != nil {
            downloadTask = session?.downloadTask(withResumeData: downloadData!)
            downloadTask!.resume()
            isDownload = true
            downloadData = nil
        }
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        backgroundSessionCompletionHandler = completionHandler
    }
    
}

// MARK: - URLSessionDownloadDelegate

extension DownloadManager: URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        if totalBytesExpectedToWrite > 0 {
            let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
            let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite, countStyle: .file)
            
            onProgress?(progress, totalSize)
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        onFinished?(location)
    }
    
}

// MARK: - URLSessionDelegate

extension DownloadManager: URLSessionDelegate {
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            if let completionHandler = DownloadManager.shared.backgroundSessionCompletionHandler {
                DownloadManager.shared.backgroundSessionCompletionHandler = nil
                DispatchQueue.main.async(execute: {
                    completionHandler()
                })
            }
        }
    }
    
}
