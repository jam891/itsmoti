//
//  NetworkManager.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/17/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class NetworkManager {
    static let shared = NetworkManager()
    private init() {}
    
    let reachability = Reachability()!
}

// MARK: - Notification

@objc
extension NetworkManager {
    func networkStatusChanged(_ notification: Notification) {

    }
}

// MARK: - 

extension NetworkManager {
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print(#function)
        stopMonitoring()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        print(#function)
        startMonitoring()
    }
    
}

// MARK: - Listener

extension NetworkManager {
    
    func startMonitoring() {
        NotificationCenter.addObserver(self, selector: #selector(networkStatusChanged), name: .reachabilityChangedNotification, object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func stopMonitoring() {
        reachability.stopNotifier()
        NotificationCenter.removeObserver(self, name: .reachabilityChangedNotification, object: reachability)
    }
    
}

