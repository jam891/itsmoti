//
//  ParseManager.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 1/10/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import Foundation
import XCDYouTubeKit
import Fuzi

class ParseManager {
    static let shared = ParseManager()
    private init() {}
}

extension ParseManager {
    
    func parse(url: URL, _ callback: @escaping (_ video: Video?, _ error: Error?) -> Void) {
        if url.absoluteString.range(of: "instagram") != nil {
            ParseManager.shared.parseInstagramURL(url, callback)
        }
        if url.absoluteString.range(of: "youtu.be") != nil || url.absoluteString.range(of: "youtube") != nil {
            ParseManager.shared.parseYoutubeURL(url, callback)
        }
    }
    
}

// MARK: - Private

private extension ParseManager {
    
    func parseYoutubeURL(_ url: URL, _ callback: @escaping (_ video: Video?, _ error: Error?) -> Void) {
        XCDYouTubeClient.default().getVideoWithIdentifier(String(url.absoluteString.suffix(11))) { video, error in
            if let error = error {
                callback(nil, error)
            } else {
                if let video = video {
                    let title = video.title
                    let url = ParseManager.shared.url(for: video)!
                    callback(Video(url: url, title: title), nil)
                } else {
                    callback(nil, nil)
                }
            }
        }
    }
    
    func parseInstagramURL(_ url: URL, _ callback: (_ video: Video?, _ error: Error?) -> Void) {
        if let data = try? Data(contentsOf: url), let doc = try? HTMLDocument(data: data) {
            let items = doc.xpath("//meta[@property='og:video']/@content")
            if let item = items.first {
                let titleItem = doc.xpath("//meta[@property='og:title']/@content")
                if let title = titleItem.first?.stringValue, let url = URL(string: item.stringValue) {
                    callback(Video(url: url, title: title), nil)
                } else {
                    callback(nil, nil)
                }
            } else {
                callback(nil, nil)
            }
        } else {
            callback(nil, nil)
        }
    }
    
}

private extension ParseManager {
    
    func url(for video: XCDYouTubeVideo?) -> URL? {
        guard let video = video else { return nil }
        
        var urlString: String!
        let streamURLs = NSDictionary(dictionary: video.streamURLs)
        if let hd720 = streamURLs[XCDYouTubeVideoQuality.HD720.rawValue] as? URL {
            urlString = hd720.absoluteString
        } else if let medium360 = streamURLs[XCDYouTubeVideoQuality.medium360.rawValue] as? URL {
            urlString = medium360.absoluteString
        } else if let small240 = streamURLs[XCDYouTubeVideoQuality.small240.rawValue] as? URL {
            urlString = small240.absoluteString
        }
        return URL(string: urlString)
    }
    
}
