//
//  StorageManager.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/4/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import CoreData

class StorageManager {
    static let shared = StorageManager()
    private init() {}
    
    lazy var context: NSManagedObjectContext = {
        return persistentContainer.viewContext
    }()
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: .itsMoti)
        container.loadPersistentStores(completionHandler: { _, error in
            if let error = error {
                fatalError(error.localizedDescription)
            }
        })
        return container
    }()
}

// MARK: -

extension StorageManager {
    
    func applicationWillTerminate(_ application: UIApplication) {
        StorageManager.shared.saveContext()
        AppManager.shared.clearTemporaryDirectory()
    }
    
}

// MARK: -

extension StorageManager {
    
    func update(_ motiModel: MotiModel) {
        let moti = motiBy(id: motiModel.id)
        
        if moti.modified != motiModel.modified {
            if let mediaModel = motiModel.media {
                if let media = moti.media {
                
                    let thumbnailURL = URL(string: mediaModel.thumbnail ?? "")
                    let resourceURL  = URL(string: mediaModel.resource ?? "")
                
                    if thumbnailURL?.lastPathComponent != media.thumbnail ||
                        resourceURL?.lastPathComponent != media.resource {
                        StorageManager.shared.remove(media)
                    }
                }
                download(mediaModel) {
                    moti.update(mediaModel: mediaModel)
                    moti.update(motiModel: motiModel)
                    StorageManager.shared.saveContext()
                }
            } else {
                moti.update(motiModel: motiModel)
                StorageManager.shared.saveContext()
            }
        }
    }
    
    func motiBy(id: String) -> Moti {
        let predicate = NSPredicate(format: "id == %@", id)
        if let moti = update(Moti.self, predicate: predicate) {
            return moti
        } else {
            let moti = StorageManager.shared.insert(Moti.self)
            moti.id  = id
            return moti
        }
    }
    
}

// MARK: -

extension StorageManager {
    
    func insert<T: NSManagedObject>(_ entity: T.Type) -> T {
        let entityName = entity.entityName
        return NSEntityDescription.insertNewObject(forEntityName: entityName, into: context) as! T
    }
    
    func update<T: NSManagedObject>(_ entity: T.Type, predicate: NSPredicate) -> T? {
        return fetch(entity, predicate: predicate)?.first
    }
    
    func fetch<T: NSManagedObject>(_ entity: T.Type, predicate: NSPredicate) -> [T]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity.entityName)
        fetchRequest.predicate = predicate
        do {
            return try context.fetch(fetchRequest) as? [T]
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    func isExist<T: NSManagedObject>(_ entity: T.Type, predicate: NSPredicate) -> Bool {
        return fetch(entity, predicate: predicate)!.count > 0
    }
    
    func all<T: NSManagedObject>(_ entity: T.Type) -> [T]? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity.entityName)
        do {
            return try context.fetch(fetchRequest) as? [T]
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    func clear<T: NSManagedObject>(_ entity: T.Type) {
        let fetchRequest = NSFetchRequest<T>(entityName: entity.entityName)
        do {
            _ = try context.fetch(fetchRequest).map { context.delete($0) }
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    func delete(_ entity: NSManagedObject) {
        if let moti = entity as? Moti, let media = moti.media {
            remove(media)
        }
        context.delete(entity)
    }
    
    func saveContext() {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                fatalError(error.localizedDescription)
            }
        }
    }
    
}

// MARK: - Media action

extension StorageManager {
    
    func append(_ media: Media) {
        guard let type = media.type else { return }
        
        if let resource = media.resource {
            guard let directory = Directory(rawValue: type) else { return }
            AppManager.shared.move(filename: resource, to: directory)
        }
        if let thumbnail = media.thumbnail {
            guard type != MediaType.photo.rawValue else { return }
            AppManager.shared.move(filename: thumbnail, to: .photo)
        }
    }
    
    func remove(_ media: Media) {
        guard let type = media.type else { return }
        
        if let resource = media.resource {
            guard let directory = Directory(rawValue: type) else { return }
            AppManager.shared.delete(filename: resource, from: directory)
        }
        
        if let thumbnail = media.thumbnail {
            if type != MediaType.photo.rawValue {
                AppManager.shared.delete(filename: thumbnail, from: .photo)
            }
        }
    }
    
}

private extension StorageManager {
    
    func download(_ mediaModel: MediaModel, _ callback: @escaping () -> Void) {
        let dispatchGroup = DispatchGroup()
        
        if let  thumbnail = mediaModel.thumbnail, let url = URL(string: thumbnail) {
            dispatchGroup.enter()
            DownloadManager.shared.download(url: url, to: .photo) { error in
                if let error = error {
                    print(error.localizedDescription)
                }
                dispatchGroup.leave()
            }
        }
        
        if let resource = mediaModel.resource, let url = URL(string: resource) {
            dispatchGroup.enter()
            let directory = Directory(rawValue: mediaModel.type.rawValue)!
            DownloadManager.shared.download(url: url, to: directory) { error in
                if let error = error {
                    print(error.localizedDescription)
                }
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            callback()
        }
    }
    
}

