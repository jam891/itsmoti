//
//  PushManager.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/4/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import UserNotifications

class PushManager: NSObject {
    static let shared = PushManager()
    private override init() {}
}

extension PushManager {
    
    func requestAuthorization() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in
            guard granted else { return }
            if User.deviceToken == nil {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
    }
    
}

// MARK: - UNUserNotificationCenterDelegate

extension PushManager: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print(#function)
        
        completionHandler([.badge, .sound, .alert])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print(#function)
        
        let categoryIdentifier = response.notification.request.content.categoryIdentifier
        let userInfo = response.notification.request.content.userInfo
        let aps = userInfo["aps"] as! [String: Any]
        
        print("aps", aps, "categoryIdentifier", categoryIdentifier)
        
        completionHandler()
    }
    
}

// MARK: - UIApplicationDelegate

extension PushManager {
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print(#function)
        
        let tokenParts = deviceToken.map { String(format: "%02.2hhx", $0) }
        User.deviceToken = tokenParts.joined()
       
        sendDeviceTokenToServer(deviceToken: User.deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(#function)
        
        let aps = userInfo["aps"] as! [String: Any]
        if aps["content-available"] as? Int == 1 {
            
            print("silent sync")
            
            SyncManager.shared.synchronize()
        } else  {
            print("push notification")
            
            print("aps:", aps)
        }
        completionHandler(.newData)
    }
    
}

private extension PushManager {
    
    func sendDeviceTokenToServer(deviceToken: String) {
        print(#function)
        
        print("deviceToken", deviceToken)
        
        API.post.register(deviceToken) { error in
            if let error = error {
                Alert.showErrorAlert(error.localizedDescription)
            }
        }
    }
    
    func incrementBadgeNumberBy(badgeNumberIncrement: Int) {
        let currentBadgeNumber = UIApplication.shared.applicationIconBadgeNumber
        let updatedBadgeNumber = currentBadgeNumber + badgeNumberIncrement
        if updatedBadgeNumber > -1 {
            UIApplication.shared.applicationIconBadgeNumber = updatedBadgeNumber
        }
    }
    
}

