//
//  LocationManager.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 12/2/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import CoreLocation

class LocationManager: NSObject {
    static let shared = LocationManager()
    private override init() {}
    
    private let locationManager = CLLocationManager()
    
    private var didUpdateLocation: ((_ location: CLLocation?, _ error: Error?) -> Void)?
}

// MARK: - Public

extension LocationManager {
    
    func updateLocation(_ callback: @escaping (_ location: LocationModel) -> Void) {
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        didUpdateLocation = { location, error in
            LocationManager.shared.reverseGeocode(location!, callback)
        }
    }
    
}

// MARK: - Private

private extension LocationManager {
    
    func reverseGeocode(_ location: CLLocation, _ callback: @escaping (_ location: LocationModel) -> Void) {
        CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
            if error != nil {
                let locationCoordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                let title = locationCoordinate.degreeDescription
                let location = LocationModel(title: title, subtitle: "")
                callback(location)
            } else {
                guard let placemark = placemarks?.first else { return }
                let location = LocationManager.shared.location(from: placemark)
                callback(location)
            }
        }
    }
    
    func location(from placemark: CLPlacemark) -> LocationModel {
        let address1   = placemark.thoroughfare ?? ""
        let address2   = placemark.subThoroughfare ?? ""
        let state      = placemark.administrativeArea ?? ""
        let postalCode = placemark.postalCode ?? ""
        let city       = placemark.locality ?? ""
        let country    = placemark.country ?? ""
        
        let title = [address1, address2].filter { $0.count > 0 }.joined(separator: ", ")
        let subtitle = [title, city, state, country, postalCode].filter { $0.count > 0 }.joined(separator: ", ")
        
        return LocationModel(title: title, subtitle: subtitle)
    }
}

// MARK: - CLLocationManagerDelegate

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        didUpdateLocation?(nil, error)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        guard let location = locations.first else { return }
        didUpdateLocation?(location, nil)
    }
    
}
