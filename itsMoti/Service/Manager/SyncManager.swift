//
//  SyncManager.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/8/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class SyncManager {
    static let shared = SyncManager()
    private init() {}
}

extension SyncManager {
    
    func synchronize() {
        print(#function)
        
        append()
        delete()
        create()
//        update()
    }
    
}

// MARK: - Private

private extension SyncManager {
    
    func append() {
        API.post.moties { moties, error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                moties.forEach { StorageManager.shared.update($0) }
                StorageManager.shared
                    .all(Moti.self)!
                    .filter { moti in !moties.contains(where: { moti.id == $0.id }) }
                    .forEach { StorageManager.shared.delete($0) }
            }
        }
    }
    
    func delete() {
        let group = DispatchGroup()
        StorageManager.shared
            .all(Moti.self)!
            .filter { $0.id != nil && $0.startDate == nil }
            .forEach {
                group.enter()
                API.post.delete($0) { error in
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        StorageManager.shared.saveContext()
                    }
                    group.leave()
                }
        }
        group.notify(queue: .main) {
            print("delete is done.")
        }
    }
    
    func create() {
        let group = DispatchGroup()
        StorageManager.shared
            .all(Moti.self)!
            .filter { $0.id == nil && $0.startDate != nil }
            .forEach {
                group.enter()
                API.post.create($0) { error in
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        StorageManager.shared.saveContext()
                    }
                    group.leave()
                }
        }
        group.notify(queue: .main) {
            print("create is done.")
        }
    }
    
    func update() {
        let group = DispatchGroup()
        StorageManager.shared
            .all(Moti.self)!
            .filter { $0.id != nil }
            .forEach {
                group.enter()
                API.post.update($0) { error in
                    if let error = error {
                        print(error.localizedDescription)
                    } else {
                        StorageManager.shared.saveContext()
                    }
                    group.leave()
                }
        }
        group.notify(queue: .main) {
            print("update is done.")
        }
    }
    
}

/////

private extension SyncManager {
    
    func log(_ moties: [MotiModel]) {
        print("moties  count:", moties.count)
        moties.forEach { print($0.id) }
        
        let storage = StorageManager.shared.all(Moti.self)!
        print("storage count:", storage.count)
        storage.forEach { print($0.id ?? "none") }
        
        let removed = storage.filter { moti in
            !moties.contains(where: { moti.id == $0.id })
        }
        print("removed count:", removed.count)
        removed.forEach { print($0.id ?? "none") }
        
        let deleted = StorageManager.shared
            .all(Moti.self)!
            .filter { $0.id != nil && $0.startDate == nil }
        print("deleted count:", deleted.count)
        deleted.forEach { print($0.id ?? "none") }
    }
    
}
