//
//  PaymentManager.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 2/8/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import PassKit

class PaymentManager: NSObject {
    
    typealias PaymentCompletionHandler = (Bool) -> Void
    
    static let shared = PaymentManager()
    private override init() {}

    private let supportedNetworks: [PKPaymentNetwork] = [
        .masterCard,
        .amex,
        .visa
    ]
    
    struct Merchant {
        static let id = "merchant." + "\(Bundle.main.bundleIdentifier!)"
        static let countryCode = "US"
        static let currencyCode = "USD"
    }

    private var paymentStatus: PKPaymentAuthorizationStatus = .failure
    private var completionHandler: PaymentCompletionHandler?
    
}

// MARK: -

extension PaymentManager {
    
    func purchase(_ subscription: SubscriptionModel, _ completion: @escaping PaymentCompletionHandler) {
        print(#function)
        
        completionHandler = completion
        
        let request = PKPaymentRequest()
        request.merchantIdentifier = Merchant.id
        request.supportedNetworks = supportedNetworks
        request.merchantCapabilities = .capability3DS
        request.countryCode  = Merchant.countryCode
        request.currencyCode = Merchant.currencyCode
        request.paymentSummaryItems = [
            PKPaymentSummaryItem(label: subscription.title, amount: NSDecimalNumber(value: subscription.price))
        ]
        request.requiredShippingContactFields = [.phoneNumber, .emailAddress]
        
        let paymentController = PKPaymentAuthorizationController(paymentRequest: request)
        paymentController.delegate = self
        
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: supportedNetworks) {
            paymentController.present(completion: nil)
        } else {
            Alert.showErrorAlert("This Device cannot make Payments as it does't support ApplePay")
            self.completionHandler!(false)
        }
    }
    
}

// MARK: - PKPaymentAuthorizationControllerDelegate

extension PaymentManager: PKPaymentAuthorizationControllerDelegate {
    
    func paymentAuthorizationController(_ controller: PKPaymentAuthorizationController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        print(#function)
        
        if payment.shippingContact?.emailAddress == nil || payment.shippingContact?.phoneNumber == nil {
            paymentStatus = .failure
        } else {
            
            API.post.confirmation(payment.token.transactionIdentifier) { error in
                
            }
            
            paymentStatus = .success
        }
        let result = PKPaymentAuthorizationResult(status: paymentStatus, errors: nil)
        completion(result)
    }
    
    func paymentAuthorizationControllerDidFinish(_ controller: PKPaymentAuthorizationController) {
        print(#function)
        
        controller.dismiss {
            DispatchQueue.main.async {
                if self.paymentStatus == .success {
                    self.completionHandler!(true)
                } else {
                    self.completionHandler!(false)
                }
            }
        }
    }
    
}


