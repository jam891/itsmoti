//
//  AppManager.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/4/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

enum Directory: String {
    case video
    case audio
    case voice
    case photo
}

class AppManager {
    static let shared = AppManager()
    private init() {}
    
    private let filemgr = FileManager.default
    private var documentDirectory: URL {
        return filemgr.urls(for: .documentDirectory, in: .userDomainMask)[0]
    }
    
    var temporaryDirectory: URL {
        return filemgr.temporaryDirectory
    }
}

extension AppManager {
    
    func url(for directory: Directory) -> URL {
        let directory = documentDirectory.appendingPathComponent(directory.rawValue.capitalized)
        if !filemgr.fileExists(atPath: directory.path) {
            do {
                try filemgr.createDirectory(atPath: directory.path, withIntermediateDirectories: false, attributes: [:])
            } catch {
                print(error.localizedDescription)
            }
        }
        return directory
    }
    
    func url(for filename: String, from directory: Directory) -> URL? {
        let sourceURL = url(for: directory).appendingPathComponent(filename)
        if filemgr.fileExists(atPath: sourceURL.path) {
            return sourceURL
        }
        return nil
    }
    
    func url(filename: String, for directory: Directory) -> URL? {
        return url(for: directory).appendingPathComponent(filename)
    }
    
    func moveItem(atPath srcPath: String, toPath dstPath: String) {
        do {
            if !filemgr.fileExists(atPath: dstPath) {
                try filemgr.moveItem(atPath: srcPath, toPath: dstPath)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func copyItem(atPath srcPath: String, toPath dstPath: String) {
        do {
            if !filemgr.fileExists(atPath: dstPath) {
                try filemgr.copyItem(atPath: srcPath, toPath: dstPath)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func removeItem(atPath: String) {
        if filemgr.fileExists(atPath: atPath) {
            do {
                try filemgr.removeItem(atPath: atPath)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func move(filename: String, to directory: Directory) {
        let atPath = temporaryDirectory.appendingPathComponent(filename).path
        let toPath = url(for: directory).appendingPathComponent(filename).path
        
        do {
            try filemgr.moveItem(atPath: atPath, toPath: toPath)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func delete(filename: String, from directory: Directory) {
        let path = url(for: directory).appendingPathComponent(filename).path
        removeItem(atPath: path)
    }
    
    func writeToTemporaryDirectoryImageData(_ data: Data) -> URL? {
        let imageURL = filemgr.temporaryDirectory
            .appendingPathComponent(UUID().uuidString)
            .appendingPathExtension(.jpeg)
        do {
            try data.write(to: imageURL)
            return imageURL
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
    
    func all(pathExtension: String, from directory: Directory) -> [URL] {
        do {
            let urls = try filemgr.contentsOfDirectory(at: url(for: directory),
                                                       includingPropertiesForKeys: nil,
                                                       options: .skipsHiddenFiles)
            return urls.filter { $0.pathExtension == pathExtension }
        } catch {
            return []
        }
    }
    
    func clearTemporaryDirectory() {
        do {
            let tmpDirURL = filemgr.temporaryDirectory
            let tmpDirectory = try filemgr.contentsOfDirectory(atPath: tmpDirURL.path)
            try tmpDirectory.forEach { file in
                let fileUrl = tmpDirURL.appendingPathComponent(file)
                try filemgr.removeItem(atPath: fileUrl.path)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
}

private extension AppManager {
    
}
