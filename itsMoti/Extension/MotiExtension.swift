//
//  MotiExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

extension Moti {
    
    func update(todolist: [TodoModel]) {
        todolist.forEach {
            let todo = StorageManager.shared.insert(Todo.self)
            todo.name       = $0.name
            todo.completed  = $0.completed
            todo.createDate = $0.createDate
            addToTodoList(todo)
        }
    }
    
    func update(mediaModel: MediaModel) {
        if media == nil {
            let media = StorageManager.shared.insert(Media.self)
            media.type   = mediaModel.type.rawValue
            media.moti   = self
        }
        
        if let resource = mediaModel.resource {
            if let resourceURL = URL(string: resource) {
                media!.resource = resourceURL.lastPathComponent
            }
        }
        
        if let thumbnail = mediaModel.thumbnail {
            if let thumbnailURL = URL(string: thumbnail) {
                media!.thumbnail = thumbnailURL.lastPathComponent
            }
        }
    }
    
    func update(locationModel: LocationModel) {
        if location == nil {
            let location = StorageManager.shared.insert(Location.self)
            location.moti  = self
        }
        location!.title    = locationModel.title
        location!.subtitle = locationModel.subtitle
    }
    
    func update(motiModel: MotiModel) {
        title     = motiModel.title
        body      = motiModel.body
        
        created   = motiModel.created
        modified  = motiModel.modified
        startDate = motiModel.startDate

        snooze    = motiModel.snooze
        recurrenceRule = motiModel.recurrenceRule
        
        if let location = motiModel.location {
            update(locationModel: location)
        }
        
        if motiModel.todoList?.count ?? 0 > 0 {
            if [todoList].count > 0 {
                todoList!.forEach { StorageManager.shared.delete($0 as! Todo) }
            }
            update(todolist: motiModel.todoList ?? [])
        }
    }
    
}
