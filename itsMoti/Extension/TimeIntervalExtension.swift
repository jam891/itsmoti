//
//  TimeIntervalExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/28/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

extension TimeInterval {
    
    var minutes: Int {
        return Int((self / 60).truncatingRemainder(dividingBy: 60))
    }
    var seconds: Int {
        return Int(truncatingRemainder(dividingBy: 60))
    }
    var milliseconds: Int {
        return Int((self * 1000).truncatingRemainder(dividingBy: 1000))
    }
    var formatted: String {
        return String(format:"%02d:%02d.%03d", minutes, seconds, milliseconds)
    }
    
    var formattedCurrent: String {
        return String(format:"%02d:%02d", minutes, seconds)
    }
    
    var stringValue: String {
        return String(format:"%02d:%02d", minutes, seconds)
    }
    
}
