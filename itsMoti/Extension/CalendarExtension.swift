//
//  CalendarExtension.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/22/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

extension Calendar {
    var currentYear: Int {
        return Calendar.current.component(.year, from: Date())
    }
    func numberOfDaysInMonth(_ month: Int, year: Int) -> Int {
        let dateComponents = dateComponentsWithYear(year, month: month, day: 1)
        let range = Calendar.current.range(of: .day, in: .month, for: Calendar.current.date(from: dateComponents)!)
        return range!.count
    }
    func weekdayOfDate(_ date: Date) -> Int {
        return Calendar.current.component(.weekday, from: date)
    }
    func dateWithYear(_ year: Int, month: Int, day: Int) -> Date {
        let dateComponents = dateComponentsWithYear(year, month: month, day: day)
        return Calendar.current.date(from: dateComponents)!
    }
    func dateComponentsWithYear(_ year: Int, month: Int, day: Int) -> DateComponents {
        return DateComponents(year: year, month: month, day: day)
    }
}
