//
//  StringExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

extension String {
    static let itsMoti     = "itsMoti"
    static let orientation = "orientation"
    static let accessToken = "accessToken"
    static let deviceToken = "deviceToken"
}

extension String {
    static let m4a  = "m4a"
    static let mov  = "mov"
    static let mp4  = "mp4"
    static let jpeg = "jpeg"
}

extension String {
    
    func trim() -> String {
        return trimmingCharacters(in: .whitespaces)
    }
    
    var isEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let rect = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        return rect.height
    }
}

extension String {
    mutating func removeSubstring(_ substring: String) {
        self = replacingOccurrences(of: substring, with: "", options: .literal, range: nil)
    }
    
    static func sequenceNumberString(of number: Int) -> String {
        var suffix = "th"
        let ones = number % 10
        let tens = (number / 10) % 10
        
        if tens == 1 {
            suffix = "th"
        } else if ones == 1 {
            suffix = "st"
        } else if ones == 2 {
            suffix = "nd"
        } else if ones == 3 {
            suffix = "rd"
        } else {
            suffix = "th"
        }
        return "\(number)\(suffix)"
    }
}

