//
//  AVPlayerExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/28/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import AVKit

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}
