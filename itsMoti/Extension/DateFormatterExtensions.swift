//
//  DateFormatterExtensions.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/7/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

extension DateFormatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.timeZone = .current
        return formatter
    }()
}
