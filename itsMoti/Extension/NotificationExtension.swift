//
//  NotificationExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let reachabilityChangedNotification = Notification.Name("ReachabilityChangedNotification")
}

extension NotificationCenter {
    static func post(_ name: Notification.Name, object: Any? = nil) {
        NotificationCenter.default.post(name: name, object: object)
    }
    static func addObserver(_ observer: Any, selector: Selector, name: Notification.Name, object: Any? = nil) {
        NotificationCenter.default.addObserver(observer, selector: selector, name: name, object: object)
    }
    static func removeObserver(_ observer: Any, name: Notification.Name?, object: Any? = nil) {
        NotificationCenter.default.removeObserver(observer, name: name, object: object)
    }
    static func removeObserver(_ observer: Any) {
        NotificationCenter.default.removeObserver(observer)
    }
}
