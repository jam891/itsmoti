//
//  UIStoryboardExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

enum Storyboard: String {
    case moti
    case photo
    case video
    case audio
    case voice
    case notes
}

enum Identifier: String {
    case moti      = "MotiViewController"
    case photo     = "PhotoViewController"
    case video     = "VideoViewController"
    case audio     = "AudioViewController"
    case voice     = "VoiceViewController"
    case notes     = "NotesViewController"
    case record    = "RecordPickerController"
    case memos     = "VoicePickerController"
    case social    = "VideoPickerController"
    case endRepeat = "EndRepeatViewController"
}

extension UIStoryboard {
    private convenience init(_ storyboard: Storyboard) {
        self.init(name: storyboard.rawValue.capitalized, bundle: .main)
    }
    private class func viewController(_ identifier: Identifier) -> UIViewController {
        switch identifier {
        case .moti, .endRepeat:
            return UIStoryboard(.moti).instantiateViewController(withIdentifier: identifier.rawValue)
        case .photo:
            return UIStoryboard(.photo).instantiateViewController(withIdentifier: identifier.rawValue)
        case .video, .social:
            return UIStoryboard(.video).instantiateViewController(withIdentifier: identifier.rawValue)
        case .audio:
            return UIStoryboard(.audio).instantiateViewController(withIdentifier: identifier.rawValue)
        case .voice, .record, .memos:
            return UIStoryboard(.voice).instantiateViewController(withIdentifier: identifier.rawValue)
        case .notes:
            return UIStoryboard(.notes).instantiateViewController(withIdentifier: identifier.rawValue)
        }
    }
}

extension UIStoryboard {
    class func motiViewController() -> MotiViewController {
        return viewController(.moti) as! MotiViewController
    }
    class func photoViewController() -> PhotoViewController {
        return viewController(.photo) as! PhotoViewController
    }
    class func videoViewController() -> VideoViewController {
        return viewController(.video) as! VideoViewController
    }
    class func audioViewController() -> AudioViewController {
        return viewController(.audio) as! AudioViewController
    }
    class func voiceViewController() -> VoiceViewController {
        return viewController(.voice) as! VoiceViewController
    }
    class func notesViewController() -> NotesViewController {
        return viewController(.notes) as! NotesViewController
    }
    class func recordPickerController() -> RecordPickerController {
        return viewController(.record) as! RecordPickerController
    }
    class func voicePickerController() -> VoicePickerController {
        return viewController(.memos) as! VoicePickerController
    }
    class func videoPickerController() -> VideoPickerController {
        return viewController(.social) as! VideoPickerController
    }
    class func endRepeatViewController() -> EndRepeatViewController {
        return viewController(.endRepeat) as! EndRepeatViewController
    }
}
