//
//  NumericExtension.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/13/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

extension Bool {
    
    var dataValue: Data {
        return description.data(using: .utf8)!
    }
    
}
