//
//  BundleExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

extension Bundle {
    var name: String {
        return object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""
    }
}

