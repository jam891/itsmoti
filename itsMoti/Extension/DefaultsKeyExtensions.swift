//
//  DefaultsKeyExtensions.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/18/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import DefaultsKit

extension DefaultsKey {
    static let userKey = Key<User>("userKey")
    static let runKey  = Key<Bool>("runKey")
}
