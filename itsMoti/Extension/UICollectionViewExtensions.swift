//
//  UICollectionViewExtensions.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 10/5/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

protocol Nibable: class {
    static var nibName: String { get }
}

extension Nibable where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}

extension UICollectionView {
    func dequeueReusableCell<T: UICollectionViewCell>(withClass name: T.Type, for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: String(describing: name), for: indexPath) as! T
    }
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(ofKind kind: String, withClass name: T.Type, for indexPath: IndexPath) -> T {
        return dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: String(describing: name), for: indexPath) as! T
    }
    func register<T: UICollectionViewCell>(cellClass name: T.Type) {
        let nib = UINib(nibName: T.nibName, bundle: Bundle(for: T.self))
        register(nib, forCellWithReuseIdentifier: String(describing: name))
    }
}

extension UICollectionViewCell: Nibable {}

