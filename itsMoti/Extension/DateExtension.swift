//
//  DateExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation
import DateToolsSwift

extension Date {
    
    var dataValue: Data {
        return string(format: "yyyy-MM-dd HH:mm:ss").data(using: .utf8)!
    }
    
    func string(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = .current
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }

    func isThisMonth() -> Bool {
        return Calendar.current.isDate(self, equalTo: Date(), toGranularity: .month)
    }
    
}

extension Date {
    
    func isLater(than date: Date) -> Bool {
        return compare(date) == .orderedDescending
    }
    
    func dateOnly() -> Date {
        var dateComponents = DateComponents()
        dateComponents.year  = component(.year)
        dateComponents.month = component(.month)
        dateComponents.day   = component(.day)
        
        guard let date = Calendar.current.date(from: dateComponents) else { return Date() }
        return date
    }
    
    func format(with dateStyle: DateFormatter.Style, timeZone: TimeZone, locale: Locale) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = dateStyle
        dateFormatter.timeZone  = timeZone
        dateFormatter.locale    = locale
        
        return dateFormatter.string(from: self)
    }
    
    func format(with dateStyle: DateFormatter.Style) -> String {
        return format(with: dateStyle, timeZone: TimeZone.autoupdatingCurrent, locale: Locale.autoupdatingCurrent)
    }
    
    func component(_ component: Calendar.Component) -> Int {
        let calendar = Calendar.autoupdatingCurrent
        return calendar.component(component, from: self)
    }
    
    var weekday: Int {
        return component(.weekday)
    }
    
    var isToday: Bool {
        let calendar = Calendar.autoupdatingCurrent
        return calendar.isDateInToday(self)
    }
    
    var isWeekend: Bool {
        if weekday == 7 || weekday == 1 {
            return true
        }
        return false
    }
    
    var day: Int {
        return component(.day)
    }
    
    func days(from date: Date, calendar: Calendar?) -> Int {
        var calendarCopy = calendar
        if (calendar == nil) {
            calendarCopy = Calendar.autoupdatingCurrent
        }
        
        let earliest = earlierDate(date)
        let latest = (earliest == self) ? date : self
        let multiplier = (earliest == self) ? -1 : 1
        let components = calendarCopy!.dateComponents([.day], from: earliest, to: latest)
        return multiplier*components.day!
    }
    
    func earlierDate(_ date:Date) -> Date {
        return (timeIntervalSince1970 <= date.timeIntervalSince1970) ? self : date
    }
    
    func add(_ chunk: TimeChunk) -> Date {
        let calendar = Calendar.autoupdatingCurrent
        var components = DateComponents()
        components.year = chunk.years
        components.month = chunk.months
        components.day = chunk.days + (chunk.weeks*7)
        components.hour = chunk.hours
        components.minute = chunk.minutes
        components.second = chunk.seconds
        return calendar.date(byAdding: components, to: self)!
    }
    
}

extension TimeChunk {
    
    public static func dateComponents(seconds: Int = 0,
                                      minutes: Int = 0,
                                      hours: Int = 0,
                                      days: Int = 0,
                                      weeks: Int = 0,
                                      months: Int = 0,
                                      years: Int = 0) -> TimeChunk {
        return TimeChunk(seconds: seconds,
                         minutes: minutes,
                         hours: hours,
                         days: days,
                         weeks: weeks,
                         months: months,
                         years: years)
    }
    
}
