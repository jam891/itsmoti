//
//  UIViewExtensions.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

extension UIView {
    
    func xibSetup() {
        let child = loadNib()
        addSubview(child)
        
        child.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            child.topAnchor.constraint( equalTo: child.superview!.topAnchor),
            child.bottomAnchor.constraint( equalTo: child.superview!.bottomAnchor),
            child.leadingAnchor.constraint( equalTo: child.superview!.leadingAnchor),
            child.trailingAnchor.constraint( equalTo: child.superview!.trailingAnchor)
            ])
    }
    
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
    
}


