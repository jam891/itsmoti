//
//  NSManagedObjectExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import CoreData

extension NSManagedObject {
    static var entityName: String {
        return String(describing: self)
    }
}
