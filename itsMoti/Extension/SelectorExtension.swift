//
//  SelectorExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

extension Selector {
    static let canRotate = #selector(MediaViewController.canRotate)
    static let orientationDidChange = #selector(MediaViewController.orientationDidChange)
}
