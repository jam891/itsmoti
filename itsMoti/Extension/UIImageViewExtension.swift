//
//  UIImageViewExtension.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func alphaAtPoint(_ point: CGPoint) -> CGFloat {
        var pixel: [UInt8] = [0, 0, 0, 0]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let alphaInfo = CGImageAlphaInfo.premultipliedLast.rawValue
        
        guard
            let context = CGContext(data: &pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: alphaInfo)
        else { return 0 }
        
        context.translateBy(x: -point.x, y: -point.y)
        
        layer.render(in: context)
       
        return CGFloat(pixel[3])
    }
    
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }
        
        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }
        
        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2
        let y = (bounds.height - size.height) / 2
        
        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
    
}
