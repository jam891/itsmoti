//
//  UILabelExtensions.swift
//  itsMoti
//
//  Created by Mihail Shevchuk on 12/19/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

extension UILabel {
    
    func setText(lhs: String, rhs: String) {
        let title = NSMutableAttributedString(string: lhs, attributes:[ .foregroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) ])
        title.append(NSAttributedString(string: " "))
        title.append(NSMutableAttributedString(string: rhs, attributes:[
            .font: UIFont(name: "PoetsenOne-Regular", size: 17)!,
            .foregroundColor: #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
            ]))
        self.attributedText = title
        self.textAlignment = .center
    }
    
}
