//
//  CLLocationExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 12/2/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import MapKit

extension CLLocationCoordinate2D {
    var degreeDescription: String {
        return [latitudeDegreeDescription, longitudeDegreeDescription].joined(separator: ", ")
    }
    var latitudeDegreeDescription: String {
        return fromDecToDeg(input: latitude) + " \(latitude >= 0 ? "N" : "S")"
    }
    var longitudeDegreeDescription: String {
        return fromDecToDeg(input: longitude) + " \(longitude >= 0 ? "E" : "W")"
    }
    private func fromDecToDeg(input: Double) -> String {
        var inputSeconds = Int(input * 3600)
        let inputDegrees = inputSeconds / 3600
        inputSeconds = abs(inputSeconds % 3600)
        let inputMinutes = inputSeconds / 60
        inputSeconds %= 60
        return "\(abs(inputDegrees))°\(inputMinutes)'\(inputSeconds)''"
    }
}
