//
//  UIViewControllerExtension.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/14/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setTitle(lhs: String, rhs: String) {
        let label = UILabel()
        let title = NSMutableAttributedString(string: lhs, attributes:[
            .foregroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),
            .font: UIFont(name: "PoetsenOne-Regular", size: 21)!
            ])
        title.append(NSAttributedString(string: " "))
        title.append(NSMutableAttributedString(string: rhs, attributes:[
            .font: UIFont(name: "PoetsenOne-Regular", size: 21)!,
            .foregroundColor: #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
            ]))
        label.attributedText = title
        label.textAlignment = .center
        navigationItem.titleView = label
    }
    
}
