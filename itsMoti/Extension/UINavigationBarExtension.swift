//
//  UINavigationBarExtension.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/29/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

extension UINavigationBar {
    var hairline: UIImageView? {
        return findShadowImage(under: self)
    }
    
    private func findShadowImage(under view: UIView) -> UIImageView? {
        if view is UIImageView && view.bounds.size.height <= 1 {
            return view as? UIImageView
        }
        for subview in view.subviews {
            if let imageView = findShadowImage(under: subview) {
                return imageView
            }
        }
        return nil
    }
}
