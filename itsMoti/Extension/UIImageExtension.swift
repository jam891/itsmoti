//
//  UIImageExtension.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/19/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

extension UIImage {
    
    func resizeImage(ratio: CGFloat = 0.3) -> UIImage {
        let resizedSize = CGSize(width: Int(size.width * ratio), height: Int(size.height * ratio))
        UIGraphicsBeginImageContext(resizedSize)
        draw(in: CGRect(x: 0, y: 0, width: resizedSize.width, height: resizedSize.height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage!
    }
    
    func applyFilter(name: String) -> UIImage? {
        let context = CIContext()
        
        if let filter = CIFilter(name: name) {
            let image = CIImage(image: self)
            filter.setValue(image, forKey: kCIInputImageKey)
            let result = filter.outputImage!
            let cgImage = context.createCGImage(result, from: result.extent)
            return UIImage(cgImage: cgImage!, scale: scale, orientation: imageOrientation)
        } else {
            return nil
        }
    }
    
    func rotate(by radians: CGFloat) -> UIImage {
        let t = CGAffineTransform(rotationAngle: radians);
    
        let rotateRect = CGRect(x: 0, y: 0, width: size.width, height: size.height).applying(t)
    let rotatedSize = rotateRect.size
    
    UIGraphicsBeginImageContextWithOptions(rotatedSize, false, UIScreen.main.scale);
    let ctx = UIGraphicsGetCurrentContext()
    
        ctx?.translateBy(x: rotatedSize.width/2.0, y: rotatedSize.height/2.0)
        
//    CGContextTranslateCTM(ctx, rotatedSize.width/2.0, rotatedSize.height/2.0);
    
//        CGContextRotateCTM(ctx!, radians);
        
        ctx?.rotate(by: radians)
        
//    let layer = CALayer()
    
        ctx?.scaleBy(x: 1, y: -1)
   
//    CGContextScaleCTM(ctx, 1.0, -1.0);
  
        ctx!.draw(cgImage!, in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height))
        
//        CGContextDrawImage(ctx, CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height), self.CGImage);
    //获取绘制后生成的新图片
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
        return newImage!
    
    }
    
}
