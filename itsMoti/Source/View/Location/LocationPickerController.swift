//
//  LocationPickerController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/16/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import MapKit

protocol LocationPickerControllerDelegate: class {
    func locationPickerControllerDidCancel(_ picker: LocationPickerController)
    func locationPickerController(_ picker: LocationPickerController, didFinishPickingLocation locationModel: LocationModel)
}

class LocationPickerController: TableViewController {
    
    @IBOutlet weak var containerView: UIView!

    private var searchController: UISearchController!
    private lazy var searchCompleter: MKLocalSearchCompleter = {
        let searchCompleter = MKLocalSearchCompleter()
        searchCompleter.delegate = self
        return searchCompleter
    }()
    
    private var places = [MKLocalSearchCompletion]()
    
    weak var delegate: LocationPickerControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController.isActive = true
        requestAuthorization()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        searchController.isActive = false
    }

}

// MARK: - Private

private extension LocationPickerController {
    
    func setup() {
        if Permission.locationWhenInUse.status == .authorized {
            updateUI()
        }
        configureSearchController()
    }
    
    func requestAuthorization() {
        if Permission.locationWhenInUse.status == .notDetermined {
            Permission.locationWhenInUse.request { status in
                if status == .authorized {
                    self.updateUI()
                }
            }
        }
    }
    
    func updateUI() {
        tableView.tableHeaderView = containerView
    }
    
    func configureSearchController() {
        searchController = SearchController(searchResultsController: nil)
        
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            tableView.tableHeaderView = searchController.searchBar
        }
        
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Enter Location"
        searchController.searchBar.returnKeyType = .done
        searchController.searchBar.delegate = self
        searchController.searchBar.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        searchController.delegate = self
        
        definesPresentationContext = true
    }
    
    func configure(_ cell: UITableViewCell, at indexPath: IndexPath) {
        cell.textLabel?.text       = places[indexPath.row].title
        cell.detailTextLabel?.text = places[indexPath.row].subtitle
    }
    
    func updateLocation(_ location: LocationModel) {
        searchController.isActive = false
        delegate?.locationPickerController(self, didFinishPickingLocation: location)
    }

}

// MARK: - Actions

extension LocationPickerController {
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        searchController.isActive = false
        delegate?.locationPickerControllerDidCancel(self)
    }
    
    @IBAction func location(_ sender: UIButton) {
        containerView.backgroundColor = #colorLiteral(red: 0.8517865539, green: 0.8469383121, blue: 0.8468726277, alpha: 1)
        LocationManager.shared.updateLocation { location in
            self.containerView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.updateLocation(location)
        }
    }
    
}

// MARK: - UITableViewDataSource

extension LocationPickerController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell") else { fatalError() }
        configure(cell, at: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let place = places[indexPath.row]
        let location = LocationModel(title: place.title, subtitle: place.subtitle)
        updateLocation(location)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Locations"
    }
    
}

// MARK: - MKLocalSearchCompleterDelegate

extension LocationPickerController: MKLocalSearchCompleterDelegate {
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        places = completer.results
        tableView.reloadData()
    }

}

// MARK: - UISearchBarDelegate

extension LocationPickerController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            searchCompleter.queryFragment = searchText
        } else {
            places.removeAll()
            tableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        let location = LocationModel(title: searchBar.text ?? "", subtitle: "")
        updateLocation(location)
    }
    
}

// MARK: - UISearchControllerDelegate

extension LocationPickerController: UISearchControllerDelegate {
    
    func didPresentSearchController(_ searchController: UISearchController) {
        DispatchQueue.main.async {
            searchController.searchBar.becomeFirstResponder()
        }
    }

}
