//
//  DayViewState.swift
//  CalendarDemo
//
//  Created by Vitaliy Delidov on 1/21/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

protocol DayViewStateUpdating: AnyObject {
    func move(from oldDate: Date, to newDate: Date)
}

class DayViewState {

    private(set) var selectedDate: Date
    private var clients = [DayViewStateUpdating]()
    
    init(date: Date = Date()) {
        selectedDate = date.dateOnly()
    }
    
}

// MARK: - 

extension DayViewState {
    
    func move(to date: Date) {
        let date = date.dateOnly()
        notify(clients: clients, moveTo: date)
        selectedDate = date
    }
    
    func client(client: DayViewStateUpdating, didMoveTo date: Date) {
        let date = date.dateOnly()
        notify(clients: allClientsWithout(client: client), moveTo: date)
        selectedDate = date
    }
    
    func subscribe(client: DayViewStateUpdating) {
        clients.append(client)
    }
    
    func unsubscribe(client: DayViewStateUpdating) {
        clients = allClientsWithout(client: client)
    }
    
}

// MARK: -

private extension DayViewState {
    
    func allClientsWithout(client: DayViewStateUpdating) -> [DayViewStateUpdating] {
        return clients.filter { $0 !== client }
    }
    
    func notify(clients: [DayViewStateUpdating], moveTo date: Date) {
        clients.forEach {
            $0.move(from: selectedDate, to: date)
        }
    }
    
}
