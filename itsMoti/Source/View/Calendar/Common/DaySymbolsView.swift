//
//  DaySymbolsView.swift
//  CalendarDemo
//
//  Created by Vitaliy Delidov on 1/21/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class DaySymbolsView: UIView {

    var calendar = Calendar.autoupdatingCurrent
    var style = DaySymbolsStyle()
    var labels = [UILabel]()
    var daysInWeek = 7
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    init(daysInWeek: Int = 7) {
        super.init(frame: .zero)
        self.daysInWeek = daysInWeek
        setup()
    }
    
    override func layoutSubviews() {
        let labelsCount = CGFloat(labels.count)
        
        var per = bounds.width - bounds.height * labelsCount
        per /= labelsCount
        
        let minX = per / 2
        for (i, label) in labels.enumerated() {
            let frame = CGRect(x: minX + (bounds.height + per) * CGFloat(i), y: 0, width: bounds.height, height: bounds.height)
            label.frame = frame
        }
    }
    
}

// MARK: - Private

private extension DaySymbolsView {
    
    func setup() {
        for _ in 1...daysInWeek {
            let label = UILabel()
            label.textAlignment = .center
            labels.append(label)
            addSubview(label)
        }
        configure()
    }
    
    func configure() {
        let daySymbols = calendar.veryShortWeekdaySymbols
        let weekendMask = [true] + [Bool](repeating: false, count: 5) + [true]
        var weekDays = Array(zip(daySymbols, weekendMask))
        
        weekDays.shift(calendar.firstWeekday - 1)
        
        for (index, label) in labels.enumerated() {
            label.text = weekDays[index].0
            label.textColor = weekDays[index].1 ? .lightGray : .black
            label.font = UIFont.systemFont(ofSize: 10)
        }
    }

}

// MARK: -

extension DaySymbolsView {
    
    func updateStyle(_ newStyle: DaySymbolsStyle) {
        style = newStyle.copy() as! DaySymbolsStyle
        configure()
    }
    
}


