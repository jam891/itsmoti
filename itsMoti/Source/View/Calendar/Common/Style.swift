//
//  Style.swift
//  CalendarDemo
//
//  Created by Vitaliy Delidov on 1/21/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class DayHeaderStyle: NSCopying {
     var daySymbols = DaySymbolsStyle()
     var daySelector = DaySelectorStyle()
     var swipeLabel = SwipeLabelStyle()
     var backgroundColor = UIColor(white: 247 / 255, alpha: 1)
     init() {}
     func copy(with zone: NSZone? = nil) -> Any {
        let copy = DayHeaderStyle()
        copy.daySymbols = daySymbols.copy() as! DaySymbolsStyle
        copy.daySelector = daySelector.copy() as! DaySelectorStyle
        copy.swipeLabel = swipeLabel.copy() as! SwipeLabelStyle
        copy.backgroundColor = backgroundColor
        return copy
    }
}

 class DaySelectorStyle: NSCopying {
     var activeTextColor = UIColor.white
     var selectedBackgroundColor = UIColor.black
    
     var weekendTextColor = UIColor.gray
     var inactiveTextColor = UIColor.black
     var inactiveBackgroundColor = UIColor.clear
    
     var todayInactiveTextColor = UIColor.red
     var todayActiveBackgroundColor = UIColor.red
    
     var font = UIFont.systemFont(ofSize: 18)
     var todayFont = UIFont.boldSystemFont(ofSize: 18)
    
     init() {}
    
     func copy(with zone: NSZone? = nil) -> Any {
        let copy = DaySelectorStyle()
        copy.activeTextColor = activeTextColor
        copy.selectedBackgroundColor = selectedBackgroundColor
        copy.weekendTextColor = weekendTextColor
        copy.inactiveTextColor = inactiveTextColor
        copy.inactiveBackgroundColor = inactiveBackgroundColor
        copy.todayInactiveTextColor = todayInactiveTextColor
        copy.todayActiveBackgroundColor = todayActiveBackgroundColor
        copy.font = font
        copy.todayFont = todayFont
        return copy
    }
}

 class DaySymbolsStyle: NSCopying {
     var weekendColor = UIColor.lightGray
     var weekDayColor = UIColor.black
     var font = UIFont.systemFont(ofSize: 10)
     init() {}
     func copy(with zone: NSZone? = nil) -> Any {
        let copy = DaySymbolsStyle()
        copy.weekendColor = weekendColor
        copy.weekDayColor = weekDayColor
        copy.font = font
        return copy
    }
}

 class SwipeLabelStyle: NSCopying {
     var textColor = UIColor.black
     var font = UIFont.systemFont(ofSize: 15)
     init() {}
     func copy(with zone: NSZone? = nil) -> Any {
        let copy = SwipeLabelStyle()
        copy.textColor = textColor
        copy.font = font
        return copy
    }
}
