//
//  GridCell.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 10/28/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class GridCell: UICollectionViewCell {
    @IBOutlet weak var mediaTypeImageView: UIImageView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    var moti: Moti! {
        didSet { setup() }
    }
}

// MARK: - Private

private extension GridCell {
    
    func setup() {
        if let media = moti.media, let type = media.type {
            guard let type = MediaType(rawValue: type) else { return }
            switch type {
            case .video: mediaTypeImageView.image = #imageLiteral(resourceName: "video")
            case .audio: mediaTypeImageView.image = #imageLiteral(resourceName: "audio")
            case .voice: mediaTypeImageView.image = #imageLiteral(resourceName: "voice")
            case .photo: mediaTypeImageView.image = #imageLiteral(resourceName: "photo")
            }
           
            if let thumbnail = media.thumbnail {
                if let url = AppManager.shared.url(for: thumbnail, from: .photo) {
                    do {
                        let imageData = try Data(contentsOf: url)
                        thumbnailImageView.image = UIImage(data: imageData)
                    } catch {
                        print(error.localizedDescription)
                    }
                } else {
                    thumbnailImageView.image = nil
                }
            } else {
                thumbnailImageView.image = nil
            }
        } else {
            mediaTypeImageView.image = #imageLiteral(resourceName: "note")
            thumbnailImageView.image = nil
        }
    }
    
}
