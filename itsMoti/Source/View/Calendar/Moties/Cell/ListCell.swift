//
//  ListCell.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 10/28/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class ListCell: UICollectionViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var mediaTypeImageView: UIImageView!
    
    var moti: Moti! {
        didSet { setup() }
    }
}

// MARK: - Private

private extension ListCell {
    
    func setup() {
        if let media = moti.media, let type = media.type {
            guard let type = MediaType(rawValue: type) else { return }
            switch type {
            case .video: mediaTypeImageView.image = #imageLiteral(resourceName: "video")
            case .audio: mediaTypeImageView.image = #imageLiteral(resourceName: "audio")
            case .voice: mediaTypeImageView.image = #imageLiteral(resourceName: "voice")
            case .photo: mediaTypeImageView.image = #imageLiteral(resourceName: "photo")
            }
        } else {
            mediaTypeImageView.image = #imageLiteral(resourceName: "note")
        }
        
        title.text    = moti.title
        subtitle.text = moti.location?.title
        date.text     = moti.startDate!.string(format: "MMM d")
    }
    
}


