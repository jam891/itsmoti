//
//  MotiesViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import CoreData

class MotiesViewController: UICollectionViewController {
    
    private lazy var fetchedResultsController: NSFetchedResultsController<Moti>! = {
        let fetchRequest: NSFetchRequest<Moti> = Moti.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Moti.startDate), ascending: false)]
        let moc = StorageManager.shared.context
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()

    private var blockOperation = BlockOperation()
    
    var layout: Layout = .list {
        didSet {
            DispatchQueue.main.async {
                self.updateUI()
            }
        }
    }
    
    var date: Date! {
        didSet { filter() }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.invalidateLayout()
    }
    
}

// MARK: - Private

private extension MotiesViewController {
    
    func setup() {
        registerCell()
    }
    
    func registerCell() {
        collectionView.register(cellClass: ListCell.self)
        collectionView.register(cellClass: GridCell.self)
    }
    
    func updateUI() {
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.reloadData()
    }
    
    func filter() {
        var calendar = Calendar.current
        calendar.timeZone = .current
        
        let dateFrom = calendar.startOfDay(for: date)
        let dateTo   = calendar.date(byAdding: .month, value: 1, to: dateFrom)!
        
        let fromPredicate = NSPredicate(format: "startDate >= %@", dateFrom as NSDate)
        let toPredicate   = NSPredicate(format: "startDate < %@", dateTo as NSDate)
        let datePredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [fromPredicate, toPredicate])
        
        fetchedResultsController.fetchRequest.predicate = datePredicate
        performFetch()
        updateUI()
    }
    
    func performFetch() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error.localizedDescription)
        }
    }
    
}

// MARK: - UICollectionViewDataSource

extension MotiesViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedResultsController.fetchedObjects?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch layout {
        case .list:
            let cell = collectionView.dequeueReusableCell(withClass: ListCell.self, for: indexPath)
            cell.moti = fetchedResultsController.object(at: indexPath)
            return cell
        case .grid:
            let cell = collectionView.dequeueReusableCell(withClass: GridCell.self, for: indexPath)
            cell.moti = fetchedResultsController.object(at: indexPath)
            return cell
        }
    }
    
}

// MARK: UICollectionViewDelegate

extension MotiesViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let moti = fetchedResultsController.object(at: indexPath)
        
        if let media = moti.media, let type = media.type, let mediaType = MediaType(rawValue: type) {
            switch mediaType {
            case .photo:
                let controller = UIStoryboard.photoViewController()
                controller.moti = moti
                navigationController?.pushViewController(controller, animated: true)
            case .video:
                let controller = UIStoryboard.videoViewController()
                controller.moti = moti
                navigationController?.pushViewController(controller, animated: true)
            case .audio:
                let controller = UIStoryboard.audioViewController()
                controller.moti = moti
                navigationController?.pushViewController(controller, animated: true)
            case .voice:
                let controller = UIStoryboard.voiceViewController()
                controller.moti = moti
                navigationController?.pushViewController(controller, animated: true)
            }
        } else {
            let controller = UIStoryboard.notesViewController()
            controller.moti = moti
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension MotiesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch layout {
        case .list:
            let itemWidth = view.frame.width
            return CGSize(width: itemWidth, height: 60)
        case .grid:
            let itemWidth = view.frame.width / 3 - 1
            return CGSize(width: itemWidth, height: itemWidth)
        }
    }
    
}

// MARK: - NSFetchedResultsControllerDelegate

extension MotiesViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        blockOperation = BlockOperation()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { break }
            blockOperation.addExecutionBlock {
                self.collectionView?.insertItems(at: [newIndexPath])
            }
        case .delete:
            guard let indexPath = indexPath else { break }
            blockOperation.addExecutionBlock {
                self.collectionView?.deleteItems(at: [indexPath])
            }
        case .update:
            guard let indexPath = indexPath else { break }
            blockOperation.addExecutionBlock {
                self.collectionView?.reloadItems(at: [indexPath])
            }
        case .move:
            guard let indexPath = indexPath, let newIndexPath = newIndexPath else { return }
            blockOperation.addExecutionBlock {
                self.collectionView?.moveItem(at: indexPath, to: newIndexPath)
            }
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView?.performBatchUpdates({
            self.blockOperation.start()
        })
    }

}


