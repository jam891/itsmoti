//
//  Layout.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

enum Layout {
    case list
    case grid
    mutating func toggle() {
        switch self {
        case .list: self = .grid
        case .grid: self = .list
        }
    }
    var image: UIImage {
        switch self {
        case .list: return #imageLiteral(resourceName: "list")
        case .grid: return #imageLiteral(resourceName: "grid")
        }
    }
}
