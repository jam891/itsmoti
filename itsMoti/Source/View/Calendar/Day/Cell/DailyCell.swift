//
//  DailyCell.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 1/25/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit
import CoreData

protocol DailyCellDelegate: class {
    func dailyCell(_ cell: DailyCell, didSelectMoti moti: Moti)
}

class DailyCell: UICollectionViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private lazy var fetchedResultsController: NSFetchedResultsController<Moti>! = {
        let fetchRequest: NSFetchRequest<Moti> = Moti.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Moti.startDate), ascending: false)]
        let moc = StorageManager.shared.context
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    private var blockOperation = BlockOperation()
    
    weak var delegate: DailyCellDelegate?
    
    var layout: Layout = .list

    var date: Date! {
        didSet { filter() }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layoutIfNeeded()
        updateLayout()
    }

}

// MARK: - Private

private extension DailyCell {
    
    func setup() {
        registerCell()
    }
    
    func registerCell() {
        collectionView.register(cellClass: ListCell.self)
        collectionView.register(cellClass: GridCell.self)
    }
    
    func filter() {
        var calendar = Calendar.current
        calendar.timeZone = .current
        
        let dateFrom = calendar.startOfDay(for: date)
        let dateTo   = calendar.date(byAdding: .day, value: 1, to: dateFrom)!
        
        let fromPredicate = NSPredicate(format: "startDate >= %@", dateFrom as NSDate)
        let toPredicate   = NSPredicate(format: "startDate < %@", dateTo as NSDate)
        let datePredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [fromPredicate, toPredicate])
        
        fetchedResultsController.fetchRequest.predicate = datePredicate
        performFetch()
        updateUI()
    }
    
    func performFetch() {
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func updateUI() {
        updateLayout()
        collectionView.reloadData()
    }
    
    func updateLayout() {
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.invalidateLayout()
    }
    
}

// MARK: - UICollectionViewDataSource

extension DailyCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedResultsController.fetchedObjects?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch layout {
        case .list:
            let cell = collectionView.dequeueReusableCell(withClass: ListCell.self, for: indexPath)
            cell.moti = fetchedResultsController.object(at: indexPath)
            return cell
        case .grid:
            let cell = collectionView.dequeueReusableCell(withClass: GridCell.self, for: indexPath)
            cell.moti = fetchedResultsController.object(at: indexPath)
            return cell
        }
    }
    
}

// MARK: UICollectionViewDelegate

extension DailyCell: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let moti = fetchedResultsController.object(at: indexPath)
        delegate?.dailyCell(self, didSelectMoti: moti)
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension DailyCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch layout {
        case .list:
            let itemWidth = collectionView.frame.width
            return CGSize(width: itemWidth, height: 60)
        case .grid:
            let itemWidth = collectionView.frame.width / 3 - 1
            return CGSize(width: itemWidth, height: itemWidth)
        }
    }
    
}

// MARK: - NSFetchedResultsControllerDelegate

extension DailyCell: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        blockOperation = BlockOperation()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            guard let newIndexPath = newIndexPath else { break }
            blockOperation.addExecutionBlock {
                self.collectionView?.insertItems(at: [newIndexPath])
            }
        case .delete:
            guard let indexPath = indexPath else { break }
            blockOperation.addExecutionBlock {
                self.collectionView?.deleteItems(at: [indexPath])
            }
        case .update:
            guard let indexPath = indexPath else { break }
            blockOperation.addExecutionBlock {
                self.collectionView?.reloadItems(at: [indexPath])
            }
        case .move:
            guard let indexPath = indexPath, let newIndexPath = newIndexPath else { return }
            blockOperation.addExecutionBlock {
                self.collectionView?.moveItem(at: indexPath, to: newIndexPath)
            }
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        collectionView?.performBatchUpdates({
            self.blockOperation.start()
        })
    }
    
}
