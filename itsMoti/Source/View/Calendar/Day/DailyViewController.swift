//
//  DailyViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 1/24/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class DailyViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var dayHeaderView: DayHeaderView!
    
    private var contentOffset: CGFloat = 0
    private var visibleDays: [Date] = []
    private var layout: Layout = .list
    private var isScrolling = false

    var state: DayViewState! {
        willSet {
            state?.unsubscribe(client: self)
        }
        didSet {
            state?.subscribe(client: self)
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.navigationBar.hairline?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController!.navigationBar.hairline?.isHidden = false
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateLayout()
        
        collectionView.reloadData()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateLayout()
        
        collectionView.reloadData()
    }

}

// MARK: - Private

private extension DailyViewController {
    
    func setup() {
        if state == nil {
            state = DayViewState()
        }
        dayHeaderView.state = state
        state.move(to: state.selectedDate)
        collectionView.register(cellClass: DailyCell.self)
    }
    
    func scroll(to day: Date, animated: Bool) {
        var index: Int
        if day < state.selectedDate {
            index = 0
        } else {
            index = visibleDays.count - 1
        }
        
        print("index", index)
        
        print("\nbefore:")
        visibleDays.forEach {
            print($0.format(with: .full))
        }
    
        if visibleDays.count > 0 {
            visibleDays[index] = day
            
            print("\nafter:")
            visibleDays.forEach {
                print($0.format(with: .full))
            }
            
            let indexPath = IndexPath(item: index, section: 0)
            collectionView.reloadItems(at: [indexPath])
            collectionView.scrollToItem(at: indexPath, at: .left, animated: animated)
        } else {
            reloadDay(state.selectedDate)
        }
    }
    
    func reloadDay(_ date: Date) {
        var newVisibleDays: [Date] = [date]
        
        let calendar = Calendar.current
        let dateComponents = DateComponents(calendar: calendar, year: 2018, month: 1)
        let startDate = calendar.date(from: dateComponents)!
        let endDate   = calendar.date(byAdding: .year, value: 100, to: startDate)!
        let numberOfDays = calendar.dateComponents([.day], from: startDate, to: endDate).day!
        
        let day = calendar.dateComponents([.day], from: startDate, to: date).day! - 1
        
        if day != 0 {
            let previousDay = calendar.date(byAdding: .day, value: -1, to: date)!
            newVisibleDays.insert(previousDay, at: 0)
        }
        
        if day != numberOfDays {
            let nextDay = calendar.date(byAdding: .day, value: 1, to: date)!
            newVisibleDays.append(nextDay)
        }
        
        visibleDays = newVisibleDays
        collectionView.reloadData()
        
        print("\nreload:")
        visibleDays.forEach {
            print($0.format(with: .full))
        }
//
//        let indexPath = IndexPath(item: visibleDays.index(of: date)!, section: 0)
//        collectionView.scrollToItem(at: indexPath, at: .left, animated: false)
        
        print("visibleDays.index(of: date)!", visibleDays.index(of: date)!)
        
        let indexPath = IndexPath(item: 1, section: 0)
        collectionView.scrollToItem(at: indexPath, at: .left, animated: false)
    }
    
    func configure(_ cell: DailyCell, at indexPath: IndexPath) {
        cell.date = visibleDays[indexPath.row]
        cell.layout = layout
        cell.delegate = self
    }
    
    func updateLayout() {
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.invalidateLayout()
    }
    
}

// MARK: - Actions

extension DailyViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func today(_ sender: UIBarButtonItem) {
        state?.move(to: Date())
    }
    
    @IBAction func layout(_ sender: UIBarButtonItem) {
        layout.toggle()
        sender.image = layout.image
        collectionView.reloadData()
    }
    
}

// MARK: - UICollectionViewDataSource

extension DailyViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return visibleDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: DailyCell.self, for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = cell as? DailyCell else { return }
        configure(cell, at: indexPath)
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension DailyViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
}

// MARK: - DayViewStateUpdating

extension DailyViewController: DayViewStateUpdating {
    
    func move(from oldDate: Date, to newDate: Date) {
        if !isScrolling {
            reloadDay(newDate)
            if newDate != state.selectedDate {
                scroll(to: newDate, animated: true)
            }
        }
    }
    
}

// MARK: - UIScrollViewDelegate

extension DailyViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isScrolling = true
        contentOffset = scrollView.contentOffset.x
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let calendar = Calendar.current
        if contentOffset < scrollView.contentOffset.x {
            let day = calendar.date(byAdding: .day, value: 1, to: state.selectedDate)!
            state.move(to: day)
            reloadDay(day)
        } else if contentOffset > scrollView.contentOffset.x {
            let day = calendar.date(byAdding: .day, value: -1, to: state.selectedDate)!
            state.move(to: day)
            reloadDay(day)
        } else {
            print("didn't move")
        }
        isScrolling = false
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        print(#function)
    }
    
}

// MARK: - EventCellDelegate

extension DailyViewController: DailyCellDelegate {
    
    func dailyCell(_ cell: DailyCell, didSelectMoti moti: Moti) {
        if let media = moti.media, let type = media.type, let mediaType = MediaType(rawValue: type) {
            switch mediaType {
            case .photo:
                let controller = UIStoryboard.photoViewController()
                controller.moti = moti
                navigationController?.pushViewController(controller, animated: true)
            case .video:
                let controller = UIStoryboard.videoViewController()
                controller.moti = moti
                navigationController?.pushViewController(controller, animated: true)
            case .audio:
                let controller = UIStoryboard.audioViewController()
                controller.moti = moti
                navigationController?.pushViewController(controller, animated: true)
            case .voice:
                let controller = UIStoryboard.voiceViewController()
                controller.moti = moti
                navigationController?.pushViewController(controller, animated: true)
            }
        } else {
            let controller = UIStoryboard.notesViewController()
            controller.moti = moti
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
}





