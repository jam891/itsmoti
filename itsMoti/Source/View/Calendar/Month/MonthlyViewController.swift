//
//  MonthlyViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import JTAppleCalendar

class MonthlyViewController: UIViewController {
    
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    private var motiesViewController: MotiesViewController {
        let controller = children.first(where: { $0 is MotiesViewController })
        return controller as! MotiesViewController
    }
    
    private var date: Date!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let date = date {
            calendarView.scrollToDate(date, triggerScrollToDateDelegate: true, animateScroll: false)
        }
        navigationController!.navigationBar.hairline?.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isBeingPresented {
            updateUI()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController!.navigationBar.hairline?.isHidden = false
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        calendarView.viewWillTransition(to: .zero, with: coordinator, anchorDate: date)
    }

}

// MARK: - Navigation

extension MonthlyViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segueIdentifier(for: segue) else { return }
        switch identifier {
        case .daily:
            let controller = segue.destination as! DailyViewController
            controller.state = DayViewState(date: sender as! Date)
        default:
            break
        }
    }
    
}

// MARK: - Private

private extension MonthlyViewController {
    
    func setup() {
//        SyncManager.shared.synchronize()
        
        calendarView.scrollDirection = .horizontal
        calendarView.scrollToDate(Date(), triggerScrollToDateDelegate: true, animateScroll: false)
    }
    
    func updateTitle() {
        guard let startDate = calendarView.visibleDates().monthDates.first?.date else { return }
        var components = Calendar.current.dateComponents([.year, .month], from: startDate)
        let year = Calendar.current.component(.year, from: startDate)
        let symbol = DateFormatter().monthSymbols[components.month! - 1]
        title = String(format: "%d %@", year, symbol)
    }
    
    func updateUI() {
        updateTitle()
        adjustCalendarViewHeight()
        view.layoutIfNeeded()
    }
    
    func configureCell(cell: JTAppleCell?, cellState: CellState) {
        guard let cell = cell as? DayCell else { return }
        cell.cellState = cellState
    }
    
    func adjustCalendarViewHeight() {
        let higher = calendarView.visibleDates().outdates.count < 7
        topConstraint.constant = higher ? 0 : -calendarView.frame.height / 6
    }
    
}

// MARK: - Action

extension MonthlyViewController {
    
    @IBAction func unwindToCalendar(_ segue: UIStoryboardSegue) {}
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @IBAction func layout(_ sender: UIBarButtonItem) {
        sender.image = motiesViewController.layout.image
        motiesViewController.layout.toggle()
    }
    
    @IBAction func today(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: .daily, sender: Date())
    }
    
}

// MARK: - JTAppleCalendarViewDataSource

extension MonthlyViewController: JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let calendar = Calendar.current
        let dateComponents = DateComponents(calendar: calendar, year: 2018, month: 1)
        
        let startDate = calendar.date(from: dateComponents)!
        let endDate   = calendar.date(byAdding: .year, value: 100, to: startDate)!
        let firstDayOfWeek = DaysOfWeek(rawValue: calendar.firstWeekday)!
        
        return ConfigurationParameters(startDate: startDate, endDate: endDate, firstDayOfWeek: firstDayOfWeek)
    }
    
}

// MARK: - JTAppleCalendarViewDelegate

extension MonthlyViewController: JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configureCell(cell: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "DayCell", for: indexPath)
        configureCell(cell: cell, cellState: cellState)
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        date = visibleDates.monthDates.first!.date
        motiesViewController.date = date
        updateUI()
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(cell: cell, cellState: cellState)
        performSegue(withIdentifier: .daily, sender: date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configureCell(cell: cell, cellState: cellState)
    }
    
}
