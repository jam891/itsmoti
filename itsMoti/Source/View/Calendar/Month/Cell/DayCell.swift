//
//  DayCell.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import JTAppleCalendar

class DayCell: JTAppleCell {
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var selectedView: UIView!
    
    var cellState: CellState! {
        didSet { setup() }
    }
}

private extension DayCell {
    
    func setup() {
        setupDayLabel()
        setupEventView()
        setupSelectedView()
        
    }
    
    func setupDayLabel() {
        dayLabel.text = cellState.text
        
        if cellState.date.isToday {
            dayLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        } else {
            if cellState.day == .sunday || cellState.day == .saturday {
                dayLabel.textColor = #colorLiteral(red: 0.6352941176, green: 0.6352941176, blue: 0.6549019608, alpha: 1)
            } else {
                dayLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
    }
    
    func setupSelectedView() {
        selectedView.isHidden = !cellState.date.isToday

        if cellState.date.isToday {
            selectedView.backgroundColor = #colorLiteral(red: 1, green: 0.68033427, blue: 0, alpha: 1)
        }
        
        isHidden = cellState.dateBelongsTo != .thisMonth
    }
    
    func setupEventView() {
        let fromDate = cellState.date
        let toDate = Calendar.current.date(byAdding: .day, value: 1, to: fromDate)!
        
        let fromPredicate = NSPredicate(format: "startDate >= %@", fromDate as NSDate)
        let toPredicate   = NSPredicate(format: "startDate < %@", toDate as NSDate)
        let datePredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [fromPredicate, toPredicate])
        
        let moties = StorageManager.shared.fetch(Moti.self, predicate: datePredicate) ?? []
        
        if moties.count > 0 {
            eventView.isHidden = false
        } else {
            eventView.isHidden = true
        }
    }
    
}
