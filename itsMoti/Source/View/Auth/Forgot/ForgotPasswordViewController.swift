//
//  ForgotPasswordViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/9/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import TweeTextField

class ForgotPasswordViewController: AuthViewController {
    
    @IBOutlet weak var emailTextField: TweeAttributedTextField!

    private var formIsValid: Bool {
        return validate()
    }
    
}

// MARK: - Actions

extension ForgotPasswordViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @IBAction func reset(_ sender: UIBarButtonItem) {
        resetPassword()
    }
    
}

// MARK: - ForgotPasswordViewController

private extension ForgotPasswordViewController {
    
    func resetPassword() {
        if reachability.isReachable {
            if formIsValid {
                dismissKeyboard()
                showLoadingView()
                API.auth.resetPassword(emailTextField.text!.trim()) { error in
                    self.hideLoadingView()
                    if let error = error {
                        Alert.showErrorAlert(error)
                    } else {
                        let message = "Success, an email was sent to the entered address with verification code."
                        Alert.showSuccessAlert(message) {
                            self.dismiss(animated: true)
                        }
                    }
                }
            }
        } else {
            showNotificationBar()
        }
    }
    
    func validate() -> Bool {
        if emailTextField.text!.trim().isEmpty {
            emailTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
            emailTextField.showInfo("Please enter your email.")
        }
        if !emailTextField.text!.trim().isEmail {
            emailTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
            emailTextField.showInfo("Invalid email address.")
        }
        return !emailTextField.text!.trim().isEmpty && emailTextField.text!.trim().isEmail
    }
    
}
