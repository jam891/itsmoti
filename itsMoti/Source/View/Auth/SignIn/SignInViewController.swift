//
//  SignInViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import TweeTextField

class SignInViewController: AuthViewController {
    
    @IBOutlet weak var usernameTextField: TweeAttributedTextField!
    @IBOutlet weak var passwordTextField: TweeAttributedTextField!
    
    private var formIsValid: Bool {
        return validate()
    }

}

// MARK: - Actions

extension SignInViewController {
    
    @IBAction func enter(_ sender: UIButton) {
        signIn()
    }
    
}

// MARK: - Private

private extension SignInViewController {
    
    func signIn() {
        if reachability.isReachable {
            if formIsValid {
                dismissKeyboard()
                showLoadingView()
                API.auth.signIn(username: usernameTextField.text!.trim(),
                                password: passwordTextField.text!.trim()) { error in
                    self.hideLoadingView()
                    if let error = error {
                        Alert.showErrorAlert(error)
                    } else {
                        self.performSegue(withIdentifier: .home)
                    }
                }
            }
        } else {
            showNotificationBar()
        }
    }
    
    func validate() -> Bool {
        if usernameTextField.text!.trim().isEmpty {
            usernameTextField.showInfo("Please enter username or email address.")
        }
        if passwordTextField.text!.trim().isEmpty {
            passwordTextField.showInfo("Please enter your password.")
        }
        return !usernameTextField.text!.trim().isEmpty && !passwordTextField.text!.trim().isEmpty
    }
    
}

// MARK: - UITextFieldDelegate

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTextField:
            passwordTextField.becomeFirstResponder()
        default:
            signIn()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn
        range: NSRange, replacementString string: String) -> Bool {
        guard let textField = textField as? TweeAttributedTextField else { return false }
        textField.hideInfo()
        return true
    }
    
}


