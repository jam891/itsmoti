//
//  WelcomeViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet weak var startButton: UIButton!
}

// MARK: - Actions

extension WelcomeViewController {
    
    @IBAction func unwindToWelcome(_ segue: UIStoryboardSegue) {}
    
    @IBAction func start(_ sender: UIButton) {
        showAlertSheet()
    }
    
}

// MARK: - Private

private extension WelcomeViewController {
    
    func showAlertSheet() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Trial Version", style: .default) { _ in
            self.performSegue(withIdentifier: .signUp)
        })
        alert.addAction(UIAlertAction(title: "Subscribe Now", style: .default) { _ in
            
        })
        alert.addAction(UIAlertAction(title: "Received a Gift?", style: .default) { _ in
            self.performSegue(withIdentifier: .signIn)
        })
        alert.addAction(UIAlertAction(title: "Already Subscribed?", style: .default) { _ in
            self.performSegue(withIdentifier: .signIn)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.view.layoutIfNeeded()
        present(alert)
    }
    
    func present(_ alert: UIAlertController) {
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = startButton
            popoverController.sourceRect = CGRect(x: startButton.bounds.midX, y: startButton.bounds.maxY - 170, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
}

