//
//  SignUpViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import TweeTextField
import PhoneNumberKit

class SignUpViewController: AuthViewController {
    
    @IBOutlet weak var usernameTextField: TweeAttributedTextField!
    @IBOutlet weak var passwordTextField: TweeAttributedTextField!
    @IBOutlet weak var emailTextField: TweeAttributedTextField!
    @IBOutlet weak var phoneTextField: PhoneNumberTextField!
    @IBOutlet weak var checkmarkButton: UIButton!
    @IBOutlet weak var warningLabel: UILabel!

    private let phoneNumberKit = PhoneNumberKit()
    private var textFields: [Field: Bool]! = [
        .username: false,
        .password: false,
        .email: false,
        .phone: false
    ]
    private var formIsValid: Bool {
        return validate()
    }
    
}

// MARK: - Private

private extension SignUpViewController {
    
    func signUp() {
        if reachability.isReachable {
            if formIsValid {
                dismissKeyboard()
                showLoadingView()
                API.auth.signUp(username: usernameTextField.text!.trim(),
                                password: passwordTextField.text!.trim(),
                                email: emailTextField.text!.trim(),
                                phoneNumber: formattedPhoneNumber(phoneTextField.text!.trim())!) { error in
                    self.hideLoadingView()
                    if let error = error {
                        Alert.showErrorAlert(error)
                    } else {
                        let message = "Success, you registered. We've sent an email to the email address entered. " +
                                      "You must confirm registration by clicking on the link in the sent email."
                        
                        Alert.showSuccessAlert(message) {
                            self.performSegue(withIdentifier: .signIn)
                        }
                    }
                }
            }
        } else {
            showNotificationBar()
        }
    }
    
    func validate() -> Bool {
        if usernameTextField.text!.trim().isEmpty {
            textFields[.username] = false
            usernameTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
            usernameTextField.showInfo("Please enter username.")
        }
        if passwordTextField.text!.trim().isEmpty {
            textFields[.password] = false
            passwordTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
            passwordTextField.showInfo("Please enter your password.")
        }
        if emailTextField.text!.trim().isEmpty {
            textFields[.email] = false
            emailTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
            emailTextField.showInfo("Please enter your email.")
        }
        if phoneTextField.text!.trim().isEmpty {
            textFields[.phone] = false
            phoneTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
            phoneTextField.showInfo("Please enter your phone.")
        }
        if !checkmarkButton.isSelected {
            warningLabel.isHidden = false
        }
        
        var formIsValid = true
        for isValid in textFields.values {
            guard isValid else {
                formIsValid = false
                break
            }
        }
        return formIsValid && checkmarkButton.isSelected
    }
    
    func formattedPhoneNumber(_ text: String) -> String? {
        do {
            let phoneNumber = try phoneNumberKit.parse(text)
            var digits = phoneNumberKit.format(phoneNumber, toType: .e164)
            digits.remove(at: digits.startIndex)
            return digits
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }

}

// MARK: - Actions

extension SignUpViewController {
    
    @IBAction func checkmark(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0.1, options: [], animations: {
            sender.isSelected = !sender.isSelected
        }) { success in
            if sender.isSelected {
                self.warningLabel.isHidden = true
            }
        }
    }
    
    @IBAction func enter(_ sender: UIButton) {
        signUp()
    }
    
}

// MARK: - UITextFieldDelegate

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTextField:
            emailTextField.becomeFirstResponder()
        case emailTextField:
            phoneTextField.becomeFirstResponder()
        case phoneTextField:
            passwordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textField = textField as? TweeAttributedTextField else { return false }
        textField.hideInfo()
        
        switch textField {
        case usernameTextField:
            if string.count > 0 {
                let allowedCharacters = CharacterSet.alphanumerics
                return string.trimmingCharacters(in: allowedCharacters).count == 0 && range.location < 21
            }
            return true
        case passwordTextField:
            return range.location < 21
        default:
            break
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let textField = textField as? TweeAttributedTextField else { return }
        textField.hideInfo()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text?.trim() else { return }
        if text.count > 0 {
            switch textField {
            case usernameTextField:
                if text.count >= 3 {
                    API.auth.validate(text) { isAvailable, error in
                        if let error = error {
                            Alert.showErrorAlert(error)
                        } else {
                            if isAvailable {
                                self.usernameTextField.infoTextColor = #colorLiteral(red: 0.5607843137, green: 0.8117647059, blue: 0.7176470588, alpha: 1)
                                self.usernameTextField.showInfo("Username is available.")
                            } else {
                                self.usernameTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
                                self.usernameTextField.showInfo("Username is taken.")
                            }
                            self.textFields[.username] = isAvailable
                        }
                    }
                } else {
                    textFields[.username] = false
                    usernameTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
                    usernameTextField.showInfo("Username is too short (3-20 alphanumeric characters).")
                }
            case passwordTextField:
                if text.count >= 6 {
                    textFields[.password] = true
                    let strength = Strength(password: text)
                    switch strength {
                    case .strong, .veryStrong, .reasonable:
                        self.passwordTextField.infoTextColor = #colorLiteral(red: 0.5607843137, green: 0.8117647059, blue: 0.7176470588, alpha: 1)
                    default:
                        self.passwordTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
                    }
                    self.passwordTextField.showInfo(strength.description)
                } else {
                    textFields[.password] = false
                    passwordTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
                    passwordTextField.showInfo("Password must be at least 6-20 characters long.")
                }
            case emailTextField:
                if text.isEmail {
                    API.auth.validate(text) { isAvailable, error in
                        if let error = error {
                            Alert.showErrorAlert(error)
                        } else {
                            if isAvailable {
                                self.emailTextField.infoTextColor = #colorLiteral(red: 0.5607843137, green: 0.8117647059, blue: 0.7176470588, alpha: 1)
                                self.emailTextField.showInfo("Email is available.")
                            } else {
                                self.emailTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
                                self.emailTextField.showInfo("Email is taken.")
                            }
                            self.textFields[.email] = isAvailable
                        }
                    }
                } else {
                    textFields[.email] = false
                    emailTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
                    emailTextField.showInfo("Invalid email address.")
                }
            case phoneTextField:
                if text.count > 10 {
                    do {
                        let phoneNumber = try phoneNumberKit.parse(text)
                        var digits = phoneNumberKit.format(phoneNumber, toType: .e164)
                        digits.remove(at: digits.startIndex)
                        API.auth.validate(digits) { isAvailable, error in
                            if let error = error {
                                Alert.showErrorAlert(error)
                            } else {
                                if isAvailable {
                                    self.phoneTextField.infoTextColor = #colorLiteral(red: 0.5607843137, green: 0.8117647059, blue: 0.7176470588, alpha: 1)
                                    self.phoneTextField.showInfo("Phone number is available.")
                                } else {
                                    self.phoneTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
                                    self.phoneTextField.showInfo("Email is taken.")
                                }
                                self.textFields[.phone] = isAvailable
                            }
                        }
                    }
                    catch {
                        textFields[.phone] = false
                        phoneTextField.infoTextColor = #colorLiteral(red: 0.9215686275, green: 0.6745098039, blue: 0.3803921569, alpha: 1)
                        phoneTextField.showInfo("Invalid phone number.")
                    }
                }
            default:
                break
            }
        }
    }
    
}
