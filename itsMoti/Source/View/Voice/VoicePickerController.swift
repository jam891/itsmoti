//
//  VoicePickerController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import AVKit

protocol VoicePickerControllerDelegate: class {
    func voicePickerControllerDidCancel(_ picker: VoicePickerController)
    func voicePickerController(_ picker: VoicePickerController, didFinishPickingMedia asset: AVAsset)
}

class VoicePickerController: UITableViewController {
    
    weak var delegate: VoicePickerControllerDelegate?
    private lazy var playback = Playback()
    private var voices: [URL] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let _ = playback.audioPlayer {
            if playback.audioPlayer.isPlaying {
                playback.state = .default
            }
        }
    }
    
    deinit {
        print("VoicePickerController deinit")
    }

}

// MARK: - Actions

extension VoicePickerController {
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        delegate?.voicePickerControllerDidCancel(self)
    }
    
}

// MARK: - Private

private extension VoicePickerController {
    
    func setup() {
        voices = AppManager.shared.all(pathExtension: .m4a, from: .voice)
    }
    
    func configure(_ cell: VoiceCell, at indexPath: IndexPath) {
        let asset = AVAsset(url: voices[indexPath.row])
        cell.delegate = self
        cell.asset = asset
    }

}

// MARK: - UITableViewDataSource

extension VoicePickerController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return voices.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: VoiceCell.self, for: indexPath)
        configure(cell, at: indexPath)
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension VoicePickerController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let asset = AVAsset(url: voices[indexPath.row])
        delegate?.voicePickerController(self, didFinishPickingMedia: asset)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let voice = voices[indexPath.row]
            
            guard let index = voices.index(of: voice) else { return }
            do {
                try FileManager.default.removeItem(at: voice)
                voices.remove(at: index)
                tableView.deleteRows(at: [indexPath], with: .fade)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
}

// MARK: - VoiceTableViewCellDelegate

extension VoicePickerController: VoiceCellDelegate {
   
    func voiceCell(_ cell: VoiceCell, didTapPlay sender: UIButton) {
        if cell.urlAsset.url == playback.voice {
            switch playback.state {
            case .play:
                playback.state = .pause
            default:
                playback.state = .play
            }
        } else {
            let url = cell.urlAsset.url
            
            do {
                try playback.audioPlayer = AVAudioPlayer(contentsOf: url)
                playback.audioPlayer?.delegate = self

                let progressView = playback.progressView
                progressView.tintColor = #colorLiteral(red: 0.5607843137, green: 0.8117647059, blue: 0.7176470588, alpha: 1)
                progressView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                progressView.frame = cell.progressView.bounds
                progressView.setProgress(0.0, animated: false)
                cell.progressView.addSubview(progressView)
                
                playback.voice = url
                playback.state = .play
            } catch {
                playback.state = .default
            }
        }
    }
    
}

// MARK: - AVAudioPlayerDelegate

extension VoicePickerController: AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag {
            playback.state = .finish
        }
    }
    
}





