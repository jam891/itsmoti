//
//  VoiceCell.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 1/9/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

import AVKit

protocol VoiceCellDelegate: class {
    func voiceCell(_ cell: VoiceCell, didTapPlay sender: UIButton)
}


class VoiceCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var progressView: CircularProgressView!
    
    
    weak var delegate: VoiceCellDelegate?
    
    var asset: AVAsset! {
        didSet { setup() }
    }
    
    var urlAsset: AVURLAsset {
        return asset as! AVURLAsset
    }

}

// MARK: - Private

private extension VoiceCell {
    
    func setup() {
        titleLabel.text    = urlAsset.url.lastPathComponent
        durationLabel.text = asset.duration.seconds.stringValue
        
        guard
            let attributes = try? FileManager.default.attributesOfItem(atPath: urlAsset.url.path),
            let creationDate = attributes[FileAttributeKey.creationDate] as? Date
            else { return }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        dateLabel.text = dateFormatter.string(from: creationDate)
    }
    
}

// MARK: - Action

extension VoiceCell {
    
    @IBAction func play(_ sender: UIButton) {
        delegate?.voiceCell(self, didTapPlay: sender)
    }
    
}
