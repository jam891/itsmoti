//
//  AddVoiceViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/29/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer

class AddVoiceViewController: UIViewController {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    
    @IBOutlet weak var voiceButton: UIBarButtonItem!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var rangeSlider: RangeSlider!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    private var asset: AVAsset!
    private var player: AVPlayer!
    private var timer: CADisplayLink!
    private var lowerValue: Double = 0
    private var playerItem: AVPlayerItem!
    private var thumbnailURL: URL?
    private var toolBar: UIToolbar {
        return navigationController!.toolbar
    }
    
    var moti: Moti!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateProgress(0)
        updateUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if moti.created == nil && isMovingToParent {
            showVoiceMenu()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let _ = player {
            if player.isPlaying {
                stopPlayback()
            }
        }
    }
    
    deinit {
        print("AddVoiceViewController deinit")
    }

}

// MARK: - Navigation

extension AddVoiceViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segueIdentifier(for: segue) else { return }
        switch identifier {
        case .editPhoto:
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! EditPhotoViewController
            controller.image = sender as? UIImage
            controller.delegate = self
        default:
            break
        }
    }
    
}

// MARK: - Private

private extension AddVoiceViewController {
    
    func setup() {
        setTitle(lhs: "Voice", rhs: "Moti")
        
        if let moti = moti, let media = moti.media, let resource = media.resource {
            guard let url = AppManager.shared.url(for: resource, from: .voice) else { return }
            loadVoice(asset: AVAsset(url: url))
            
            if let thumbnail = media.thumbnail {
                guard let url = AppManager.shared.url(for: thumbnail, from: .photo) else { return }
                do {
                    let imageData = try Data(contentsOf: url)
                    imageView.image = UIImage(data: imageData)
                } catch {
                    print(error.localizedDescription)
                }
            }
        } else {
            moti = StorageManager.shared.insert(Moti.self)
        }
        registeredNotification()
    }
    
    func registeredNotification() {
        NotificationCenter.addObserver(self, selector: #selector(playerItemDidReachEnd), name: .AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    
    func loadVoice(asset: AVAsset) {
        if player != nil {
            player = nil
        }
        playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)
        
        startTimeLabel.text   = "0:00.000"
        currentTimeLabel.text = "0:00"
        endTimeLabel.text     = asset.duration.seconds.formatted
        
        setupRangeSlider()
        
        self.asset = asset
    }
    
    func setupRangeSlider() {
        rangeSlider.lowerValue = 0
        rangeSlider.upperValue = 1
        rangeSlider.progress   = 0
    }
    
    func updateProgress(_ progress: Float) {
        if 0.001..<1 ~= progress {
            progressView.isHidden  = false
            progressLabel.isHidden = false
            nextButton.isEnabled   = false
        } else {
            progressView.isHidden  = true
            progressLabel.isHidden = true
            nextButton.isEnabled   = true
        }
        progressView.progress = progress
    }
    
    func updateUI() {
        if let _ = asset {
            nextButton.isEnabled = true
        } else {
            nextButton.isEnabled = false
        }
        
        if let _ = imageView.image {
            editButton.isHidden  = false
        } else {
            editButton.isHidden  = true
        }
        
        if let _ = player {
            var items = toolBar.items
            let item: UIBarButtonItem.SystemItem = player.isPlaying ? .pause : .play
            items![5] = UIBarButtonItem(barButtonSystemItem: item, target: self, action: #selector(play))
            toolBar.setItems(items, animated: true)
        }
    }
    
    func openLibrary() {
        Permission.photos.request { status in
            if status == .authorized {
                DispatchQueue.main.async {
                    MediaPicker.open(.photoLibrary, delegate: self)
                }
            }
        }
    }
    
    func openCamera() {
        Permission.camera.request { status in
            if status == .authorized {
                DispatchQueue.main.async {
                    MediaPicker.open(.photoCamera, delegate: self)
                }
            }
        }
    }
    
    func openRecorder() {
        Permission.microphone.request { status in
            if status == .authorized {
                DispatchQueue.main.async {
                    MediaPicker.open(.voiceRecord, delegate: self)
                }
            }
        }
    }
    
    func openVoiceList() {
        DispatchQueue.main.async {
            MediaPicker.open(.voiceLibrary, delegate: self)
        }
    }
    
    func showVoiceMenu() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Voice Recorder", style: .default, handler: { _ in
            self.openRecorder()
        }))
        alert.addAction(UIAlertAction(title: "Voice Recorder Library", style: .default, handler: { _ in
            self.openVoiceList()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.permittedArrowDirections = .any
            popoverController.barButtonItem = voiceButton
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func showPhotoMenu() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default) { _ in
            self.openCamera()
        })
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default) { _ in
            self.openLibrary()
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            let sourceRect = CGRect(x: photoButton.bounds.midX, y: photoButton.bounds.minY + 120, width: 0, height: 0)
            popoverController.permittedArrowDirections = .any
            popoverController.sourceView = photoButton
            popoverController.sourceRect = sourceRect
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func showDeleteMenu() {
        
    }
    
    func startPlayback() {
        player.play()
        startTimer()
        updateUI()
    }
    
    func stopPlayback() {
        player.pause()
        stopTimer()
        updateUI()
    }
    
    func startTimer() {
        stopTimer()
        timer = CADisplayLink(target: self, selector: #selector(updateTimer))
        timer.add(to: .current, forMode: .default)
    }
    
    func stopTimer() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
    }
    
    func trim(asset: AVAsset) {
        let startTime = CMTime(seconds: rangeSlider.lowerValue * asset.duration.seconds, preferredTimescale: 1000000)
        let endTime   = CMTime(seconds: rangeSlider.upperValue * asset.duration.seconds, preferredTimescale: 1000000)
        
        MediaManager.shared.trimAudio(asset, startTime: startTime, endTime: endTime, { progress in
            DispatchQueue.main.async {
                self.updateProgress(progress)
            }
        }) { audioURL in
            DispatchQueue.global(qos: .default).async {
                var imageURL: URL?
                if let thumbnailURL = self.thumbnailURL {
                    imageURL = thumbnailURL
                } else {
                    if let thumbnail = self.moti.media?.thumbnail, thumbnail != "null" {
                        guard let url = AppManager.shared.url(for: thumbnail, from: .photo) else { return }
                        imageURL = url
                    }
                }
                
                let mediaModel = MediaModel(type: .voice,
                                            resource: audioURL.absoluteString,
                                            thumbnail: imageURL?.absoluteString,
                                            size: "")
                
                self.moti.update(mediaModel: mediaModel)
                
                DispatchQueue.main.async {
                    self.updateProgress(1)
                    self.push()
                }
            }
        }
    }
    
    func push() {
        let controller = UIStoryboard.motiViewController()
        controller.setTitle(lhs: "Voice", rhs: "Moti")
        controller.moti  = moti
        navigationController?.pushViewController(controller, animated: true)
    }
    
}

// MARK: - Notification

@objc
extension AddVoiceViewController {
    
    func playerItemDidReachEnd(_ notification: Notification) {
        if let _ = player {
            let startTime = CMTime(seconds: rangeSlider.lowerValue * asset.duration.seconds, preferredTimescale: 1000000)
            currentTimeLabel.text = startTime.seconds.formattedCurrent
            rangeSlider.progress  = rangeSlider.lowerValue
            player.seek(to: startTime)
            stopPlayback()
        }
    }
    
    func updateTimer(_ timer: Timer) {
        let startTime = CMTime(seconds: rangeSlider.lowerValue * asset.duration.seconds, preferredTimescale: 1000000)
        let endTime   = CMTime(seconds: rangeSlider.upperValue * asset.duration.seconds, preferredTimescale: 1000000)
        
        if player.currentTime() >= endTime {
            rangeSlider.progress  = rangeSlider.lowerValue
            currentTimeLabel.text = startTime.seconds.formattedCurrent
            player.seek(to: startTime)
            stopPlayback()
        } else {
            currentTimeLabel.text = player.currentTime().seconds.formattedCurrent
            rangeSlider.progress  = player.currentTime().seconds / asset.duration.seconds
        }
    }
    
}

// MARK: - Action

extension AddVoiceViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        StorageManager.shared.context.rollback()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func photo(_ sender: UIBarButtonItem) {
        showPhotoMenu()
    }
    
    @IBAction func edit(_ sender: UIButton) {
        performSegue(withIdentifier: .editPhoto, sender: imageView.image)
    }
    
    @IBAction func voice(_ sender: UIBarButtonItem) {
        showVoiceMenu()
    }
    
    @IBAction func clear(_ sender: UIBarButtonItem) {
        showDeleteMenu()
    }
    
    @IBAction func next(_ sender: UIBarButtonItem) {
        trim(asset: asset)
    }
    
    @IBAction func play(_ sender: UIBarButtonItem) {
        if let player = player {
            player.isPlaying ? stopPlayback() : startPlayback()
        } else {
            guard let asset = asset else { return }
            loadVoice(asset: asset)
            play(sender)
        }
    }
    
    @IBAction func valueChanged(_ sender: RangeSlider) {
        if let asset = asset {
            let startTime = sender.lowerValue * asset.duration.seconds
            let endTime   = sender.upperValue * asset.duration.seconds
            
            startTimeLabel.text = startTime.formatted
            endTimeLabel.text   = endTime.formatted
            
            if lowerValue != sender.lowerValue {
                lowerValue = sender.lowerValue
                
                sender.progress = sender.lowerValue
                
                let currentTime = CMTime(seconds: startTime, preferredTimescale: 1000000)
                player.seek(to: currentTime, toleranceBefore: .zero, toleranceAfter: .zero)
                currentTimeLabel.text = currentTime.seconds.formattedCurrent
            }
        }
    }
    
}

// MARK: - VoicePickerControllerDelegate

extension AddVoiceViewController: VoicePickerControllerDelegate {
    
    func voicePickerControllerDidCancel(_ picker: VoicePickerController) {
        picker.dismiss(animated: true)
    }
    
    func voicePickerController(_ picker: VoicePickerController, didFinishPickingMedia asset: AVAsset) {
        picker.dismiss(animated: true)
        loadVoice(asset: asset)
    }
    
}


// MARK: - RecordPickerControllerDelegate

extension AddVoiceViewController: RecordPickerControllerDelegate {
    
    func recordPickerControllerDidCancel(_ picker: RecordPickerController) {
        picker.dismiss(animated: true)
    }
    
    func recordPickerController(_ picker: RecordPickerController, didFinishPickingMedia asset: AVAsset) {
        picker.dismiss(animated: true)
        loadVoice(asset: asset)
    }

}

// MARK: - UIImagePickerControllerDelegate

extension AddVoiceViewController: UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        
        guard let originalImage = info[.originalImage] as? UIImage else { return }
        let image = MediaManager.shared.addWatermark(to: originalImage)
        imageView.image = image
        updateUI()
        
        if let data = image.pngData() {
            if let imageURL = AppManager.shared.writeToTemporaryDirectoryImageData(data) {
                thumbnailURL = imageURL
            }
        }
    }
    
}

// MARK: - UINavigationControllerDelegate

extension AddVoiceViewController: UINavigationControllerDelegate {}

// MARK: - EditPhotoViewControllerDelegate

extension AddVoiceViewController: EditPhotoViewControllerDelegate {
    
    func editPhotoControllerDidCancel(_ controller: EditPhotoViewController) {
        controller.dismiss(animated: true)
    }
    
    func editPhotoController(_ controller: EditPhotoViewController, photoDidEndEditing photo: UIImage) {
        controller.dismiss(animated: true)
        imageView?.image = photo
        
        if let data = photo.pngData() {
            if let imageURL = AppManager.shared.writeToTemporaryDirectoryImageData(data) {
                thumbnailURL = imageURL
            }
        }
    }
    
}
