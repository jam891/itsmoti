//
//  MotiViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import EventKit

class MotiViewController: TableViewController {
    
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var locationCell: LocationCell!
    
    @IBOutlet weak var startsLabel: UILabel!
    @IBOutlet weak var snoozeLabel: UILabel!
    @IBOutlet weak var repeatLabel: UILabel!
    @IBOutlet weak var endRepeatLabel: UILabel!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var timePicker: UIPickerView!
    
    private var todoViewController: TodoViewController {
        let controller = children.first(where: { $0 is TodoViewController })
        return controller as! TodoViewController
    }
    
    private var datePickerVisible = false {
        didSet { updateUI() }
    }
    
    private var timePickerVisible = false {
        didSet { updateUI() }
    }
    
    private let pickerData = [
        "0", "5", "10", "15", "20",
        "25", "30", "35", "40", "45",
        "50", "55", "60", "65", "70",
        "75", "80", "85", "90"
    ]

    var moti: Moti!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        todoViewController.isEditable = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        todoViewController.isEditable = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isMovingToParent {
            if textField.text!.isEmpty {
                textField.becomeFirstResponder()
            }
        }
    }
  
}

// MARK: - Navigation

extension MotiViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segueIdentifier(for: segue) else { return }
        
        switch identifier {
        case .location:
            guard let navigationController = segue.destination as? UINavigationController else { return }
            let controller = navigationController.topViewController as! LocationPickerController
            controller.delegate = self
        case .endRepeat:
            let controller = segue.destination as! EndRepeatViewController
            controller.date = sender as? Date ?? nil
            controller.didSelectDate = { date in
                if let date = date {
                    self.endRepeatLabel.text = date.string(format: "MMM d, yyyy")
                    if let rruleString = self.moti.recurrenceRule {
                        if var recurrenceRule = RecurrenceRule(rruleString: rruleString) {
                            recurrenceRule.recurrenceEnd = EKRecurrenceEnd(end: date)
                            self.moti.recurrenceRule = recurrenceRule.toRRuleString()
                        }
                    }
                } else {
                    self.endRepeatLabel.text = "Never"
                    if let rruleString = self.moti.recurrenceRule {
                        if var recurrenceRule = RecurrenceRule(rruleString: rruleString) {
                            recurrenceRule.recurrenceEnd = nil
                            self.moti.recurrenceRule = recurrenceRule.toRRuleString()
                        }
                    }
                }
            }
        default:
            break
        }
    }
    
}

// MARK: - Private

private extension MotiViewController {
    
    func setup() {
        if let moti = moti {
            textField.text        = moti.title
            locationCell.location = moti.location
        } else {
            setTitle(lhs: "Note/List", rhs: "Moti")
            moti = StorageManager.shared.insert(Moti.self)
        }
    
        saveButton.isEnabled = !(moti.title ?? "").trim().isEmpty
        updateStartsDate(moti.startDate ?? Date())
        updateSnooze(moti.snooze ?? "0")
        todoViewController.moti = moti
        updateRepeatLabels()
    }
    
    func updateStartsDate(_ date: Date) {
        datePicker.date  = date
        startsLabel.text = date.string(format: "MMM d, yyyy  h:mm a")
    }
    
    func updateRepeatLabels() {
        print(#function)

        if let recurrenceRule = moti.recurrenceRule {
            if let rule = RecurrenceRule(rruleString: recurrenceRule) {
                repeatLabel.text = stringFormatter(rule)
                if let recurrenceEnd = rule.recurrenceEnd, let date = recurrenceEnd.endDate {
                    endRepeatLabel.text = date.string(format: "MMM d, yyyy")
                } else {
                    endRepeatLabel.text = "Never"
                }
            }
        } else {
            repeatLabel.text = "Never"
        }
    }
    
    func updateSnooze(_ value: String) {
        snoozeLabel.text = String(format: "%@ min", value)
    }
    
    func updateUI() {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func remove() {
        moti.startDate = nil
        
        if moti.id != nil {
            delete()
        } else {
            StorageManager.shared.delete(moti)
            StorageManager.shared.saveContext()
        }
        performSegue(withIdentifier: .home)
    }
    
    func save() {
        if let media = moti.media {
            updateMedia(media)
        }
        
        if moti.created == nil {
            moti.startDate = datePicker.date
            moti.modified  = Date()
            moti.endDate   = Date()
            moti.created   = Date()
            create()
        } else {
            moti.modified = Date()
            update()
        }
        performSegue(withIdentifier: .home)
    }
    
    func updateMedia(_ media: Media) {
        if media.isUpdated {
            let committedValues = media.committedValues(forKeys: ["thumbnail", "resource"])
            
            if let resource = committedValues["resource"] as? String {
                guard let type = media.type, let directory = Directory(rawValue: type) else { return }
                AppManager.shared.delete(filename: resource, from: directory)
            }
            if let thumbnail = committedValues["thumbnail"] as? String {
                guard let type = media.type else { return }
                if type != MediaType.photo.rawValue {
                    AppManager.shared.delete(filename: thumbnail, from: .photo)
                }
            }
        }
        StorageManager.shared.append(media)
    }
    
    func showAlert() {
        let title   = NSLocalizedString("Are you sure you want to delete the moti '\(moti.title!.trim())'?", comment: "")
        let message = NSLocalizedString("Doing so will permanently remove this Moti from your calendar. This decision cannot be undone.", comment: "")
        let alert   = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive) { [unowned self] _ in
           self.remove()
        })
        if let popoverController = alert.popoverPresentationController {
            popoverController.barButtonItem = navigationItem.rightBarButtonItem
            popoverController.permittedArrowDirections = .up
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func showRepeat() {
        print(#function)
        
        var recurrenceRule: RecurrenceRule?
        if let rruleString = moti.recurrenceRule {
            recurrenceRule = RecurrenceRule(rruleString: rruleString)
        }
       
        let picker = RecurrencePicker(recurrenceRule: recurrenceRule)
        picker.delegate = self
        picker.tintColor = .black
        picker.language = .english
        picker.occurrenceDate = datePicker.date
        navigationController?.pushViewController(picker, animated: true)
    }
    
    func showEndRepeat() {
        if let recurrenceRule = moti.recurrenceRule,
            let rule = RecurrenceRule(rruleString: recurrenceRule),
            let recurrenceEnd = rule.recurrenceEnd {
            performSegue(withIdentifier: .endRepeat, sender: recurrenceEnd.endDate)
        } else {
            performSegue(withIdentifier: .endRepeat, sender: nil)
        }
    }
    
    func stringFormatter(_ recurrenceRule: RecurrenceRule?) -> String {
        if let recurrenceRule = recurrenceRule {
            switch recurrenceRule.interval {
            case 1:
                switch recurrenceRule.frequency {
                case .daily:
                    return "Daily"
                case .weekly:
                    return "Weekly"
                case .monthly:
                    return "Monthly"
                case .yearly:
                    return "Yearly"
                default:
                    return "Custom"
                }
            case 2:
                let calendar = Calendar.current
                let weekday = EKWeekday(rawValue: calendar.component(.weekday, from: Date()))!
                
                if recurrenceRule.frequency == .weekly && recurrenceRule.byweekday.count == 1 && recurrenceRule.byweekday.first! == weekday {
                    return "BiWeekly"
                } else {
                    return "Custom"
                }
            default:
                return "Custom"
            }
        } else {
            return "Never"
        }
    }
    
}

// MARK: -

private extension MotiViewController {
    
    func create() {
        API.post.create(moti) { error in
            if let error = error {
                print(error.localizedDescription)
            }
            StorageManager.shared.saveContext()
        }
    }
    
    func update() {
        API.post.update(moti) { error in
            if let error = error {
                print(error.localizedDescription)
            }
            StorageManager.shared.saveContext()
        }
    }
    
    func delete() {
        API.post.delete(moti) { error in
            if let error = error {
                print(error.localizedDescription)
            }
            StorageManager.shared.saveContext()
        }
    }

}

// MARK: - Action

extension MotiViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        if moti.created != nil {
            StorageManager.shared.context.rollback()
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        moti.startDate = sender.date
        updateStartsDate(sender.date)
    }
    
    @IBAction func clear(_ sender: UIBarButtonItem) {
        showAlert()
    }
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        save()
    }
    
}

// MARK: - UITableViewDelegate

extension MotiViewController {
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 18
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        switch indexPath.section {
        case 0:
            if indexPath.row == 1 {
                performSegue(withIdentifier: .location)
            }
        case 1:
            switch indexPath.row {
            case 0:
                if timePickerVisible {
                    timePickerVisible.toggle()
                }
                datePickerVisible.toggle()
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            case 2:
                showRepeat()
            case 3:
                showEndRepeat()
            case 4:
                if datePickerVisible {
                    datePickerVisible.toggle()
                }
                timePickerVisible.toggle()
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            default:
                break
            }
        default:
            break
        }
        
        if textField.isFirstResponder {
            textField.resignFirstResponder()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 1:
            if indexPath.row == 1 {
                if !datePickerVisible { return 0 }
            }
            if indexPath.row == 3 {
                if let recurrenceRule = moti.recurrenceRule, let _ = RecurrenceRule(rruleString: recurrenceRule) {
                    break
                } else {
                    return 0
                }
            }
            if indexPath.row == 5 {
                if !timePickerVisible { return 0 }
            }
        case 3:
            if moti.created == nil { return 0 }
        default:
            break
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
}

// MARK: - LocationPickerControllerDelegate

extension MotiViewController: LocationPickerControllerDelegate {
    
    func locationPickerControllerDidCancel(_ picker: LocationPickerController) {
        picker.dismiss(animated: true)
    }
    
    func locationPickerController(_ picker: LocationPickerController, didFinishPickingLocation locationModel: LocationModel) {
        picker.dismiss(animated: true)
        moti.update(locationModel: locationModel)
        locationCell.location = moti.location
    }
    
}

// MARK: - UIPickerViewDataSource

extension MotiViewController: UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
}

// MARK: - UIPickerViewDelegate

extension MotiViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        moti.snooze = pickerData[row]
        updateSnooze(pickerData[row])
    }
    
}

// MARK: - UITextFieldDelegate

extension MotiViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            let title = text.replacingCharacters(in: Range(range, in: text)!, with: string)
            saveButton.isEnabled = !title.trim().isEmpty
            moti.title = title
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        saveButton.isEnabled = false
        return true
    }
    
}

// MARK: - RecurrencePickerDelegate

extension MotiViewController: RecurrencePickerDelegate {
    
    func recurrencePicker(_ picker: RecurrencePicker, didPickRecurrence recurrenceRule: RecurrenceRule?) {
        print(#function)
        
        if let rruleString = moti.recurrenceRule,
            var rule = RecurrenceRule(rruleString: rruleString),
            let recurrenceRule = recurrenceRule,
            let recurrenceEnd = rule.recurrenceEnd {
            print("if .")
            rule = recurrenceRule
            rule.recurrenceEnd = recurrenceEnd
            moti.recurrenceRule = rule.toRRuleString()
        } else {
            print("else .")
            moti.recurrenceRule = recurrenceRule?.toRRuleString()
        }

        updateRepeatLabels()
        tableView.reloadData()
    }
    
}

