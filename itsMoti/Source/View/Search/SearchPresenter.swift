//
//  SearchPresenter.swift
//  itsMoti
//
//  Created by Александр Васильченко on 1/31/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import Foundation

// MARK: -
// MARK: Search View Protocol

protocol SearchViewPresenter {
    init(view: SearchView)
    
    //TODO: - Maybe better send Error instead Bool
    func shareMotis(_ moti: Moti, for users: [User], completion: @escaping (Error?)->())
}

class SearchPresenter: SearchViewPresenter {
    unowned let view: SearchView
    
    required init(view: SearchView) {
        self.view = view
    }
    
    func shareMotis(_ moti: Moti, for users: [User], completion: @escaping (Error?) -> ()) {
        API.post.share(moti, for: users) { (error) in
            completion(error)
        }
    }
}

