//
//  SearchFactory.swift
//  itsMoti
//
//  Created by Александр Васильченко on 1/31/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

struct SearchFactory {
  private init() {}
  
    static func make(_ moti: Moti) -> UIViewController {
    return makeSearchViewController(moti)
  }
}

private extension SearchFactory {
  static func makeSearchViewController(_ moti: Moti) -> UIViewController {
    let storyBoard = UIStoryboard(name: "SearchViewController", bundle: nil)
    
    guard let navigation = storyBoard.instantiateInitialViewController() as? UINavigationController else {
      return UINavigationController()
    }
    
    guard let view = navigation.topViewController as? SearchViewController else {
      return UINavigationController()
    }
    
    let presenter = SearchPresenter(view: view)
    view.presenter = presenter
    view.moti = moti
    
    return navigation
  }
}
