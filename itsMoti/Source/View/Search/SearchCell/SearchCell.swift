//
//  SearchCell.swift
//  itsMoti
//
//  Created by Александр Васильченко on 1/31/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var checkBoxImage: UIImageView!
    
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
//                checkBoxButton.setBackgroundImage(#imageLiteral(resourceName: "select"), for: UIControl.State.normal)
            } else {
//                checkBoxButton.setBackgroundImage(#imageLiteral(resourceName: "reveal"), for: UIControl.State.normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        checkBoxImage.image = selected ? #imageLiteral(resourceName: "select") : #imageLiteral(resourceName: "reveal")
        
    }
    
    @IBAction func checkBoxButtonClicked(_ sender: Any) {
        isChecked = !isChecked
    }
}
