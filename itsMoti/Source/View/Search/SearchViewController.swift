//
//  SearchViewController.swift
//  itsMoti
//
//  Created by Александр Васильченко on 1/31/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit
import SwiftMessages

class SearchViewController: UIViewController {
    
    // MARK: - Properties

    @IBOutlet weak var tableView: UITableView!
    private let backgroundView = BackgroundView()
    
    var presenter: SearchPresenter!
    let searchController = UISearchController(searchResultsController: nil)
    var moti: Moti!
    var reachability: Reachability!
    var oldSearchString = ""
    
    private var toolBar: UIToolbar {
        return navigationController!.toolbar
    }
    
    private var searchedData = [User]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    @IBAction func sendMotis(_ sender: UIBarButtonItem) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        searchController.isActive = false
        
        if reachability.isReachable {
            let indexPathes = tableView.indexPathsForSelectedRows!
            var chosenUsers = [User]()
            
            for indexPath in indexPathes {
                chosenUsers.append(searchedData[indexPath.row])
            }
            
            presenter.shareMotis(self.moti, for: chosenUsers) { [weak self] (completed) in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                self?.dismiss(animated: true, completion: nil)
                print(completed)
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            showNotificationBar()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
    }
}

// MARK: - Actions

extension SearchViewController {
    @objc func cancel() {
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Private

private extension SearchViewController {
    func setup() {
        setupSearchBar()
        setupReachability()
        setupTableView()
        
        let backItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
        navigationItem.title = "Share"
        navigationItem.leftBarButtonItem = backItem
    }
    
    func setupTableView() {
        tableView.backgroundView = backgroundView
        tableView.register(UINib(nibName: "SearchCell", bundle: nil), forCellReuseIdentifier: "SearchCell")
        tableView.rowHeight = 65
        tableView.tableFooterView = UIView()
    }
    
    func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.delegate = self
        searchController.searchBar.delegate = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.tintColor = UIColor.black
        searchController.searchBar.returnKeyType = .done
        definesPresentationContext = true
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.searchController = searchController
    }
    
    func setupReachability() {
        reachability = Reachability()
        do {
            try reachability.startNotifier()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    @objc func dismissKeyboard() {
        searchController.searchBar.endEditing(true)
    }
    
    func showNotificationBar() {
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureContent(body: "No Internet Connection!")
        view.configureTheme(backgroundColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), foregroundColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1))
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.preferredStatusBarStyle = .lightContent
        config.presentationStyle = .top
        config.shouldAutorotate = true
        config.duration = .automatic
        
        SwiftMessages.show(config: config, view: view)
    }
    
    func setStateSendButton(count: Int = 0) {
        toolBar.items![1].isEnabled = count > 0
        toolBar.barTintColor = count > 0 ? #colorLiteral(red: 0.2078431373, green: 0.8274509804, blue: 0.7137254902, alpha: 1) : nil
        toolBar.items![1].tintColor = count > 0 ? UIColor.white : UIColor.black
    }
}

// MARK: - UITableViewDataSource

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView?.isHidden = searchedData.count != 0
        
        return searchedData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchCell = tableView.dequeueReusableCell(withClass: SearchCell.self, for: indexPath)
        
        let user = searchedData[indexPath.row]
        searchCell.nameLabel.text = user.username
        searchCell.iconImageView.setImageForName(user.username, backgroundColor: nil, circular: true, textAttributes: nil)
        
        return searchCell
    }
}

// MARK: - UITableViewDelegate

extension SearchViewController: UITableViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        dismissKeyboard()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let count = tableView.indexPathsForSelectedRows?.count ?? 0
        searchController.searchBar.resignFirstResponder()
        setStateSendButton(count: count)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let count = tableView.indexPathsForSelectedRows?.count ?? 0
 
        setStateSendButton(count: count)
    }
}

// MARK: - SearchView

extension SearchViewController: SearchView {
   
}

// MARK: - UISearchResultsUpdating

extension SearchViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchString = searchController.searchBar.text?.trim(), searchString.count > 0 else {
            oldSearchString = ""
            
            return
        }

        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        if reachability.isReachable {
            API.post.search(searchString) { (users, error)  in
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                if self.oldSearchString != searchString {
                    self.oldSearchString = searchString
                    self.searchedData = users
                }
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            showNotificationBar()
        }
    }
}

// MARK: - UISearchBarDelegate

extension SearchViewController: UISearchControllerDelegate, UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        setStateSendButton()
    }
}

