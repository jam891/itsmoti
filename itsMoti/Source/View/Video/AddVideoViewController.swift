//
//  AddVideoViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/28/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import AVKit
import PryntTrimmerView
import MobileCoreServices

class AddVideoViewController: UIViewController {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    
    @IBOutlet weak var videoButton: UIBarButtonItem!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    
    @IBOutlet weak var trimmerView: TrimmerView!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playerView: PlayerView!

    private var asset: AVAsset!
    private var player: AVPlayer!
    private var timer: CADisplayLink!
    private var playerLayer: AVPlayerLayer!
    private var toolBar: UIToolbar {
        return navigationController!.toolbar
    }
    
    var moti: Moti!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateProgress(0)
        updateUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if moti.created == nil && isMovingToParent {
            showVideoMenu()
        } else {
            guard let _ = asset else { return }
            loadTrimmer(asset: asset)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let _ = player {
            if player.isPlaying { stopPlayback() }
            player = nil
        }
    }
    
}

// MARK: - Private

private extension AddVideoViewController {
    
    func setup() {
        setTitle(lhs: "Video", rhs: "Moti")
        
        if let moti = moti, let media = moti.media, let resource = media.resource {
            guard let url = AppManager.shared.url(for: resource, from: .video) else { return }
            asset = AVAsset(url: url)
            loadVideo(asset: asset)
        } else {
            moti = StorageManager.shared.insert(Moti.self)
        }
        trimmerView.maxDuration = 120
        trimmerView.delegate = self
        registeredNotification()
    }
    
    func registeredNotification() {
        NotificationCenter.addObserver(self, selector: #selector(playerItemDidReachEnd), name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    func loadVideo(asset: AVAsset) {
        let playerItem = AVPlayerItem(asset: asset)
        
        player = AVPlayer(playerItem: playerItem)
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = playerView.bounds
        playerLayer.videoGravity = .resizeAspect
        
        playerView.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        playerView.layer.addSublayer(playerLayer)
        playerView.playerLayer = playerLayer
    }
    
    func loadTrimmer(asset: AVAsset) {
        trimmerView.asset   = asset
        startTimeLabel.text = trimmerView.startTime?.seconds.formatted
        endTimeLabel.text   = trimmerView.endTime?.seconds.formatted
    }
    
    func startPlayback() {
        player.play()
        startTimer()
        updateUI()
    }
    
    func stopPlayback() {
        player.pause()
        stopTimer()
        updateUI()
    }
    
    func startTimer() {
        stopTimer()
        timer = CADisplayLink(target: self, selector: #selector(updateTimer))
        timer.add(to: .current, forMode: .default)
    }
    
    func stopTimer() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
    }
    
    func updateProgress(_ progress: Float) {
        if 0.001..<1 ~= progress {
            progressView.isHidden  = false
            progressLabel.isHidden = false
            nextButton.isEnabled   = false
        } else {
            progressView.isHidden  = true
            progressLabel.isHidden = true
            nextButton.isEnabled   = true
        }
        progressView.progress = progress
    }
    
    func updateUI() {
        if let _ = playerLayer {
            trimmerView.isHidden    = false
            nextButton.isEnabled    = true
            startTimeLabel.isHidden = false
            endTimeLabel.isHidden   = false
        } else {
            trimmerView.isHidden    = true
            nextButton.isEnabled    = false
            startTimeLabel.isHidden = true
            endTimeLabel.isHidden   = true
        }
        if let _ = player {
            let item: UIBarButtonItem.SystemItem = player.isPlaying ? .pause : .play
            toolBar.items![5] = UIBarButtonItem(barButtonSystemItem: item, target: self, action: #selector(play))
            
            playButton.isHidden = player.isPlaying
        }
    }
    
    func openLibrary() {
        Permission.photos.request { status in
            if status == .authorized {
                DispatchQueue.main.async {
                    MediaPicker.open(.videoLibrary, delegate: self)
                }
            }
        }
    }
    
    func openCamera() {
        Permission.camera.request { status in
            if status == .authorized {
                DispatchQueue.main.async {
                    MediaPicker.open(.videoCamera, delegate: self)
                }
            }
        }
    }
    
    func showVideoMenu() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Video", style: .default) { action in
            self.openCamera()
        })
        alert.addAction(UIAlertAction(title: "Video Library", style: .default) { action in
            self.openLibrary()
        })
        alert.addAction(UIAlertAction(title: "Instagram/Youtube URL", style: .default) { action in
            MediaPicker.open(.socialVideo, delegate: self)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { [weak self] action in
            _ = self?.navigationController?.popViewController(animated: true)
        }))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.permittedArrowDirections = .any
            popoverController.barButtonItem = videoButton
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func showDeleteMenu() {
        
    }
    
    func trim(asset: AVAsset) {
        MediaManager.shared.trimVideo(asset,
                                 startTime: trimmerView.startTime!,
                                 endTime: trimmerView.endTime!, { progress in
                                    DispatchQueue.main.async {
                                        self.updateProgress(progress)
                                    }
        }) { videoURL in
            DispatchQueue.global(qos: .default).async {
                let asset = AVAsset(url: videoURL)
                MediaManager.shared.thumbnail(from: asset) { thumbnail in
                    let mediaModel = MediaModel(type: .video,
                                                resource: videoURL.lastPathComponent,
                                                thumbnail: thumbnail?.lastPathComponent,
                                                size: "")
                    
                    self.moti.update(mediaModel: mediaModel)
                    DispatchQueue.main.async {
                        self.push()
                    }
                }
            }
        }
    }
    
    func push() {
        let controller = UIStoryboard.motiViewController()
        controller.setTitle(lhs: "Video", rhs: "Moti")
        controller.moti  = moti
        navigationController?.pushViewController(controller, animated: true)
    }
    
}

// MARK: - Notification

@objc
extension AddVideoViewController {
    
    func playerItemDidReachEnd(_ notification: Notification) {
        if let _ = player {
            player.seek(to: .zero)
            stopPlayback()
        }
    }
    
    func updateTimer(_ timer: Timer) {
        if let player = player {
            let startTime   = trimmerView.startTime ?? .zero
            let endTime     = trimmerView.endTime ?? .zero
            let currentTime = player.currentTime()
            
            trimmerView.seek(to: currentTime)
            
            if currentTime >= endTime {
                player.seek(to: startTime)
                trimmerView.seek(to: startTime)
            }
        }
    }
    
}

// MARK: - Action

extension AddVideoViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        StorageManager.shared.context.rollback()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func video(_ sender: UIBarButtonItem) {
        showVideoMenu()
    }
    
    @IBAction func clear(_ sender: UIBarButtonItem) {
        showDeleteMenu()
    }
    
    @IBAction func next(_ sender: UIBarButtonItem) {
        trim(asset: asset)
    }
    
    @IBAction func play(_ sender: UIBarButtonItem) {
        if let player = player {
            player.isPlaying ? stopPlayback() : startPlayback()
        } else {
            guard let asset = asset else { return }
            loadVideo(asset: asset)
            play(sender)
        }
    }
    
    @IBAction func stop(_ sender: UITapGestureRecognizer) {
        if let player = player, player.isPlaying {
            stopPlayback()
        }
    }
    
}

// MARK: - TrimmerViewDelegate

extension AddVideoViewController: TrimmerViewDelegate {
    
    func didChangePositionBar(_ playerTime: CMTime) {
        guard let player = player else { return }
        player.pause()
        
        toolBar.items![5] = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(play))
        
        startTimeLabel.text = trimmerView.startTime?.seconds.formatted
        endTimeLabel.text   = trimmerView.endTime?.seconds.formatted
        player.seek(to: playerTime, toleranceBefore: .zero, toleranceAfter: .zero)
    }
    
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        guard let player = player else { return }
        player.seek(to: playerTime, toleranceBefore: .zero, toleranceAfter: .zero)
    }
    
}

// MARK: - UIImagePickerControllerDelegate

extension AddVideoViewController: UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        
        guard
            let mediaType = info[.mediaType] as? String,
            let videoURL  = info[.mediaURL] as? URL else { return }
        
        if mediaType == kUTTypeMovie as String {
            asset = AVAsset(url: videoURL)
            loadTrimmer(asset: asset)
            loadVideo(asset: asset)
            updateUI()
        }
    }
    
}

// MARK: - UINavigationControllerDelegate

extension AddVideoViewController: UINavigationControllerDelegate {}

// MARK: - SocialPickerControllerDelegate

extension AddVideoViewController: VideoPickerControllerDelegate {
    
    func videoPickerControllerDidCancel(_ picker: VideoPickerController) {
        picker.dismiss(animated: true)
    }
    
    func videoPickerController(_ picker: VideoPickerController, didFinishPickingMedia asset: AVAsset) {
        picker.dismiss(animated: true)
        self.asset = asset
        loadTrimmer(asset: self.asset)
        loadVideo(asset: self.asset)
        updateUI()
    }

}

