//
//  VideoPickerController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 1/9/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit
import AVKit
import TweeTextField

protocol VideoPickerControllerDelegate: class {
    func videoPickerControllerDidCancel(_ picker: VideoPickerController)
    func videoPickerController(_ picker: VideoPickerController, didFinishPickingMedia asset: AVAsset)
}

class VideoPickerController: UITableViewController {
    
    @IBOutlet weak var urlTextField: TweeAttributedTextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var tableViewCell: UITableViewCell!
    
    weak var delegate: VideoPickerControllerDelegate?
    
    private var isLoading: Bool = false
    private var video: Video!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}

// MARK: - Private

private extension VideoPickerController {
    
    func setup() {
        tableViewCell.isHidden = true
        urlTextField.delegate = self
        
        DownloadManager.shared.onProgress = { progress, totalSize in
            DispatchQueue.main.async {
                self.downloadButton.isEnabled = false
                self.downloadButton.backgroundColor = #colorLiteral(red: 0.8000000119, green: 0.8000000119, blue: 0.8000000119, alpha: 1)
                self.urlTextField.isEnabled = false
                self.progressView.progress = progress
                self.progressLabel.text = String(format: "%.1f%% of %@", progress * 100, totalSize)
            }
        }
        
        DownloadManager.shared.onFinished = { location in
            DispatchQueue.main.async {
                self.downloadButton.isEnabled = true
                self.downloadButton.backgroundColor = #colorLiteral(red: 0.2078431373, green: 0.8274509804, blue: 0.7137254902, alpha: 1)
                self.urlTextField.isEnabled = true
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            let videoURL = AppManager.shared
                .url(for: .video)
                .appendingPathComponent(UUID().uuidString)
                .appendingPathExtension(.mp4)

            AppManager.shared.moveItem(atPath: location.path, toPath: videoURL.path)
            
            let url = AppManager.shared.url(filename: videoURL.lastPathComponent, for: .video)
            self.delegate?.videoPickerController(self, didFinishPickingMedia: AVAsset(url: url!))
        }
        
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Download Video",
                                                message: nil,
                                                preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            if let link = alert.textFields?[0].text {
                if link.count > 10 {
                    if let url = URL(string: link) {
                        self.startDownload(url)
                    }
                } else {
                    Alert.showErrorAlert("URL too short to be valid")
                }
            }
        })
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        alert.addTextField() { textField in
            textField.placeholder = "Enter video URL"
            textField.keyboardType = .URL
            textField.becomeFirstResponder()
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func startDownload(_ url: URL) {
        DownloadManager.shared.startDownload(url) { [unowned self] video, error in
            if let error = error {
                Alert.showErrorAlert(error.localizedDescription)
            } else {
                DispatchQueue.main.async {
                    if let video = video {
                        self.animate()
                        self.video = video
                        self.isLoading = true
                        self.titleLabel.text = video.title
                        self.tableViewCell.isHidden = false
                        UIApplication.shared.isNetworkActivityIndicatorVisible = true
                    }
                }
            }
        }
    }
    
    func animate() {
        tableViewCell.alpha = 0
        UIView.animate(withDuration: 0.1) {
            self.tableViewCell.alpha = 1
        }
    }
    
}

// MARK: - UITextFieldDelegate

extension VideoPickerController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        guard let count = urlTextField.text?.count  else {
            return true
        }

        downloadButton.isEnabled = count > 10 || string.count > 10 ? true : false
        downloadButton.backgroundColor =  downloadButton.isEnabled ? #colorLiteral(red: 0.2078431373, green: 0.8274509804, blue: 0.7137254902, alpha: 1) : #colorLiteral(red: 0.8000000119, green: 0.8000000119, blue: 0.8000000119, alpha: 1)
        if string == "" && textField.text?.count == 1 {
            downloadButton.isEnabled = false
            downloadButton.backgroundColor =  #colorLiteral(red: 0.8000000119, green: 0.8000000119, blue: 0.8000000119, alpha: 1)
        }
        
        return true
    }
}

// MARK: - Action

extension VideoPickerController {
    
    @IBAction func download(_ sender: UIButton) {
        if let url = URL(string: urlTextField.text!) {
            self.startDownload(url)
        }
    }
    
    @IBAction func cancelDownload(_ sender: UIButton) {
        DownloadManager.shared.cancel()
        downloadButton.isEnabled = true
        downloadButton.backgroundColor = downloadButton.isEnabled ? #colorLiteral(red: 0.2078431373, green: 0.8274509804, blue: 0.7137254902, alpha: 1) : #colorLiteral(red: 0.8000000119, green: 0.8000000119, blue: 0.8000000119, alpha: 1)
        tableViewCell.isHidden = true
        urlTextField.isEnabled = true
    }
    
    @IBAction func resumeDownload(_ sender: UIButton) {
        DownloadManager.shared.resume()
        
        isLoading.toggle()
        
        if isLoading {
            pauseButton.setTitle("Pause", for: .normal)
        } else {
            pauseButton.setTitle("Resume", for: .normal)
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        delegate?.videoPickerControllerDidCancel(self)
    }
    
}

// MARK: - UITableViewDelegate

extension VideoPickerController {
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }
    
}






