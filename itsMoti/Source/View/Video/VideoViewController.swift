//
//  VideoViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/28/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import AVKit

class VideoViewController: MediaViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet var height: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playerView: PlayerView!
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var durationTimeLabel: UILabel!
    
    private var todoViewController: TodoViewController {
        let controller = children.first(where: { $0 is TodoViewController })
        return controller as! TodoViewController
    }
    
    private var asset: AVAsset!
    private var player: AVPlayer!
    private var timer: CADisplayLink!
    private var playerLayer: AVPlayerLayer!
    private var toolBar: UIToolbar {
        return navigationController!.toolbar
    }
    
    var moti: Moti!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let _ = player {
            if player.isPlaying { stopPlayback() }
            player = nil
        }
    }

}

// MARK: - Navigation

extension VideoViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segueIdentifier(for: segue) else { return }
        switch identifier {
        case .addVideo:
            let controller = segue.destination as! AddVideoViewController
            controller.moti = moti
        default:
            break
        }
    }
    
}

// MARK: - Private

private extension VideoViewController {
    
    func setup() {
        setTitle(lhs: "Video", rhs: "Moti")
        
        guard let moti = moti, let media = moti.media else { return }
        
        if let resource = media.resource {
            guard let url = AppManager.shared.url(for: resource, from: .video) else { return }
            asset = AVAsset(url: url)
            loadVideo(asset: asset)
        }
        
        todoViewController.moti = moti
        registeredNotification()
    }
    
    func registeredNotification() {
        NotificationCenter.addObserver(self, selector: #selector(playerItemDidReachEnd), name: .AVPlayerItemDidPlayToEndTime, object: nil)
    }
    
    func loadVideo(asset: AVAsset) {
        let playerItem = AVPlayerItem(asset: asset)
        
        player = AVPlayer(playerItem: playerItem)
        
        seek()
    
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspect
        playerLayer.frame = playerView.bounds
        
        playerView.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
        playerView.layer.addSublayer(playerLayer)
        playerView.playerLayer = playerLayer
        
        updateTimeMarks()
    }
    
    func startPlayback() {
        player.play()
        startTimer()
        updateUI()
    }
    
    func stopPlayback() {
        player.pause()
        stopTimer()
        updateUI()
    }
    
    func startTimer() {
        stopTimer()
        timer = CADisplayLink(target: self, selector: #selector(updateTimer))
        timer.add(to: .current, forMode: .default)
    }
    
    func stopTimer() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
    }
    
    func updateTimeMarks() {
        let currentTime = Double(slider.value) * asset.duration.seconds
        
        currentTimeLabel.text  = currentTime.stringValue
        durationTimeLabel.text = asset.duration.seconds.stringValue
    }
    
    func updateUI() {
        if let _ = player {
            var items = toolBar.items
            let item: UIBarButtonItem.SystemItem = player.isPlaying ? .pause : .play
            items![3] = UIBarButtonItem(barButtonSystemItem: item, target: self, action: #selector(play))
            toolBar.setItems(items, animated: true)
            
            playButton.isHidden = player.isPlaying
        }
    }
    
    func seek() {
        let seconds     = Double(slider.value) * asset.duration.seconds
        let currentTime = CMTime(seconds: seconds, preferredTimescale: 1000000)
        player.seek(to: currentTime)
    }
    
    func showAlert() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alert.addAction(UIAlertAction(title: "Share via itsMoti", style: .default) { _ in
            self.present(SearchFactory.make(self.moti), animated: true)
        })
        alert.addAction(UIAlertAction(title: "Share", style: .default) { _ in
            self.share()
        })
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.barButtonItem = navigationItem.rightBarButtonItem
            popoverController.permittedArrowDirections = .up
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func share() {
        let title     = moti.title ?? ""
        let startDate = moti.startDate?.string(format: "MMM d, yyyy  h:mm a") ?? ""
        let location  = moti.location?.title ?? ""
        let notes     = moti.body ?? ""
        
        var activityItems = [title, startDate, location, notes] as [Any]
        
        if let media = moti.media, let resource = media.resource {
            let directory = Directory(rawValue: media.type!)
            let url = AppManager.shared.url(filename: resource, for: directory!)!
            activityItems.append(url)
        }
        
        let activity = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activity.excludedActivityTypes = [
            .print,
            .airDrop,
            .openInIBooks,
            .assignToContact,
            .saveToCameraRoll,
            .addToReadingList,
            .copyToPasteboard
        ]
        
        if let popoverController = activity.popoverPresentationController {
            popoverController.barButtonItem = navigationItem.rightBarButtonItem
            popoverController.permittedArrowDirections = .up
        }
        
        present(activity, animated: true)
    }
    
    func shareVia() {
        
    }
    
}

// MARK: - Notification

@objc
extension VideoViewController {
    
    override func orientationDidChange(_ notification: Notification) {
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight:
            height.isActive = false
            stackView.removeArrangedSubview(containerView)
        case .portrait, .portraitUpsideDown:
            height.isActive = true
            stackView.addArrangedSubview(containerView)
        default:
            break
        }
        stackView.setNeedsLayout()
        stackView.layoutIfNeeded()
        updateUI()
    }
    
    func playerItemDidReachEnd(_ notification: Notification) {
        currentTimeLabel.text = "00:00"
        player.seek(to: .zero)
        slider.value = 0
        stopPlayback()
    }
    
    func updateTimer(_ timer: Timer) {
        if !slider.isTracking {
            let position = player.currentTime().seconds
            let duration = asset.duration.seconds
            slider.value = Float(position / duration)
        }
        updateTimeMarks()
    }
    
}

// MARK: - Action

extension VideoViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func play(_ sender: UIBarButtonItem) {
        if let player = player {
            player.isPlaying ? stopPlayback() : startPlayback()
        } else {
            guard let asset = asset else { return }
            loadVideo(asset: asset)
            play(sender)
        }
    }
    
    @IBAction func stop(_ sender: UITapGestureRecognizer) {
        if let player = player, player.isPlaying {
            stopPlayback()
        }
    }
    
    @IBAction func valueChanged(_ sender: UISlider) {
        if !sender.isTracking {
            seek()
        }
        updateTimeMarks()
    }
    
    @IBAction func edit(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: .addVideo)
    }
    
    @IBAction func share(_ sender: UIBarButtonItem) {
        showAlert()
    }
    
}
