//
//  EndRepeatViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 1/15/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class EndRepeatViewController: UITableViewController {
    
    @IBOutlet weak var neverCell: UITableViewCell!
    @IBOutlet weak var onDateCell: UITableViewCell!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var didSelectDate: ((_ date: Date?) -> Void)?
    
    private var datePickerVisible = false {
        didSet { updateUI() }
    }
    
    var date: Date?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}

// MARK: - Private

private extension  EndRepeatViewController {
    
    func setup() {
        if let date = date {
            datePicker.date = date
        }
        datePickerVisible        = date != nil
        neverCell.accessoryType  = date != nil ? .none : .checkmark
        onDateCell.accessoryType = date != nil ? .checkmark : .none
    }
    
    func updateUI() {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
}

// MARK: - Action

extension EndRepeatViewController {
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        date = sender.date
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        didSelectDate?(date)
        navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - UITableViewDelegate

extension EndRepeatViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0:
            date = nil
            datePickerVisible = false
        case 1:
            datePickerVisible = true
        default:
            break
        }
        
        tableView.visibleCells.forEach {
            if indexPath == tableView.indexPath(for: $0) {
                $0.accessoryType = .checkmark
            } else {
                $0.accessoryType = .none
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 2:
            if !datePickerVisible { return 0 }
        default:
            break
        }
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
}
