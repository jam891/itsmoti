//
//  NotesViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 12/2/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class NotesViewController: UIViewController {

    private var todoViewController: TodoViewController {
        let controller = children.first(where: { $0 is TodoViewController })
        return controller as! TodoViewController
    }

    var moti: Moti!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
}

// MARK: - Navigation

extension NotesViewController {
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard let identifier = segueIdentifier(for: segue) else { return }
//        switch identifier {
//        default:
//            break
//        }
//    }
    
}

// MARK: - Private

private extension NotesViewController {
    
    func setup() {
        setTitle(lhs: "Note/List", rhs: "Moti")
        
        guard let moti = moti else { return }

        todoViewController.moti = moti
    }
    
    
    func push() {
        let controller = UIStoryboard.motiViewController()
        controller.setTitle(lhs: "Note/List", rhs: "Moti")
        controller.moti = moti
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func showAlert() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alert.addAction(UIAlertAction(title: "Share via itsMoti", style: .default) { _ in
            self.present(SearchFactory.make(self.moti), animated: true)
        })
        alert.addAction(UIAlertAction(title: "Share", style: .default) { _ in
            self.share()
        })
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.barButtonItem = navigationItem.rightBarButtonItem
            popoverController.permittedArrowDirections = .up
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func share() {
        let title     = moti.title ?? ""
        let startDate = moti.startDate?.string(format: "MMM d, yyyy  h:mm a") ?? ""
        let location  = moti.location?.title ?? ""
        let notes     = moti.body ?? ""
        
        let activityItems = [title, startDate, location, notes]
        
        let activity = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activity.excludedActivityTypes = [
            .print,
            .airDrop,
            .openInIBooks,
            .assignToContact,
            .saveToCameraRoll,
            .addToReadingList,
            .copyToPasteboard
        ]
        
        if let popoverController = activity.popoverPresentationController {
            popoverController.barButtonItem = navigationItem.rightBarButtonItem
            popoverController.permittedArrowDirections = .up
        }
        
        present(activity, animated: true)
    }
    
    func shareVia() {
        
    }
    
}

// MARK: - Action

extension NotesViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func edit(_ sender: UIBarButtonItem) {
        push()
    }
    
    @IBAction func share(_ sender: UIBarButtonItem) {
        showAlert()
    }
    
}
