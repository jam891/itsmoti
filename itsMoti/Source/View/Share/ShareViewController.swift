//
//  ShareViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import SwiftMessages

class ShareViewController: UIViewController {
    
    @IBOutlet weak var giftLabel: UILabel!
    @IBOutlet weak var shareLabel: UILabel!
    
    var subscription: SubscriptionModel!
    var reachability: Reachability!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
}

// MARK: - Private

private extension ShareViewController  {
    
    func setup() {
        setTitle(lhs: "Gift or Share", rhs: "itsMoti")
        giftLabel.setText(lhs: "Gift", rhs: "itsMoti")
        shareLabel.setText(lhs: "Share", rhs: "itsMoti")
        
        setupReachability()
    }
    
    func setupReachability() {
        reachability = Reachability()
        do {
            try reachability.startNotifier()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    
    func share() {
        if let link = NSURL(string: "http://motiurl.com") {
            let activityItems = [link] as [Any]
            let activity = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
            activity.excludedActivityTypes = [
                .print,
                .airDrop,
                .openInIBooks,
                .assignToContact,
                .saveToCameraRoll,
                .addToReadingList,
                .copyToPasteboard
            ]
            
            if let popover = activity.popoverPresentationController {
                popover.sourceView = view
                popover.sourceRect = CGRect(x: view.frame.size.width / 2,
                                            y: view.frame.size.height, width: 0, height: 0)
            }
            
            present(activity, animated: true)
        }
    }
    
    func purchase() {
        if reachability.isReachable {
            API.post.subscription { subscription, error in
                if let error = error {
                    Alert.showErrorAlert(error.localizedDescription)
                } else {
                    if let subscription = subscription {
                        PaymentManager.shared.purchase(subscription) { success in
                            print("success", success)
                        }
                    } else {
                        
                    }
                }
            }
        } else {
            showNotificationBar() 
        }
    }
    
    func showNotificationBar() {
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureContent(body: "No Internet Connection!")
        view.configureTheme(backgroundColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), foregroundColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1))
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.preferredStatusBarStyle = .lightContent
        config.presentationStyle = .top
        config.shouldAutorotate = true
        config.duration = .automatic
        
        SwiftMessages.show(config: config, view: view)
    }
    
}

// MARK: - Action

extension ShareViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
  
    @IBAction func gift(_ sender: UIBarButtonItem) {
    }
    
    @IBAction func share(_ sender: UIBarButtonItem) {
        share()
    }
    
    @IBAction func purchase(_ sender: UIButton) {
        purchase()
    }
    
}
