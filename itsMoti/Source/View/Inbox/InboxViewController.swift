//
//  InboxViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 2/4/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit
import SwiftMessages

class InboxViewController: UIViewController {
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    
    private let refreshControl = UIRefreshControl()
    private var indexPaths: [IndexPath: Bool] = [:]
    private var moties: [MotiModel] = []
    private var toolBar: UIToolbar {
        return navigationController!.toolbar
    }
    var reachability: Reachability!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
}

// MARK: - Private

private extension InboxViewController {
    
    func setup() {
        setupReachability()

        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        NotificationCenter.addObserver(self, selector: #selector(reachabilityChanged), name: .reachabilityChangedNotification, object: nil)
        
        fetchData()
    }
    
    func setupReachability() {
        reachability = Reachability()
        do {
            try reachability.startNotifier()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func fetchData() {
        if reachability.isReachable {
            API.post.shared { moties, error in
                self.spinner.stopAnimating()
                
                if let error = error {
                    Alert.showErrorAlert(error.localizedDescription)
                } else {
                    self.moties = moties.sorted(by: { $0.startDate > $1.startDate })
                    self.update()
                }
            }
        } else {
            showNotificationBar()
            spinner.stopAnimating()
            refreshControl.endRefreshing()
        }
    }
    
    func configure(_ cell: InboxCell, at indexPath: IndexPath) {
        if !indexPaths.keys.contains(indexPath) {
            indexPaths[indexPath] = false
        }
        cell.moti = moties[indexPath.row]
        cell.isSelected = indexPaths[indexPath]!
    }
    
    func updateToolbar() {
        let selected = indexPaths.filter { $0.value }
        toolBar.items?.forEach {
            $0.tintColor = selected.count > 0 ? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        toolBar.barTintColor = selected.count > 0 ? #colorLiteral(red: 0.2078431373, green: 0.8274509804, blue: 0.7137254902, alpha: 1) : nil
        toolBar.items![1].isEnabled = selected.count > 0
    }
    
    func update() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    func delete(_ moti: MotiModel) {
        if reachability.isReachable {
            API.post.removeShared(moti) { error in
                if let error = error {
                    Alert.showErrorAlert(error.localizedDescription)
                } else {
                    if let index = self.moties.index(of: moti) {
                        self.moties.remove(at: index)
                        let indexPath = IndexPath(row: index, section: 0)
                        self.tableView.deleteRows(at: [indexPath], with: .fade)
                    }
                }
            }
        } else {
            showNotificationBar()
        }
    }
    
    func showNotificationBar() {
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureContent(body: "No Internet Connection!")
        view.configureTheme(backgroundColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), foregroundColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1))
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.preferredStatusBarStyle = .lightContent
        config.presentationStyle = .top
        config.shouldAutorotate = true
        config.duration = .automatic
        
        SwiftMessages.show(config: config, view: view)
    }
    
    
}

// MARK: - TableViewDataSource

extension InboxViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moties.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: InboxCell.self, for: indexPath)
        configure(cell, at: indexPath)
        return cell
    }

}

// MARK: - TableViewDelegate

extension InboxViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { _, indexPath in
            self.delete(self.moties[indexPath.row])
        }
        return [delete]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        indexPaths[indexPath]?.toggle()
        tableView.reloadRows(at: [indexPath], with: .none)
        updateToolbar()
    }
    
}

// MARK: - Refresh

@objc
extension InboxViewController {
    
    func refresh(_ sender: UIRefreshControl) {
        fetchData()
    }
    
    func reachabilityChanged(_ notification: Notification) {
        if reachability.isReachable {
            fetchData()
        }
    }
    
}

// MARK: - Actions

extension InboxViewController {
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
    @IBAction func append(_ sender: UIBarButtonItem) {
        var selected: [MotiModel] = []
        for index in 0..<moties.count {
            let indexPath = IndexPath(row: index, section: 0)
            if indexPaths[indexPath]! {
                selected.append(moties[index])
            }
        }
      
        if reachability.isReachable {
            API.post.appendShared(selected) { error in
                if let error = error {
                    Alert.showErrorAlert(error.localizedDescription)
                } else {
                    DispatchQueue.main.async {
                        self.dismiss(animated: true)
                    }
                }
            }
        } else {
            showNotificationBar()
        }
    }
    
}

