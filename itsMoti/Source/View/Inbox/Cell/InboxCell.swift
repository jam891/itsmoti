//
//  InboxCell.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 2/4/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class InboxCell: UITableViewCell {
    
    @IBOutlet weak var ownerLabel: UILabel!
    @IBOutlet weak var motiTitleLabel: UILabel!
    @IBOutlet weak var motiStartDateLabel: UILabel!
    @IBOutlet weak var motiSizeLabel: UILabel!
    @IBOutlet weak var mediaTypeImageView: UIImageView!
    @IBOutlet weak var checkbox: UIImageView!
    
    var moti: MotiModel! {
        didSet { setup() }
    }

    override var isSelected: Bool {
        didSet { updateUI() }
    }
    
}

// MARK: - Private

private extension InboxCell {
    
    func setup() {
        if let media = moti.media {
            guard let type = MediaType(rawValue: media.type.rawValue) else { return }
            switch type {
            case .video: mediaTypeImageView.image = #imageLiteral(resourceName: "video")
            case .audio: mediaTypeImageView.image = #imageLiteral(resourceName: "audio")
            case .voice: mediaTypeImageView.image = #imageLiteral(resourceName: "voice")
            case .photo: mediaTypeImageView.image = #imageLiteral(resourceName: "photo")
            }
            motiSizeLabel.isHidden = false
            motiSizeLabel.text = media.size ?? "unknown"
        } else {
            mediaTypeImageView.image = #imageLiteral(resourceName: "note")
            motiSizeLabel.isHidden = true
        }
        
        motiTitleLabel.text = moti.title.capitalized
        ownerLabel.text = moti.owner?.username.capitalized
        motiStartDateLabel.text = moti.startDate.format(with: .full)
    }
    
    func updateUI() {
        checkbox?.image = isSelected ? #imageLiteral(resourceName: "select") : #imageLiteral(resourceName: "reveal")
    }
    
}
