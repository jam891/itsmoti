//
//  TodoViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import CoreData

class TodoViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var addTodoView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var containerView: UIView!
    
    
    private lazy var fetchedResultsController: NSFetchedResultsController<Todo>! = {
        let fetchRequest: NSFetchRequest<Todo> = Todo.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(Todo.createDate), ascending: false)]
        let moc = StorageManager.shared.context
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: moc, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    var isEditable = false {
        didSet { updateUI() }
    }
    private var isUpdated = false
    
    var moti: Moti!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isUpdated {
            update()
        }
    }
    
}

// MARK: - Navigation

extension TodoViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segueIdentifier(for: segue) else { return }
        
        guard let navigationController = segue.destination as? UINavigationController else { return }
        
        switch identifier {
        case .addTodo:
            let controller = navigationController.topViewController as! AddTodoViewController
            controller.moti = moti
        case .editTodo:
            let controller = navigationController.topViewController as! EditTodoViewController
            controller.todo = sender as? Todo
        default:
            break
        }
    }
    
}

// MARK: - Private

private extension TodoViewController {
    
    func setup() {
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func updateUI() {
        guard let moti = moti else { return }
        
        if isEditable {
            tableView.tableHeaderView = addTodoView
            stackView.removeArrangedSubview(containerView)
        } else {
            titleLabel.text    = moti.title
            subtitleLabel.text = moti.location?.title
//            stackView.addArrangedSubview(containerView)
        }
        
        stackView.setNeedsLayout()
        stackView.layoutIfNeeded()

        textView.isEditable = isEditable
        textView.text       = moti.body
        performFetch()
    }
    
    func performFetch() {
        let predicate = NSPredicate(format: "%K == %@", "moti", moti)
        fetchedResultsController.fetchRequest.predicate = predicate
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func configure(_ cell: TodoCell, at indexPath: IndexPath) {
        let todo = fetchedResultsController.object(at: indexPath)
        cell.todo = todo
    }
    
    func editTodo(at indexPath: IndexPath) {
        let todo = fetchedResultsController.object(at: indexPath)
        performSegue(withIdentifier: .editTodo, sender: todo)
    }
    
    func deleteTodo(at indexPath: IndexPath) {
        let todoItem = fetchedResultsController.object(at: indexPath)
        StorageManager.shared.delete(todoItem)
    }
    
    func update() {
        API.post.update(moti) { error in
            if let error = error {
                print(error.localizedDescription)
            }
            self.isUpdated = false
            StorageManager.shared.saveContext()
        }
    }
    
}

// MARK: - Action

extension TodoViewController {
    
    @IBAction func valueChanged(_ sender: UISegmentedControl) {
        textView.isHidden  = sender.selectedSegmentIndex == 1
        tableView.isHidden = sender.selectedSegmentIndex == 0
    }
    
    @IBAction func add(_ sender: UIButton) {
        performSegue(withIdentifier: .addTodo)
    }
    
}

// MARK: - UITableViewDataSource

extension TodoViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.fetchedObjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: TodoCell.self, for: indexPath)
        configure(cell, at: indexPath)
        return cell
    }
    
}

// MARK: - UITableViewDelegate

extension TodoViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 18
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return isEditable
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { _, indexPath in
            self.deleteTodo(at: indexPath)
        }
        let edit = UITableViewRowAction(style: .normal, title: "Edit") { _, indexPath in
            self.editTodo(at: indexPath)
        }
        edit.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        return [delete, edit]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard !isEditable else { return }
        
        let todo = fetchedResultsController.object(at: indexPath)
        todo.completed.toggle()
        
        StorageManager.shared.saveContext()
        
        moti.modified = Date()
        
        isUpdated = true
    }
    
}

// MARK: - NSFetchedResultsControllerDelegate

extension TodoViewController: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            guard
                let indexPath = indexPath,
                let cell = tableView.cellForRow(at: indexPath) as? TodoCell else { return }
            configure(cell, at: indexPath)
        default:
            return
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
}

// MARK: - UITextViewDelegate

extension TodoViewController: UITextViewDelegate {
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text != moti.body {
            moti.body = textView.text
        }
    }
    
}

