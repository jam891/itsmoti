//
//  AddTodoViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class AddTodoViewController: TableViewController {

    @IBOutlet weak var name: UITextView!
    
    private var errorLabel: UILabel!
    
    var moti: Moti!
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        name.becomeFirstResponder()
    }

}

// MARK: - Private

private extension AddTodoViewController {
    
    func addTodo() {
        let todo = StorageManager.shared.insert(Todo.self)
        todo.name       = name.text.trim()
        todo.moti       = moti
        todo.completed  = false
        todo.createDate = Date()
    }
    
    func validate() {
        if name.text.isEmpty {
            errorLabel.text = NSLocalizedString("Todo names must contain at least 1 character.", comment: "")
            animate()
        } else {
            addTodo()
            dismissKeyboard()
            dismiss(animated: true)
        }
    }
    
    func animate() {
        errorLabel.alpha = 0
        UIView.animate(withDuration: 1) {
            self.errorLabel.alpha = 1
        }
    }
    
}

// MARK: - Action

extension AddTodoViewController {
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismissKeyboard()
        dismiss(animated: true)
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        validate()
    }
    
}

// MARK: - UITableViewDelegate

extension AddTodoViewController {

    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        errorLabel = UILabel()
        errorLabel.textColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        errorLabel.textAlignment = .center
        errorLabel.font = .systemFont(ofSize: 12)
        errorLabel.adjustsFontSizeToFitWidth = true
        return errorLabel
    }

}


