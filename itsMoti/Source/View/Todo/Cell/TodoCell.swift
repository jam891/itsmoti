//
//  TodoCell.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class TodoCell: UITableViewCell {
    var todo: Todo! {
        didSet { setup() }
    }
}

// MARK: - Private

private extension TodoCell {
    func setup() {
        textLabel!.text  = todo.name
        imageView?.image = todo.completed ? #imageLiteral(resourceName: "select") : #imageLiteral(resourceName: "reveal")
    }
}

