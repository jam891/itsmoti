//
//  EditTodoViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class EditTodoViewController: TableViewController {

    @IBOutlet weak var name: UITextView!
    
    var todo: Todo!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        name.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    
    }

}

// MARK: - Private

private extension EditTodoViewController {
    
    func setup() {
        name.text = todo.name
    }
    
    func editTodo() {
        todo.name = name.text.trim()
    }
    
    func removeTodo() {
        StorageManager.shared.delete(todo)
        dismiss(animated: true)
    }
    
    func validate() {
        if !name.text.isEmpty && name.text != todo.name {
            editTodo()
        }
        dismissKeyboard()
        dismiss(animated: true)
    }
    
    func showAlert() {
        dismissKeyboard()
        
        let title   = NSLocalizedString("Are you sure you want to delete the todo '\(todo.name!)'?", comment: "")
        let message = NSLocalizedString("This todo will be moved to the Trash. This operation cannot be undone.", comment: "")
        let alert   = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Delete", comment: ""), style: .destructive) { _ in
            self.removeTodo()
        })
        alert.view.layoutIfNeeded()
        present(alert, animated: true, completion: nil)
    }
    
}

// MARK: - Action

extension EditTodoViewController {
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismissKeyboard()
        dismiss(animated: true)
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        validate()
    }
    
    @IBAction func remove(_ sender: UIButton) {
        showAlert()
    }
    
}

