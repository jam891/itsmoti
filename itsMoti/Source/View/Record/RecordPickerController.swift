//
//  RecordPickerController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import AVKit

protocol RecordPickerControllerDelegate: class {
    func recordPickerControllerDidCancel(_ picker: RecordPickerController)
    func recordPickerController(_ picker: RecordPickerController, didFinishPickingMedia asset: AVAsset)
}

class RecordPickerController: UIViewController {
    
    @IBOutlet weak var timeLabel: UILabel!
    
    weak var delegate: RecordPickerControllerDelegate?
    
    private var recorder: AVAudioRecorder!
    private var startTime: TimeInterval!
    private var outputURL: URL!
    private var timer: Timer!
    
    deinit {
        print("RecordPickerController deinit")
    }

}

// MARK: - Private

private extension RecordPickerController {
    
    func setupRecorder() {
        let settings: [String: Any] = [
            AVFormatIDKey: kAudioFormatMPEG4AAC,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
            AVEncoderBitRateKey: 128000,
            AVNumberOfChannelsKey: 2,
            AVSampleRateKey: 44100.0
        ]
        
        let directory = AppManager.shared.url(for: .voice)
        outputURL = directory
            .appendingPathComponent(NSUUID().uuidString)
            .appendingPathExtension(.m4a)

        do {
            recorder = try AVAudioRecorder(url: outputURL, settings: settings)
            recorder.delegate = self
            recorder.prepareToRecord()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func startRecording() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default, options: .defaultToSpeaker)
            try AVAudioSession.sharedInstance().setActive(true)
            recorder.record()
            startRecordTimer()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func stopRecording() {
        do {
            recorder.stop()
            stopRecordTimer()
            try AVAudioSession.sharedInstance().setActive(false)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func startRecordTimer() {
        stopRecordTimer()
        startTime = NSDate.timeIntervalSinceReferenceDate
        timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(updateLabel), userInfo: nil, repeats: true)
        RunLoop.current.add(timer, forMode: .common)
    }
    
    func stopRecordTimer() {
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
    }
    
}

// MARK: -

@objc
extension RecordPickerController {
    
    func updateLabel() {
        guard recorder != nil else { return }
        if recorder.isRecording {
            let currentTime = NSDate.timeIntervalSinceReferenceDate
            var elapsedTime = currentTime - startTime
            let minutes = UInt8(elapsedTime / 60)
            elapsedTime -= (TimeInterval(minutes) * 60)
            let seconds = UInt8(elapsedTime)
            elapsedTime -= TimeInterval(seconds)
            let fraction = UInt8(elapsedTime * 100)
            timeLabel.text = String(format: "%02d:%02d.%02d", minutes, seconds, fraction)
        }
    }
    
}

// MARK: - Actions

extension RecordPickerController {
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        delegate?.recordPickerControllerDidCancel(self)
    }
    
    @IBAction func record(_ sender: UIButton) {
        if recorder == nil {
            setupRecorder()
        }
        
        if recorder.isRecording {
            stopRecording()
        } else {
            startRecording()
        }
        
        sender.isSelected.toggle()
    }
    
}

// MARK: - AVAudioRecordDelegate

extension RecordPickerController: AVAudioRecorderDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        let alert = UIAlertController(title: "Recorder", message: "Finished Recording", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Keep", style: .default) { _ in
            let asset = AVAsset(url: recorder.url)
            self.delegate?.recordPickerController(self, didFinishPickingMedia: asset)
        })
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive) { _ in
            self.recorder.deleteRecording()
        })
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print(error!.localizedDescription)
    }
    
}

