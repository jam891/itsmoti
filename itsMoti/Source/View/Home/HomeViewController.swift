//
//  HomeViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        requestAuthorization()
    }

}

// MARK: - Private

private extension HomeViewController {
    
    func setup() {
        setTitle(lhs: "", rhs: "itsMoti")

//        API.post.clearAll { error in
//            StorageManager.shared.clear(Moti.self)
//            StorageManager.shared.saveContext()
//        }
        
        SyncManager.shared.synchronize()
    }
    
    func requestAuthorization() {
        PushManager.shared.requestAuthorization()
    }
    
    func showMenu() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Calendar", style: .default) { _ in
            self.performSegue(withIdentifier: .calendar)
        })
        alert.addAction(UIAlertAction(title: "Inbox", style: .default) { _ in
            self.performSegue(withIdentifier: .inbox)
        })
        alert.addAction(UIAlertAction(title: "Settings", style: .default) { _ in
            
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.barButtonItem = navigationItem.rightBarButtonItem
            popoverController.permittedArrowDirections = .up
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
}

// MARK: - Action

extension HomeViewController {
    
    @IBAction func unwindToHome(_ segue: UIStoryboardSegue) {}
    
    @IBAction func more(_ sender: UIBarButtonItem) {
        showMenu()
    }
    
}
