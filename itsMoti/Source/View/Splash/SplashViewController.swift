//
//  SplashViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !User.hasRunBefore {
            if User.accessToken != nil {
                User.accessToken = nil
            }
            User.hasRunBefore = true
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if User.accessToken != nil {
            performSegue(withIdentifier: .home)
        } else {
            performSegue(withIdentifier: .auth)
        }
    }

}

 


