//
//  AddPhotoViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class AddPhotoViewController: UIViewController {

    @IBOutlet weak var photoButton: UIBarButtonItem!
    @IBOutlet weak var nextButton: UIBarButtonItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    
    var moti: Moti!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if moti.created == nil && isMovingToParent {
            showPhotoMenu()
        }
    }
    
}

// MARK: - Navigation

extension AddPhotoViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segueIdentifier(for: segue) else { return }
        switch identifier {
        case .editPhoto:
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! EditPhotoViewController
            controller.image = sender as? UIImage
            controller.delegate = self
        default:
            break
        }
    }
    
}

// MARK: - Private

private extension AddPhotoViewController {
    
    func setup() {
        setTitle(lhs: "Photo", rhs: "Moti")
        
        if let moti = moti, let media = moti.media, let resource = media.resource {
            guard let url = AppManager.shared.url(for: resource, from: .photo) else { return }
            do {
                let imageData = try Data(contentsOf: url)
                imageView.image = UIImage(data: imageData)
            } catch {
                print(error.localizedDescription)
            }
        } else {
            moti = StorageManager.shared.insert(Moti.self)
        }
        updateUI()
    }
    
    func update(_ imageURL: URL) {
        let mediaModel = MediaModel(type: .photo,
                                    resource: imageURL.lastPathComponent,
                                    thumbnail: imageURL.lastPathComponent,
                                    size:"")
        
        moti.update(mediaModel: mediaModel)
    }
    
    func updateUI() {
        if let _ = imageView.image {
            nextButton.isEnabled = true
            editButton.isHidden  = false
        } else {
            nextButton.isEnabled = false
            editButton.isHidden  = true
        }
    }
    
    func openLibrary() {
        Permission.photos.request { status in
            if status == .authorized {
                DispatchQueue.main.async {
                    MediaPicker.open(.photoLibrary, delegate: self)
                }
            }
        }
    }
    
    func openCamera() {
        Permission.camera.request { status in
            if status == .authorized {
                DispatchQueue.main.async {
                    MediaPicker.open(.photoCamera, delegate: self)
                }
            }
        }
    }
    
    func showPhotoMenu() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default) { _ in
            self.openCamera()
        })
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default) { _ in
            self.openLibrary()
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if let popoverController = alert.popoverPresentationController {
            popoverController.permittedArrowDirections = .any
            popoverController.barButtonItem = photoButton
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func showDeleteMenu() {
        
    }
    
    func push() {
        let controller = UIStoryboard.motiViewController()
        controller.setTitle(lhs: "Photo", rhs: "Moti")
        controller.moti = moti
        navigationController?.pushViewController(controller, animated: true)
    }
    
}

// MARK: - Action

extension AddPhotoViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        StorageManager.shared.context.rollback()
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func photo(_ sender: UIBarButtonItem) {
        showPhotoMenu()
    }
    
    @IBAction func clear(_ sender: UIBarButtonItem) {
        showDeleteMenu()
    }
    
    @IBAction func next(_ sender: UIBarButtonItem) {
        push()
    }
    
    @IBAction func edit(_ sender: UIButton) {
        performSegue(withIdentifier: .editPhoto, sender: imageView.image)
    }
    
}

// MARK: - UIImagePickerControllerDelegate

extension AddPhotoViewController: UIImagePickerControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        
        guard let originalImage = info[.originalImage] as? UIImage else { return }
        let image = MediaManager.shared.addWatermark(to: originalImage)
        imageView.image = image
        updateUI()
        
        if let data = image.pngData() {
            if let imageURL = AppManager.shared.writeToTemporaryDirectoryImageData(data) {
                update(imageURL)
            }
        }
    }
    
}

// MARK: - UINavigationControllerDelegate

extension AddPhotoViewController: UINavigationControllerDelegate {}

// MARK: - EditPhotoViewControllerDelegate

extension AddPhotoViewController: EditPhotoViewControllerDelegate {
    
    func editPhotoControllerDidCancel(_ controller: EditPhotoViewController) {
        controller.dismiss(animated: false)
    }
    
    func editPhotoController(_ controller: EditPhotoViewController, photoDidEndEditing photo: UIImage) {
        controller.dismiss(animated: false)
        
        imageView.image = photo
        updateUI()
        
        if let data = photo.pngData() {
            if let imageURL = AppManager.shared.writeToTemporaryDirectoryImageData(data) {
                update(imageURL)
            }
        }
    }
    
}

