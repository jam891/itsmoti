//
//  PhotoViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class PhotoViewController: MediaViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var height: NSLayoutConstraint!
    
    private var todoViewController: TodoViewController {
        let controller = children.first(where: { $0 is TodoViewController })
        return controller as! TodoViewController
    }
    
    var moti: Moti!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}

// MARK: - Navigation

extension PhotoViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segueIdentifier(for: segue) else { return }
        switch identifier {
        case .addPhoto:
            let controller = segue.destination as! AddPhotoViewController
            controller.moti = moti
        default:
            break
        }
    }
    
}

// MARK: - Private

private extension PhotoViewController {
    
    func setup() {
        setTitle(lhs: "Photo", rhs: "Moti")
        
        guard let moti = moti, let media = moti.media else { return }
        if let resource = media.resource {
            guard let url = AppManager.shared.url(for: resource, from: .photo) else { return }
            do {
                let imageData = try Data(contentsOf: url)
                imageView.image = UIImage(data: imageData)
            } catch {
                print(error.localizedDescription)
            }
        }
        todoViewController.moti = moti
    }
    
    func showAlert() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alert.addAction(UIAlertAction(title: "Share via itsMoti", style: .default) { _ in
            self.present(SearchFactory.make(self.moti), animated: true)
        })
        alert.addAction(UIAlertAction(title: "Share", style: .default) { _ in
            self.share()
        })
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.barButtonItem = navigationItem.rightBarButtonItem
            popoverController.permittedArrowDirections = .up
        }
        alert.view.layoutIfNeeded()
        present(alert, animated: true)
    }
    
    func share() {
        let title     = moti.title ?? ""
        let startDate = moti.startDate?.string(format: "MMM d, yyyy  h:mm a") ?? ""
        let location  = moti.location?.title ?? ""
        let notes     = moti.body ?? ""
        
        var activityItems = [title, startDate, location, notes] as [Any]
        
        if let media = moti.media, let resource = media.resource {
            let directory = Directory(rawValue: media.type!)
            let url = AppManager.shared.url(filename: resource, for: directory!)!
            activityItems.append(url)
        }
        
        let activity = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activity.excludedActivityTypes = [
            .print,
            .airDrop,
            .openInIBooks,
            .assignToContact,
            .saveToCameraRoll,
            .addToReadingList,
            .copyToPasteboard
        ]
        
        if let popoverController = activity.popoverPresentationController {
            popoverController.barButtonItem = navigationItem.rightBarButtonItem
            popoverController.permittedArrowDirections = .up
        }
        
        present(activity, animated: true)
    }
    
    func shareVia() {
        
    }
    
}

// MARK: - Notification

extension PhotoViewController {
    
    override func orientationDidChange(_ notification: Notification) {
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight:
            height.isActive = false
            stackView.removeArrangedSubview(containerView)
        case .portrait, .portraitUpsideDown:
            height.isActive = true
            stackView.addArrangedSubview(containerView)
        default:
            break
        }
        stackView.setNeedsLayout()
        stackView.layoutIfNeeded()
    }
    
}

// MARK: - Action

extension PhotoViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func edit(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: .addPhoto)
    }
    
    @IBAction func share(_ sender: UIBarButtonItem) {
        showAlert()
    }
    
}


