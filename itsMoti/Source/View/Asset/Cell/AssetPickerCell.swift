//
//  AssetPickerCell.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/10/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class AssetPickerCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var durationLabel: UILabel!
}
