//
//  AssetPickerController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/10/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import Photos

protocol AssetPickerControllerDelegate: class {
    func assetPickerControllerDidCancel(_ picker: AssetPickerController)
    func assetPickerController(_ picker: AssetPickerController, didFinishPickingMedia asset: PHAsset)
}

class AssetPickerController: UICollectionViewController {
    
    private var fetchResult: PHFetchResult<PHAsset>!
    
    weak var delegate: AssetPickerControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayout.invalidateLayout()
    }
    
    deinit {
        print("AssetViewController deinit")
    }

}

// MARK: - Actions

extension AssetPickerController {
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        delegate?.assetPickerControllerDidCancel(self)
    }
    
}

// MARK: - Private

private extension AssetPickerController {
    
    func setup() {
        fetchAssets()
    }
    
    func fetchAssets() {
        if fetchResult == nil {
            let options = PHFetchOptions()
            options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
            fetchResult = PHAsset.fetchAssets(with: .video, options: options)
        }
    }
    
    func configure(_ cell: AssetPickerCell, at indexPath: IndexPath) {
        let asset = fetchResult.object(at: indexPath.item)
        let scale = UIScreen.main.scale
        let cellSize = cell.frame.size
        let thumbnailSize = CGSize(width: cellSize.width * scale, height: cellSize.height * scale)
        PHCachingImageManager().requestImage(for: asset, targetSize: thumbnailSize, contentMode: .aspectFill, options: nil) { image, _ in
            cell.imageView.image = image
        }
        cell.durationLabel.text = asset.duration.stringValue
    }
    
}

// MARK: - UICollectionViewDataSource

extension AssetPickerController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchResult.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: AssetPickerCell.self), for: indexPath) as! AssetPickerCell
        configure(cell, at: indexPath)
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let asset = fetchResult.object(at: indexPath.item)
        delegate?.assetPickerController(self, didFinishPickingMedia: asset)
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout

extension AssetPickerController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = (view.bounds.size.width - 3) / 3
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
}
