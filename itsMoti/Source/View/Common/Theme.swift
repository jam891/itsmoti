//
//  Theme.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

enum Theme {
    case `default`
    
    var color: UIColor {
        return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    var font: UIFont {
        return UIFont(name: "PoetsenOne-Regular", size: 21)!
    }
    
    func apply() {
        UINavigationBar.appearance().tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: color, .font: font]
    }
}
