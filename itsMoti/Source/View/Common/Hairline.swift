//
//  Hairline.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/14/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

enum HairLineType: String {
    case top
    case bottom
}

@IBDesignable
class Hairline: UIView {
    
    @available(*, unavailable, message: "This property is reserved for Interface Builder. Use 'top' instead.")
    @IBInspectable var hairLineType: String? {
        willSet {
            if let hairLineType = HairLineType(rawValue: newValue?.lowercased() ?? "") {
                type = hairLineType
            }
        }
    }
    
    private let opacity: Float = 0.26
    private let radius: CGFloat = 0
    private var scale: CGFloat {
        return 1 / UIScreen.main.scale
    }
    private var rect: CGRect {
        switch type {
        case .top:
            return CGRect(x: 0, y: 0, width: bounds.width, height: scale)
        case .bottom:
            return CGRect(x: 0, y: 1 - scale, width: bounds.width, height: scale)
        }
    }
    private var type: HairLineType = .top
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.shadowPath = CGPath(rect: rect, transform: nil)
    }
    
}

private extension Hairline {
    
    func setup() {
        backgroundColor     = .clear
        layer.shadowOffset  = .zero
        layer.shadowRadius  = radius
        layer.shadowOpacity = opacity
        layer.shadowColor   = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        layer.shadowPath    = CGPath(rect: rect, transform: nil)
    }
    
}


