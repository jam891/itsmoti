//
//  DesignableImageView.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/18/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableImageView: UIImageView {
    @IBInspectable
    var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable
    var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable
    var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
}
