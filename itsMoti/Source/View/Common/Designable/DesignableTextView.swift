//
//  DesignableTextView.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/14/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableTextView: UITextView {
    
    @IBInspectable
    var placeholder: String! {
        get {
            return placeholderTextView.text
        }
        set {
            placeholderTextView.text = newValue
        }
    }
    
    override var font: UIFont? {
        didSet {
            placeholderTextView.font = font
        }
    }
    
    override var text: String! {
        didSet {
            placeholderTextView.isHidden = !text.isEmpty
        }
    }

    private var placeholderTextView = UITextView()


    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
}

// MARK: - Notification

@objc
extension DesignableTextView {
    
    func textDidChange(_ notification: Notification) {
        placeholderTextView.isHidden = !text.isEmpty
    }
    
}

// MARK: - Private

private extension DesignableTextView {
    
    func setup() {
        setupPlaceholder()
        activateConstraints()
        registeredNotification()
    }
    
    func setupPlaceholder() {
        placeholderTextView.textColor = #colorLiteral(red: 0.7803147435, green: 0.7804473042, blue: 0.8017958999, alpha: 1)
        placeholderTextView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        placeholderTextView.isScrollEnabled = false
        placeholderTextView.isUserInteractionEnabled = false
        placeholderTextView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(placeholderTextView)
    }
    
    func registeredNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange), name: UITextView.textDidChangeNotification, object: self)
    }
    
    func activateConstraints() {
        NSLayoutConstraint.activate([
            placeholderTextView.leadingAnchor.constraint(equalTo: leadingAnchor),
            placeholderTextView.trailingAnchor.constraint(equalTo: trailingAnchor),
            placeholderTextView.topAnchor.constraint(equalTo: topAnchor),
            placeholderTextView.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
    }
    
}

