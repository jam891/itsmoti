//
//  MediaPicker.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/19/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import AVKit
import MediaPlayer
import MobileCoreServices

enum Attachment {
    case photoLibrary
    case videoLibrary
    case mediaLibrary
    case voiceLibrary
    case videoCamera
    case photoCamera
    case voiceRecord
    case socialVideo
}

struct MediaPicker {
    static func open(_ attachment: Attachment, delegate: UIViewController) {
        switch attachment {
        case .photoLibrary: photoLibrary(delegate: delegate as! UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate)
        case .videoLibrary: videoLibrary(delegate: delegate as! UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate)
        case .mediaLibrary: mediaLibrary(delegate: delegate as! UIViewController & MPMediaPickerControllerDelegate & UINavigationControllerDelegate)
        case .voiceLibrary: voiceLibrary(delegate: delegate as! UIViewController & VoicePickerControllerDelegate   & UINavigationControllerDelegate)
        case .videoCamera:  videoCamera(delegate:  delegate as! UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate)
        case .photoCamera:  photoCamera(delegate:  delegate as! UIViewController & UIImagePickerControllerDelegate & UINavigationControllerDelegate)
        case .voiceRecord:  voiceRecord(delegate:  delegate as! UIViewController & RecordPickerControllerDelegate  & UINavigationControllerDelegate)
        case .socialVideo:  socialVideo(delegate:  delegate as! UIViewController & VideoPickerControllerDelegate   & UINavigationControllerDelegate)
        }
    }
}

private extension MediaPicker {
    static func photoCamera(delegate: UIViewController & UINavigationControllerDelegate & UIImagePickerControllerDelegate) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let picker = UIImagePickerController()
            picker.delegate = delegate
            picker.sourceType = .camera
            picker.allowsEditing = false
            picker.cameraCaptureMode = .photo
            delegate.present(picker, animated: true)
        }
    }
    
    static func videoCamera(delegate: UIViewController & UINavigationControllerDelegate & UIImagePickerControllerDelegate) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let picker = UIImagePickerController()
            picker.delegate = delegate
            picker.sourceType = .camera
            picker.videoQuality = .typeHigh
            picker.mediaTypes = [kUTTypeMovie as String]
            delegate.present(picker, animated: true)
        }
    }
    
    static func photoLibrary(delegate: UIViewController & UINavigationControllerDelegate & UIImagePickerControllerDelegate) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let picker = UIImagePickerController()
            picker.delegate = delegate
            picker.sourceType = .photoLibrary
            delegate.present(picker, animated: true)
        }
    }
    
    static func videoLibrary(delegate: UIViewController & UINavigationControllerDelegate & UIImagePickerControllerDelegate) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let picker = UIImagePickerController()
            picker.delegate = delegate
            picker.sourceType = .photoLibrary
            picker.mediaTypes = [kUTTypeMovie as String, kUTTypeVideo as String]
            delegate.present(picker, animated: true)
        }
    }
    
    static func mediaLibrary(delegate: UIViewController & UINavigationControllerDelegate & MPMediaPickerControllerDelegate) {
        let mediaPicker = MPMediaPickerController(mediaTypes: .anyAudio)
        mediaPicker.delegate = delegate
        delegate.present(mediaPicker, animated: true)
    }
    
    static func voiceRecord(delegate: UIViewController & UINavigationControllerDelegate & RecordPickerControllerDelegate) {
        let recordPicker = UIStoryboard.recordPickerController()
        recordPicker.delegate = delegate
        
        let navigationController = UINavigationController(rootViewController: recordPicker)
        navigationController.navigationBar.isTranslucent = false
        navigationController.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        delegate.present(navigationController, animated: true)
    }
    
    static func voiceLibrary(delegate: UIViewController & UINavigationControllerDelegate & VoicePickerControllerDelegate) {
        let voicePicker = UIStoryboard.voicePickerController()
        voicePicker.delegate = delegate
        
        let navigationController = UINavigationController(rootViewController: voicePicker)
       
        delegate.present(navigationController, animated: true)
    }
    
    static func socialVideo(delegate: UIViewController & UINavigationControllerDelegate & VideoPickerControllerDelegate) {
        let videoPicker = UIStoryboard.videoPickerController()
        videoPicker.delegate = delegate
        
        let navigationController = UINavigationController(rootViewController: videoPicker)
        
        delegate.present(navigationController, animated: true)
    }
    
}
