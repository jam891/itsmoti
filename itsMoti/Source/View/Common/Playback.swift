//
//  Playback.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/17/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import AVKit

enum PlaybackState {
    case play
    case pause
    case finish
    case `default`
    
    func changePlaybackState(_ playback: Playback) {
        switch self {
        case .play:
            if let player = playback.audioPlayer {
                if let _ = playback.timer {
                    playback.timer.invalidate()
                }
                playback.timer = CADisplayLink(target: playback, selector: #selector(playback.updateProgress))
                playback.timer.add(to: .current, forMode: .common)
                
                let currentTime = player.currentTime
                player.currentTime = currentTime
                AudioSessionHelper.setupSessionActive(true)
                player.play()
            }
            playback.progressView.iconStyle = .pause
        case .pause:
            if let _ = playback.timer {
                playback.timer.invalidate()
                playback.timer = nil
            }
            playback.audioPlayer?.pause()
            AudioSessionHelper.setupSessionActive(false)
            playback.progressView.iconStyle = .play
        case .finish:
            if let _ = playback.timer {
                playback.timer.invalidate()
                playback.timer = nil
            }
            
            AudioSessionHelper.setupSessionActive(false)
            playback.progressView.progress = 1.0
            playback.progressView.iconStyle = .play
        case .default:
            if let _ = playback.timer {
                playback.timer.invalidate()
                playback.timer = nil
            }
            
            playback.audioPlayer = nil
            playback.voice = nil

            AudioSessionHelper.setupSessionActive(false)

            playback.progressView.removeFromSuperview()
            playback.progressView.setProgress(0.0, animated: false)
        }
    }
 
}

class Playback: NSObject {
    var voice: URL!
    var audioPlayer: AVAudioPlayer!
    var timer: CADisplayLink!
    let progressView = CircularProgressView()
    
    var state: PlaybackState = .default {
        didSet {
            state.changePlaybackState(self)
        }
    }
}

@objc
extension Playback {
    func updateProgress() {
        if let audioPlayer = audioPlayer {
            let progress = CGFloat(audioPlayer.currentTime / audioPlayer.duration)
            if progress < progressView.progress {
                progressView.setProgress(progress, animated: false)
            } else {
                progressView.progress = progress
            }
        }
    }
}


class AudioSessionHelper {
    static func setupSessionActive(_ active: Bool) {
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setActive(active)
        } catch {
            print(error.localizedDescription)
        }
    }
}
