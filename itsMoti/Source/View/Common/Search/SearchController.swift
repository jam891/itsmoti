//
//  SearchController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/16/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class SearchBar: UISearchBar {
    override func layoutSubviews() {
        super.layoutSubviews()
        setShowsCancelButton(false, animated: false)
    }
}

class SearchController: UISearchController {
    lazy var _searchBar: SearchBar = { [unowned self] in
        let result = SearchBar(frame: .zero)
        result.delegate = self
        return result
        }()
    
    override var searchBar: UISearchBar {
        return _searchBar
    }
}

extension SearchController: UISearchBarDelegate {}
