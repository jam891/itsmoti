//
//  LocationCell.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/30/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var locationLabel: UILabel!
    
    var location: Location! {
        didSet { setup() }
    }

}

// MARK: - Private

private extension LocationCell {
    
    func setup() {
        guard let location = location else { return }
        textField.text = " "
        let title      = location.title ?? ""
        let subtitle   = location.subtitle ?? ""
        setLocation(title: title, subtitle: subtitle)
    }
    
    func setLocation(title: String, subtitle: String) {
        let attributedTitle    = NSAttributedString(string: title, attributes: [.font: UIFont.systemFont(ofSize: 17)])
        let attributedSubtitle = NSAttributedString(string: subtitle, attributes: [.font: UIFont.systemFont(ofSize: 12)])
        
        let attributedText = NSMutableAttributedString()
        attributedText.append(attributedTitle)
        if subtitle.count > 0 {
            attributedText.append(NSAttributedString(string: "\n"))
            attributedText.append(attributedSubtitle)
        }
        locationLabel.attributedText = attributedText
    }
    
}

// MARK: - Action

extension LocationCell {
    
    @IBAction func clear(_ sender: UITapGestureRecognizer) {
        textField.text     = ""
        locationLabel.text = ""
    }
    
}
