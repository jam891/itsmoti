//
//  NibView.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/15/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class NibView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

}
