//
//  PlayerView.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/6/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class PlayerView: UIView {

    var playerLayer: CALayer?
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        playerLayer?.frame = bounds
    }

}
