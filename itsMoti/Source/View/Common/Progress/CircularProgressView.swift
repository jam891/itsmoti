//
//  CircularProgressView.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/17/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

enum IconStyle {
    case play
    case pause
    
    func path(_ layerBounds: CGRect) -> CGPath {
        switch self {
        case .play:
            let path = UIBezierPath()
            path.move(to: CGPoint(x: layerBounds.width / 5, y: 0))
            path.addLine(to: CGPoint(x: layerBounds.width, y: layerBounds.height / 2))
            path.addLine(to: CGPoint(x: layerBounds.width / 5, y: layerBounds.height))
            path.close()
            return path.cgPath
        case .pause:
            let rect = CGRect(origin: CGPoint(x: layerBounds.width * 0.1, y: 0),
                              size: CGSize(width: layerBounds.width * 0.2, height: layerBounds.height))
            
            let path = UIBezierPath(rect: rect)
            path.append(UIBezierPath(rect: rect.offsetBy(dx: layerBounds.width * 0.6, dy: 0)))
            return path.cgPath
        }
    }
}

@IBDesignable
class CircularProgressView: UIView {
    
    @IBInspectable
    var progress: CGFloat = 0 {
        didSet {
            progressLayer.strokeEnd = progress
        }
    }
    
    @IBInspectable
    var lineWidth: CGFloat = 2 {
        didSet {
            backgroundLayer.lineWidth = lineWidth
            progressLayer.lineWidth   = lineWidth
        }
    }
    
    @IBInspectable
    var backgroundLayerStrokeColor: UIColor = UIColor(white: 0.9, alpha: 1) {
        didSet {
            backgroundLayer.strokeColor = backgroundLayerStrokeColor.cgColor
        }
    }
    
    @IBInspectable
    var iconLayerFrameRatio: CGFloat = 0.4 {
        didSet {
            iconLayer.frame = iconLayerFrame(iconLayerBounds, ratio: iconLayerFrameRatio)
            iconLayer.path  = iconStyle.path(iconLayerBounds)
        }
    }
    
    
    private lazy var progressLayer: CAShapeLayer = {
        let progressLayer = CAShapeLayer()
        progressLayer.fillColor   = nil
        progressLayer.lineWidth   = lineWidth
        progressLayer.strokeColor = tintColor.cgColor
        layer.insertSublayer(progressLayer, above: backgroundLayer)
        return progressLayer
    }()
    
    private lazy var backgroundLayer: CAShapeLayer = {
        let backgroundLayer = CAShapeLayer()
        backgroundLayer.fillColor   = nil
        backgroundLayer.lineWidth   = lineWidth
        backgroundLayer.strokeColor = backgroundLayerStrokeColor.cgColor
        layer.addSublayer(backgroundLayer)
        return backgroundLayer
    }()
    
    private lazy var iconLayer: CAShapeLayer = {
        let iconLayer = CAShapeLayer()
        iconLayer.fillColor = tintColor.cgColor
        layer.addSublayer(iconLayer)
        return iconLayer
    }()
    
    var iconStyle: IconStyle = .play {
        didSet {
            iconLayer.path = iconStyle.path(iconLayerBounds)
        }
    }
    
    private let π  = Double.pi
    private let π2 = Double.pi / 2
    private var iconLayerBounds: CGRect {
        return iconLayer.bounds
    }
    
//    override var isSelected: Bool {
//        didSet {
//            if isSelected {
//                iconStyle = .pause
//            } else {
//                iconStyle = .play
//            }
//        }
//    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let squareRect = squareLayerFrame(layer.bounds)
        backgroundLayer.frame = squareRect
        progressLayer.frame   = squareRect
        
        let innerRect = squareRect.insetBy(dx: lineWidth / 2, dy: lineWidth / 2)
        iconLayer.frame = iconLayerFrame(innerRect, ratio: iconLayerFrameRatio)
        
        let center = CGPoint(x: squareRect.width / 2, y: squareRect.height / 2)
        let path = UIBezierPath(arcCenter: center,
                                radius: innerRect.width / 2,
                                startAngle: CGFloat(-π2),
                                endAngle: CGFloat(-π2 + 2.0 * π),
                                clockwise: true)
        
        backgroundLayer.path = path.cgPath
        progressLayer.path   = path.cgPath
        iconLayer.path = iconStyle.path(iconLayerBounds)
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        iconStyle = .play
    }
    
}

extension CircularProgressView {
    
    func setProgress(_ progress: CGFloat, animated: Bool = true) {
        if animated {
            self.progress = progress
        } else {
            self.progress = progress
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.duration  = 0
            animation.fromValue = progress
            progressLayer.add(animation, forKey: nil)
        }
    }
    
}

// MARK: - Private

private extension CircularProgressView {
    
    func squareLayerFrame(_ rect: CGRect) -> CGRect {
        if rect.width != rect.height {
            let width = min(rect.width, rect.height)
            
            let originX = (rect.width - width) / 2
            let originY = (rect.height - width) / 2
            
            return CGRect(x: originX, y: originY, width: width, height: width)
        }
        return rect
    }
    
    func iconLayerFrame(_ rect: CGRect, ratio: CGFloat) -> CGRect {
        let insetRatio = (1 - ratio) / 2
        return rect.insetBy(dx: rect.width * insetRatio, dy: rect.height * insetRatio)
    }
    
}
