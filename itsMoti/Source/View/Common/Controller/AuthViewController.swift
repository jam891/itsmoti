//
//  AuthViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import SwiftMessages

enum Field {
    case username
    case password
    case email
    case phone
}

class AuthViewController: UITableViewController {
    
    private lazy var loadingView: LoadingView! = {
        let frame = navigationController!.view.bounds
        let loadingView = LoadingView(frame: frame)
        loadingView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        navigationController?.view.addSubview(loadingView)
        return loadingView
    }()
    
    var reachability: Reachability!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
}

// MARK: - Notification

@objc
extension AuthViewController {
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

// MARK: - Public

extension AuthViewController {
    
    func showLoadingView() {
        if let loadingView = loadingView {
            loadingView.alpha = 0
            loadingView.animate = true
            UIView.animate(withDuration: 0.3) {
                loadingView.alpha = 1
            }
        }
    }
    
    func hideLoadingView() {
        if let loadingView = loadingView {
            loadingView.alpha = 1
            loadingView.animate = false
            UIView.animate(withDuration: 0.3) {
                loadingView.alpha = 0
            }
        }
    }
    
    func showNotificationBar() {
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureContent(body: "No Internet Connection!")
        view.configureTheme(backgroundColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), foregroundColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1))
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.preferredStatusBarStyle = .lightContent
        config.presentationStyle = .top
        config.shouldAutorotate = true
        config.duration = .automatic
        
        SwiftMessages.show(config: config, view: view)
    }
    
}


// MARK: - Private

private extension AuthViewController {
    
    func setup() {
        setupReachability()
        setupKeyboardDismissRecognizer()
        setTitle(lhs: "", rhs: "itsMoti")
    }
    
    func setupReachability() {
        reachability = Reachability()
        do {
            try reachability.startNotifier()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func setupKeyboardDismissRecognizer() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapRecognizer)
    }
    
}

