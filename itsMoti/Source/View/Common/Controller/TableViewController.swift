//
//  TableViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/13/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
