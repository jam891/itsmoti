//
//  MediaViewController.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class MediaViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registeredNotification()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setPortraitOrientation()
    }

}

// MARK: - Private

private extension MediaViewController {
    
    func registeredNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: .orientationDidChange,
                                               name: UIDevice.orientationDidChangeNotification,
                                               object: nil)
    }
    
    func setPortraitOrientation() {
        let value = UIInterfaceOrientation.portrait.rawValue
        UIDevice.current.setValue(value, forKey: .orientation)
    }
    
}

// MARK: - Notification

@objc
extension MediaViewController {
    func canRotate() {}
    func orientationDidChange(_ notification: Notification) {}
}
