//
//  Segue.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/14/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

enum SegueIdentifier: String {
    case auth        = "AuthSegue"
    case moti        = "MotiSegue"
    case home        = "HomeSegue"
    
    case signIn      = "SignInSegue"
    case signUp      = "SignUpSegue"
    
    case daily       = "DailySegue"
    case inbox       = "InboxSegue"
    case calendar    = "CalendarSegue"
    case location    = "LocationSegue"
    case endRepeat   = "EndRepeatSegue"
    case addPhoto    = "AddPhotoSegue"
    case addVideo    = "AddVideoSegue"
    case addAudio    = "AddAudioSegue"
    case addVoice    = "AddVoiceSegue"
    case addTodo     = "AddTodoSegue"
    case editTodo    = "EditTodoSegue"
    case editPhoto   = "EditPhotoSegue"
    case cropPhoto   = "CropPhotoSegue"
    case filterPhoto = "FilterPhotoSegue"
    case textPhoto   = "TextPhotoSegue"
    case markup      = "MarkupSegue"
    case drawPopover = "DrawPopoverSegue"
    case textPopover = "TextPopoverSegue"
}

protocol StoryboardSegueProtocol {}

extension StoryboardSegueProtocol where Self: UIViewController {
    
    func performSegue(withIdentifier: SegueIdentifier, sender: Any? = nil) {
        performSegue(withIdentifier: withIdentifier.rawValue, sender: sender)
    }
    
    func segueIdentifier(for segue: UIStoryboardSegue) -> SegueIdentifier? {
        guard
            let identifier = segue.identifier,
            let segueIdentifier = SegueIdentifier(rawValue: identifier)
        else { return nil }
        return segueIdentifier
    }
    
}

extension UIViewController: StoryboardSegueProtocol {}
