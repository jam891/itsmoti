//
//  DrawableView.swift
//  MarkupDemo
//
//  Created by Vitaliy Delidov on 1/11/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

protocol CanvasDelegate: class {
    func canvas(_ canvas: DrawableView, didUpdateDrawing drawing: Drawing, mergedImage image: UIImage?)
    func canvas(_ canvas: DrawableView, didSaveDrawing drawing: Drawing, mergedImage image: UIImage?)
    func brush() -> Brush?
}

class DrawableView: UIView {
    
    private var mainImageView: UIImageView!
    private var tempImageView: UIImageView!
    
    private let session = Session()
    private var drawing = Drawing()
    private let path = UIBezierPath()
    private let scale = UIScreen.main.scale
    
    private var saved = false
    private var pointMoved = false
    private var pointIndex = 0
    private var points = [CGPoint](repeating: .zero, count: 5)
    
    var backgroundImageView: UIImageView!
    var isEditing = false
    var brush = Brush()
    
    weak var delegate: CanvasDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
}

// MARK: - Private

private extension DrawableView {
    
    func setup() {
        path.lineCapStyle = .round
        
        backgroundImageView = UIImageView(frame: bounds)
        backgroundImageView.contentMode = .scaleAspectFit
        backgroundImageView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(backgroundImageView)
        
        mainImageView = UIImageView(frame: bounds)
        mainImageView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(mainImageView)
        
        tempImageView = UIImageView(frame: bounds)
        tempImageView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(tempImageView)
    }
    
    func strokePath() {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        path.lineWidth = brush.width / scale
        brush.color.withAlphaComponent(brush.alpha).setStroke()
        path.stroke(with: .normal, alpha: 1)
        tempImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    func mergePaths() {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        mainImageView.image?.draw(in: bounds)
        tempImageView.image?.draw(in: bounds)
        mainImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        session.append(Drawing(stroke: mainImageView.image, background: backgroundImageView.image))
        tempImageView.image = nil
        UIGraphicsEndImageContext()
    }
    
    func mergePathsAndImages() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        if backgroundImageView.image != nil {
            let rect = centeredBackgroundImageRect()
            backgroundImageView.image?.draw(in: rect)
        }
        mainImageView.image?.draw(in: bounds)
        let mergedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return mergedImage!
    }
    
    func centeredBackgroundImageRect() -> CGRect {
        if frame.size.equalTo((backgroundImageView.image?.size)!) {
            return frame
        }
        
        let width       = frame.width
        let height      = frame.height
        let imageWidth  = backgroundImageView.image?.size.width
        let imageHeight = backgroundImageView.image?.size.height
        
        let widthRatio  = width / imageWidth!
        let heightRatio = height / imageHeight!
        let scale = min(widthRatio, heightRatio)
        let resizedWidth  = scale * imageWidth!
        let resizedHeight = scale * imageHeight!
        
        var rect: CGRect = .zero
        rect.size = CGSize(width: resizedWidth, height: resizedHeight)
        
        if width > resizedWidth {
            rect.origin.x = (width - resizedWidth) / 2
        }
        if height > resizedHeight {
            rect.origin.y = (height - resizedHeight) / 2
        }
        return rect
    }
    
    func didUpdateCanvas() {
        let mergedImage = mergePathsAndImages()
        let currentDrawing = Drawing(stroke: mainImageView.image, background: backgroundImageView.image)
        delegate?.canvas(self, didUpdateDrawing: currentDrawing, mergedImage: mergedImage)
    }
    
    func updateByLastSession() {
        let lastSession = session.lastSession()
        mainImageView.image = lastSession?.stroke
    }
    
    func isStrokeEqual() -> Bool {
        return compare(drawing.stroke, isEqualTo: mainImageView.image)
    }
    
    func isBackgroundEqual() -> Bool {
        return compare(drawing.background, isEqualTo: backgroundImageView.image)
    }
    
    func compare(_ image1: UIImage?, isEqualTo image2: UIImage?) -> Bool {
        if image1 == nil && image2 == nil {
            return true
        } else if image1 == nil || image2 == nil {
            return false
        }
        
        let data1 = image1!.pngData()
        let data2 = image2!.pngData()
        
        if data1 == nil || data2 == nil {
            return false
        }
        return data1! == data2
    }
    
}

// MARK: - Touches

extension DrawableView {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        if isEditing {
            saved = false
            pointMoved = false
            pointIndex = 0
            
            let touch = touches.first!
            points[0] = touch.location(in: self)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        
        if isEditing {
            let touch = touches.first!
            let currentPoint = touch.location(in: self)
            pointMoved = true
            pointIndex += 1
            points[pointIndex] = currentPoint
            
            if pointIndex == 4 {
                points[3] = CGPoint(x: (points[2].x + points[4].x) / 2,
                                    y: (points[2].y + points[4].y) / 2)
                
                path.move(to: points[0])
                path.addCurve(to: points[3],
                              controlPoint1: points[1],
                              controlPoint2: points[2])
                
                points[0] = points[3]
                points[1] = points[4]
                pointIndex = 1
            }
            strokePath()
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        touchesEnded(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        if isEditing {
            if !pointMoved {
                path.move(to: points[0])
                path.addLine(to: points[0])
                strokePath()
            }
            
            mergePaths()
            didUpdateCanvas()
            
            path.removeAllPoints()
            pointIndex = 0
        }
    }
    
}

// MARK: - Action

extension DrawableView {
    
    func undo() {
        session.undo()
        updateByLastSession()
        saved = canSave()
        didUpdateCanvas()
    }
    
    func canSave() -> Bool {
        return !(isStrokeEqual() && isBackgroundEqual())
    }
    
}
