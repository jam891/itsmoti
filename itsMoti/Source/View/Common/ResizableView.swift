//
//  ResizableView.swift
//  MarkupDemo
//
//  Created by Vitaliy Delidov on 1/11/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

protocol ResizableViewDelegate: NSObjectProtocol {
    func resizableViewDidBeginEditing(_ resizableView: ResizableView)
    func resizableViewDidEndEditing(_ resizableView: ResizableView)
    func resizableViewDidMove(_ resizableView: ResizableView)
}

struct ResizableViewAnchorPoint {
    var adjustsX: CGFloat = 0
    var adjustsY: CGFloat = 0
    var adjustsH: CGFloat = 0
    var adjustsW: CGFloat = 0
}

struct ResizableViewAnchorPointPair {
    var point: CGPoint = .zero
    var anchorPoint = ResizableViewAnchorPoint()
}

class ResizableView: UIView {
    
    private let resizableViewGlobalInset: CGFloat           = 5
    private let resizableViewDefaultMinWidth: CGFloat       = 48
    private let resizableViewDefaultMinHeight: CGFloat      = 48
    private let resizableViewInteractiveBorderSize: CGFloat = 10
    
    private var borderView: BorderView!
    
    private var _contentView: UIView!
    var contentView: UIView! {
        get {
            return _contentView
        }
        set {
            newValue.removeFromSuperview()
            newValue.frame = bounds.insetBy(dx: resizableViewGlobalInset + resizableViewInteractiveBorderSize / 2,
                                            dy: resizableViewGlobalInset + resizableViewInteractiveBorderSize / 2)
            
            addSubview(newValue)
            
            borderView.removeFromSuperview()
            addSubview(borderView)
            
            _contentView = newValue
        }
    }
    
    var touchStart: CGPoint = .zero
    var minWidth: CGFloat = 48
    var minHeight: CGFloat = 48
    
    var anchorPoint = ResizableViewAnchorPoint()
    
    weak var delegate: ResizableViewDelegate?
    
    private var resizableViewNoResizeAnchorPoint    = ResizableViewAnchorPoint(adjustsX: 0, adjustsY: 0, adjustsH: 0, adjustsW: 0)
    private var resizableViewUpperLeftAnchorPoint   = ResizableViewAnchorPoint(adjustsX: 1, adjustsY: 1, adjustsH: -1, adjustsW: 1)
    private var resizableViewMiddleLeftAnchorPoint  = ResizableViewAnchorPoint(adjustsX: 1, adjustsY: 0, adjustsH: 0, adjustsW: 1)
    private var resizableViewLowerLeftAnchorPoint   = ResizableViewAnchorPoint(adjustsX: 1, adjustsY: 0, adjustsH: 1, adjustsW: 1)
    private var resizableViewUpperMiddleAnchorPoint = ResizableViewAnchorPoint(adjustsX: 0, adjustsY: 1, adjustsH: -1, adjustsW: 0)
    private var resizableViewUpperRightAnchorPoint  = ResizableViewAnchorPoint(adjustsX: 0, adjustsY: 1, adjustsH: -1, adjustsW: -1)
    private var resizableViewMiddleRightAnchorPoint = ResizableViewAnchorPoint(adjustsX: 0, adjustsY: 0, adjustsH: 0, adjustsW: -1)
    private var resizableViewLowerRightAnchorPoint  = ResizableViewAnchorPoint(adjustsX: 0, adjustsY: 0, adjustsH: 1, adjustsW: -1)
    private var resizableViewLowerMiddleAnchorPoint = ResizableViewAnchorPoint(adjustsX: 0, adjustsY: 0, adjustsH: 1, adjustsW: 0)
    
    var editing: Bool = true {
        didSet {
            borderView.isHidden = !editing
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    deinit {
        contentView.removeFromSuperview()
    }
    
}

// MARK: - Private

extension ResizableView {
    
    func setup() {
        borderView = BorderView(frame: bounds.insetBy(dx: resizableViewGlobalInset,
                                                      dy: resizableViewGlobalInset))
        borderView.isHidden = true
        addSubview(borderView)
        
        minWidth  = resizableViewDefaultMinWidth
        minHeight = resizableViewDefaultMinHeight
    }
    
    func distanceBetweenTwoPoints(point1: CGPoint, point2: CGPoint) -> CGFloat {
        let dx = point2.x - point1.x
        let dy = point2.y - point1.y
        return sqrt(dx * dx + dy * dy)
    }
    
    func anchorPoint(forTouchLocation touchPoint: CGPoint) -> ResizableViewAnchorPoint {
        let middleRight = ResizableViewAnchorPointPair(point: CGPoint(x: bounds.size.width, y: bounds.size.height / 2),
                                                       anchorPoint: resizableViewMiddleRightAnchorPoint)
        
        let middleLeft  = ResizableViewAnchorPointPair(point: CGPoint(x: 0, y: bounds.size.height / 2),
                                                       anchorPoint: resizableViewMiddleLeftAnchorPoint)
        
        let centerPoint = ResizableViewAnchorPointPair(point: CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2),
                                                       anchorPoint: resizableViewNoResizeAnchorPoint)
        
        let allPoints: [ResizableViewAnchorPointPair] = [ middleLeft, middleRight, centerPoint]
        var smallestDistance = CGFloat(MAXFLOAT)
        var closestPoint: ResizableViewAnchorPointPair = centerPoint
        
        for i in 0..<allPoints.count {
            let distance = distanceBetweenTwoPoints(point1: touchPoint, point2: allPoints[i].point)
            if distance < smallestDistance {
                closestPoint = allPoints[i]
                smallestDistance = distance
            }
        }
        return closestPoint.anchorPoint
    }
    
    func translate(usingTouchLocation touchPoint: CGPoint) {
        var newCenter = CGPoint(x: center.x + touchPoint.x - touchStart.x, y: center.y + touchPoint.y - touchStart.y)
        if let superView = superview {
            let midPointX = bounds.midX
            if newCenter.x > superView.bounds.size.width - midPointX {
                newCenter.x = superView.bounds.size.width - midPointX
            }
            if newCenter.x < midPointX {
                newCenter.x = midPointX
            }
            let midPointY = bounds.midY
            if newCenter.y > superView.bounds.size.height - midPointY {
                newCenter.y = superView.bounds.size.height - midPointY
            }
            if newCenter.y < midPointY {
                newCenter.y = midPointY
            }
        }
        center = newCenter
    }
    
    func resize(usingTouchLocation touchPoint: CGPoint) {
        var deltaW = anchorPoint.adjustsW * (touchStart.x - touchPoint.x)
        let deltaX = anchorPoint.adjustsX * (-1.0 * deltaW)
        var deltaH = anchorPoint.adjustsH * (touchPoint.y - touchStart.y)
        let deltaY = anchorPoint.adjustsY * (-1.0 * deltaH)
        
        var newX = frame.origin.x + deltaX
        var newY = frame.origin.y + deltaY
        var newWidth  = frame.size.width + deltaW
        var newHeight = frame.size.height + deltaH
        
        if newWidth < minWidth {
            newWidth = frame.size.width
            newX = frame.origin.x
        }
        if newHeight < minHeight {
            newHeight = frame.size.height
            newY = frame.origin.y
        }
        
        if let superView = superview {
            if newX < superView.bounds.origin.x {
                deltaW = frame.origin.x - superView.bounds.origin.x
                newWidth = frame.size.width + deltaW
                newX = superView.bounds.origin.x
            }
            
            if newX + newWidth > superView.bounds.origin.x + superView.bounds.size.width {
                newWidth = superView.bounds.size.width - newX
            }
            
            if newY < superView.bounds.origin.y {
                deltaH = frame.origin.y - superView.bounds.origin.y
                newHeight = frame.size.height + deltaH
                newY = superView.bounds.origin.y
            }
            
            if newY + newHeight > superView.bounds.origin.y + superView.bounds.size.height {
                newHeight = superView.bounds.size.height - newY
            }
        }
        
        setFrame(CGRect(x: newX, y: newY, width: newWidth, height: newHeight))
        
        touchStart = touchPoint
    }
    
    func isResizing() -> Bool {
        return anchorPoint.adjustsH != 0 || anchorPoint.adjustsW != 0 || anchorPoint.adjustsX != 0 || anchorPoint.adjustsY != 0
    }
    
}

// MARK: - Public

extension ResizableView {
    
    func setFrame(_ newFrame: CGRect) {
        frame = newFrame
        contentView.frame = bounds.insetBy(dx: resizableViewGlobalInset + resizableViewInteractiveBorderSize / 2,
                                           dy: resizableViewGlobalInset + resizableViewInteractiveBorderSize / 2)
        
        borderView.frame = bounds.insetBy(dx: resizableViewGlobalInset, dy: resizableViewGlobalInset)
        borderView.setNeedsDisplay()
    }
    
}

// MARK: - Touches

extension ResizableView {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        delegate?.resizableViewDidBeginEditing(self)
        
        if editing {
            if let touch = touches.first {
                anchorPoint = anchorPoint(forTouchLocation: touch.location(in: self))
                touchStart = touch.location(in: superview)
                if !isResizing() {
                    touchStart = touch.location(in: self)
                }
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        
        delegate?.resizableViewDidMove(self)
        
        if editing {
            if isResizing() {
                if let superView = superview {
                    resize(usingTouchLocation: (touches.first?.location(in: superView))!)
                }
            } else {
                translate(usingTouchLocation: (touches.first?.location(in: self))!)
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        delegate?.resizableViewDidEndEditing(self)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        delegate?.resizableViewDidEndEditing(self)
    }
    
}


class BorderView: UIView {
    
    private let borderSize: CGFloat = 10
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func draw(_ rect: CGRect) {
        guard let ctx = UIGraphicsGetCurrentContext() else { return }
        ctx.saveGState()
        
        ctx.setLineWidth(1)
        ctx.setStrokeColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1))
        ctx.addRect(bounds.insetBy(dx: borderSize / 2,
                                   dy: borderSize / 2))
        ctx.strokePath()
        
        let middleLeft = CGRect(x: 0,
                                y: (bounds.size.height - borderSize) / 2,
                                width: borderSize,
                                height: borderSize)
        
        let middleRight = CGRect(x: bounds.size.width - borderSize,
                                 y: (bounds.size.height - borderSize) / 2,
                                 width: borderSize,
                                 height: borderSize)
        
        ctx.setLineWidth(1)
        
        let points: [CGRect] = [middleLeft, middleRight]
        
        points.forEach {
            let point = CGRect(x: $0.origin.x, y: $0.origin.y, width: 10, height: 10)
            
            ctx.saveGState()
            ctx.addEllipse(in: point)
            ctx.clip()
            ctx.setFillColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
            ctx.fill(point)
            ctx.restoreGState()
            ctx.setStrokeColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
            ctx.strokeEllipse(in: point.insetBy(dx: 0.5, dy: 0.5))
        }
        ctx.restoreGState()
    }
    
}

// MARK: - Private

private extension BorderView {
    
    func setup() {
        backgroundColor = .clear
        isUserInteractionEnabled = true
    }
    
}
