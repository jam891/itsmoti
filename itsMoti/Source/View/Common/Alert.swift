//
//  Alert.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/27/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

struct Alert {}

extension Alert {
    
    static func showErrorAlert(_ message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.view.layoutIfNeeded()
        present(alert)
    }
    
    static func showSuccessAlert(_ message: String, _ callback: @escaping () -> Void) {
        let alert = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            callback()
        }))
        alert.view.layoutIfNeeded()
        present(alert)
    }
    
}

// MARK: - Private

private extension Alert {
    
    static func present(_ alert: UIAlertController) {
        if let controller = UIApplication.shared.topViewController {
            controller.present(alert, animated: true)
        }
    }
    
}
