//
//  MaterialProgress.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/15/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

private let duration = 1.5
private let strokeRange = (start: 0.0, end: 0.8)

@IBDesignable
class MaterialProgress: IndeterminateAnimation {

    var backgroundRotationLayer = CAShapeLayer()
    
    var progressLayer: CAShapeLayer = {
        var tempLayer = CAShapeLayer()
        tempLayer.strokeEnd = CGFloat(strokeRange.end)
        tempLayer.lineCap = .round
        tempLayer.fillColor = UIColor.clear.cgColor
        return tempLayer
    }()
    
    var animationGroup: CAAnimationGroup = {
        var tempGroup = CAAnimationGroup()
        tempGroup.repeatCount = 1
        tempGroup.duration = duration
        return tempGroup
    }()
    
    var rotationAnimation: CABasicAnimation = {
        var tempRotation = CABasicAnimation(keyPath: "transform.rotation")
        tempRotation.repeatCount = HUGE
        tempRotation.fromValue = 0
        tempRotation.toValue = 1
        tempRotation.isCumulative = true
        tempRotation.isRemovedOnCompletion = false
        tempRotation.duration = duration / 2
        return tempRotation
    }()
    
    @IBInspectable
    var lineWidth: CGFloat = -1 {
        didSet { progressLayer.lineWidth = lineWidth }
    }
    
    override func notifyViewRedesigned() {
        super.notifyViewRedesigned()
        progressLayer.strokeColor = foreground.cgColor
    }
    
    func makeStrokeAnimationGroup() {
        var strokeStartAnimation: CABasicAnimation!
        var strokeEndAnimation: CABasicAnimation!
        
        func makeAnimationforKeyPath(_ keyPath: String) -> CABasicAnimation {
            let tempAnimation = CABasicAnimation(keyPath: keyPath)
            tempAnimation.repeatCount = 1
            tempAnimation.speed = 2
            tempAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
            
            tempAnimation.fromValue = strokeRange.start
            tempAnimation.toValue =  strokeRange.end
            tempAnimation.duration = duration
            
            return tempAnimation
        }
        strokeEndAnimation = makeAnimationforKeyPath("strokeEnd")
        strokeStartAnimation = makeAnimationforKeyPath("strokeStart")
        strokeStartAnimation.beginTime = duration / 2
        animationGroup.animations = [strokeEndAnimation, strokeStartAnimation]
        animationGroup.isRemovedOnCompletion = false
        animationGroup.fillMode = .forwards
        animationGroup.delegate = self
    }
    
    var currentRotation = 0.0
    let π2 = Double.pi * 2
    
    override func configureLayers() {
        super.configureLayers()
        makeStrokeAnimationGroup()
        let rect = bounds
        
        backgroundRotationLayer.frame = rect
        layer.addSublayer(backgroundRotationLayer)
        
        let radius = (rect.width / 2) * 0.6
        progressLayer.frame =  rect
        progressLayer.lineWidth = lineWidth == -1 ? radius / 5 : lineWidth
        let arcPath = UIBezierPath()
        arcPath.addArc(withCenter: CGPoint(x: rect.midX, y: rect.midY), radius: radius, startAngle: 0, endAngle: CGFloat(π2), clockwise: true)
        progressLayer.path = arcPath.cgPath
        backgroundRotationLayer.addSublayer(progressLayer)
    }
    
    override func startAnimation() {
        progressLayer.add(animationGroup, forKey: "strokeAnimation")
        backgroundRotationLayer.add(rotationAnimation, forKey: rotationAnimation.keyPath)
    }
    
    override func stopAnimation() {
        backgroundRotationLayer.removeAllAnimations()
        progressLayer.removeAllAnimations()
    }
    
}

// MARK: - CAAnimationDelegate

extension MaterialProgress: CAAnimationDelegate {
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if !animate { return }
        progressLayer.removeAnimation(forKey: "strokeAnimation")
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        currentRotation += (strokeRange.end * π2)
        currentRotation = currentRotation.truncatingRemainder(dividingBy: π2)
        progressLayer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(currentRotation)))
        CATransaction.commit()
        progressLayer.add(animationGroup, forKey: "strokeAnimation")
    }
    
}
