//
//  LoadingView.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/15/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class LoadingView: NibView {
    
    @IBOutlet weak var spinner: MaterialProgress!
    
    var animate: Bool = false {
        didSet { spinner.animate = animate }
    }

}
