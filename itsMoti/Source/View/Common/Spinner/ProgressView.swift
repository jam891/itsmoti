//
//  ProgressView.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/22/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

@IBDesignable
class ProgressView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureLayers()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureLayers()
    }
    
    func configureLayers() {
        notifyViewRedesigned()
    }
    
    @IBInspectable
    var background: UIColor = UIColor(red: 88.3 / 256, green: 104.4 / 256, blue: 118.5 / 256, alpha: 1.0) {
        didSet { notifyViewRedesigned() }
    }
    
    @IBInspectable
    var foreground: UIColor = UIColor(red: 66.3 / 256, green: 173.7 / 256, blue: 106.4 / 256, alpha: 1.0) {
        didSet { notifyViewRedesigned() }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 5.0 {
        didSet { notifyViewRedesigned() }
    }
    
    func notifyViewRedesigned() {
        layer.backgroundColor = background.cgColor
        layer.cornerRadius = cornerRadius
    }
    
}

