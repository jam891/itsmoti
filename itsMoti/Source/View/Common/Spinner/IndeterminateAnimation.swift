//
//  IndeterminateAnimation.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/15/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

protocol AnimationStatusDelegate {
    func startAnimation()
    func stopAnimation()
}

class IndeterminateAnimation: ProgressView, AnimationStatusDelegate {

    @IBInspectable
    var displayAfterAnimationEnds: Bool = false
    
    var animate: Bool = false {
        didSet {
            guard animate != oldValue else { return }
            if animate {
                isHidden = false
                startAnimation()
            } else {
                if !displayAfterAnimationEnds {
                    isHidden = true
                }
                stopAnimation()
            }
        }
    }
    
    func startAnimation() {
        fatalError("This is an abstract function")
    }
    
    func stopAnimation() {
        fatalError("This is an abstract function")
    }
    
}
