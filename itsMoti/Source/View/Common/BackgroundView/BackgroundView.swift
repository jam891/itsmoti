//
//  BackgroundView.swift
//  itsMoti
//
//  Created by Александр Васильченко on 2/4/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class BackgroundView: NibView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
//        guard let view = loadViewFromNib() else { return }
//        view.frame = self.bounds
//        self.addSubview(view)
    }
    
//    private func loadViewFromNib() -> UIView? {
//        let bundle = Bundle(for: type(of: self))
//        let nib = UINib(nibName: "BackgroundView", bundle: bundle)
//        
//        return nib.instantiate(withOwner: self, options: nil).first as? UIView
//    }
}

