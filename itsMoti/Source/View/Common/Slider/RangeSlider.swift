//
//  RangeSlider.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/16/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

@IBDesignable
class RangeSlider: UIControl {
    
    @IBInspectable
    var minimumValue: Double = 0.0 {
        didSet { updateLayerFrames() }
    }
    
    @IBInspectable
    var maximumValue: Double = 1.0 {
        didSet { updateLayerFrames() }
    }
    
    @IBInspectable
    var lowerValue: Double = 0.0 {
        didSet { updateLayerFrames() }
    }
    
    @IBInspectable
    var upperValue: Double = 1.0 {
        didSet { updateLayerFrames() }
    }
    
    @IBInspectable
    var minimumRange: Double = 0.1 {
        didSet { updateLayerFrames() }
    }
    
    var progress: Double = 0.0 {
        didSet { updateLayerFrames() }
    }
    
    @IBInspectable
    var trackTintColor: UIColor = UIColor(white: 0.9, alpha: 1) {
        didSet { trackLayer.setNeedsDisplay() }
    }
    
    @IBInspectable
    var trackHighlightTintColor: UIColor = UIColor(red: 0.0, green: 0.45, blue: 0.94, alpha: 1) {
        didSet { trackLayer.setNeedsDisplay() }
    }
    
    @IBInspectable
    var traсkProgressTintColor: UIColor = UIColor(red: 0.0, green: 0.45, blue: 0.94, alpha: 1) {
        didSet { trackLayer.setNeedsDisplay() }
    }
    
    @IBInspectable
    var thumbProgressTintColor: UIColor = UIColor(red: 0.0, green: 0.45, blue: 0.94, alpha: 1) {
        didSet { trackLayer.setNeedsDisplay() }
    }
    
    @IBInspectable
    var thumbTintColor: UIColor = UIColor.white {
        didSet {
            lowerThumbLayer.setNeedsDisplay()
            upperThumbLayer.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var curvaceousness: CGFloat = 1.0 {
        didSet {
            trackLayer.setNeedsDisplay()
            lowerThumbLayer.setNeedsDisplay()
            upperThumbLayer.setNeedsDisplay()
        }
    }
    
    let trackLayer      = RangeSliderTrackLayer()
    let progressLayer   = RangeSliderProgressLayer()
    let lowerThumbLayer = RangeSliderThumbLayer()
    let upperThumbLayer = RangeSliderThumbLayer()
    
    var previousLocation = CGPoint()
    
    var thumbWidth: CGFloat {
        return CGFloat(bounds.height)
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateLayerFrames()
    }
    
}

// MARK: - Private

private extension RangeSlider {
    
    func setup() {
        trackLayer.rangeSlider = self
        trackLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(trackLayer)
        
        progressLayer.rangeSlider = self
        progressLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(progressLayer)
        
        lowerThumbLayer.rangeSlider = self
        lowerThumbLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(lowerThumbLayer)
        
        upperThumbLayer.rangeSlider = self
        upperThumbLayer.contentsScale = UIScreen.main.scale
        layer.addSublayer(upperThumbLayer)
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        trackLayer.frame = CGRect(x: bounds.origin.x + 12, y: bounds.midY, width: bounds.width - 24, height: 2)
        trackLayer.setNeedsDisplay()
        
        let progressThumbCenter = CGFloat(positionForValue(progress))
        progressLayer.frame = CGRect(x: progressThumbCenter - thumbWidth / 2, y: 1, width: thumbWidth, height: thumbWidth)
        progressLayer.setNeedsDisplay()
        
        let lowerThumbCenter = CGFloat(positionForValue(lowerValue))
        lowerThumbLayer.frame = CGRect(x: lowerThumbCenter - thumbWidth / 2, y: 0, width: thumbWidth, height: thumbWidth)
        lowerThumbLayer.setNeedsDisplay()
        
        let upperThumbCenter = CGFloat(positionForValue(upperValue))
        upperThumbLayer.frame = CGRect(x: upperThumbCenter - thumbWidth / 2, y: 0, width: thumbWidth, height: thumbWidth)
        upperThumbLayer.setNeedsDisplay()
        
        CATransaction.commit()
    }
    
    func positionForValue(_ value: Double) -> Double {
        return Double(bounds.width - thumbWidth) * (value - minimumValue) /
            (maximumValue - minimumValue) + Double(thumbWidth / 2)
    }
    
    func boundValue(_ value: Double, toLowerValue lowerValue: Double, upperValue: Double) -> Double {
        return min(max(value, lowerValue), upperValue)
    }
    
}

// MARK: - Events

extension RangeSlider {
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        previousLocation = touch.location(in: self)
        
        if lowerThumbLayer.frame.contains(previousLocation) {
            lowerThumbLayer.highlighted = true
        } else if upperThumbLayer.frame.contains(previousLocation) {
            upperThumbLayer.highlighted = true
        }
        return lowerThumbLayer.highlighted || upperThumbLayer.highlighted
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let location = touch.location(in: self)
        let deltaLocation = Double(location.x - previousLocation.x)
        let deltaValue = (maximumValue - minimumValue) * deltaLocation / Double(bounds.width - thumbWidth)
        
        previousLocation = location
        
        if lowerThumbLayer.highlighted {
            lowerValue += deltaValue
            lowerValue = boundValue(lowerValue, toLowerValue: minimumValue, upperValue: upperValue - minimumRange)
        } else if upperThumbLayer.highlighted {
            upperValue += deltaValue
            upperValue = boundValue(upperValue, toLowerValue: lowerValue + minimumRange, upperValue: maximumValue)
        }
        sendActions(for: .valueChanged)
        return true
    }
    
    override func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        lowerThumbLayer.highlighted = false
        upperThumbLayer.highlighted = false
    }
    
}

// MARK: - Thumb Layer

class RangeSliderThumbLayer: CALayer {
    var highlighted: Bool = false {
        didSet { setNeedsDisplay() }
    }
    weak var rangeSlider: RangeSlider?
    
    override func draw(in ctx: CGContext) {
        if let slider = rangeSlider {
            let thumbFrame = bounds.insetBy(dx: 2, dy: 2)
            let cornerRadius = thumbFrame.height * slider.curvaceousness / 2
            let thumbPath = UIBezierPath(roundedRect: thumbFrame, cornerRadius: cornerRadius)
            let shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
            let borderColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15)
            
            ctx.setShadow(offset: CGSize(width: 0, height: 2), blur: 2, color: shadowColor.cgColor)
            ctx.setFillColor(slider.thumbTintColor.cgColor)
            ctx.addPath(thumbPath.cgPath)
            ctx.fillPath()
            
            ctx.setStrokeColor(borderColor.cgColor)
            ctx.setLineWidth(0.5)
            ctx.addPath(thumbPath.cgPath)
            ctx.strokePath()
            
            if highlighted {
                ctx.setFillColor(UIColor(white: 0, alpha: 0.1).cgColor)
                ctx.addPath(thumbPath.cgPath)
                ctx.fillPath()
            }
        }
    }
    
}

// MARK: - Track Layer

class RangeSliderTrackLayer: CALayer {
    weak var rangeSlider: RangeSlider?
    
    override func draw(in ctx: CGContext) {
        if let slider = rangeSlider {
            let cornerRadius = bounds.height * slider.curvaceousness / 2
            let path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
            ctx.addPath(path.cgPath)
            
            ctx.setFillColor(slider.trackTintColor.cgColor)
            ctx.addPath(path.cgPath)
            ctx.fillPath()
            
            ctx.setFillColor(slider.trackHighlightTintColor.cgColor)
            let lowerValuePosition = CGFloat(slider.positionForValue(slider.lowerValue))
            let upperValuePosition = CGFloat(slider.positionForValue(slider.upperValue))
            let rect = CGRect(x: lowerValuePosition, y: 0, width: upperValuePosition - lowerValuePosition, height: bounds.height)
            ctx.fill(rect)
        }
    }
    
}

// MARK: - Progress Layer

class RangeSliderProgressLayer: CALayer {
    weak var rangeSlider: RangeSlider?
    
    override func draw(in ctx: CGContext) {
        if let slider = rangeSlider {
            let thumbFrame = bounds.insetBy(dx: 12, dy: 4)
            let thumbPath = UIBezierPath(rect: thumbFrame)
            ctx.setFillColor(slider.thumbProgressTintColor.cgColor)
            ctx.addPath(thumbPath.cgPath)
            ctx.fillPath()
        }
    }
    
}


