//
//  TextViewCell.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 11/14/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class TextViewCell: UITableViewCell {
    var tableView: UITableView? {
        var view: UIView? = superview
        while !(view is UITableView) && view != nil {
            view = view?.superview
        }
        return view as? UITableView
    }
}

// MARK: - UITextViewDelegate

extension TextViewCell: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        let size = textView.bounds.size
        let newSize = textView.sizeThatFits(CGSize(width: size.width, height: .greatestFiniteMagnitude))
        
        if size.height != newSize.height {
            tableView?.beginUpdates()
            tableView?.endUpdates()
        }
    }
    
}

