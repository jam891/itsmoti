//
//  SettingsViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/28/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

}

// MARK: - Action

extension SettingsViewController {
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
}

extension SettingsViewController {
    
}
