//
//  MarkupViewController.swift
//  MarkupDemo
//
//  Created by Vitaliy Delidov on 1/11/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

protocol MarkupViewControllerDelegate: class {
    func markupViewControllerDidCancel(_ controller: MarkupViewController)
    func markupViewController(_ controller: MarkupViewController, photoDidEndEditing textImage: UIImage)
}

class MarkupViewController: UIViewController {
    
    @IBOutlet weak var drawBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var sizeBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var drawableView: DrawableView!
    
    private var resizableView: ResizableView!
    private var textView: UITextView!
    
    private var currentEditingView: ResizableView!
    private var currentTextView: UITextView!
    private var selectedColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) {
        didSet {
            if let textView = currentTextView {
                if currentEditingView.editing {
                    textView.textColor = selectedColor
                }
            }
            drawableView.brush.color = selectedColor
        }
    }
    
    weak var delegate: MarkupViewControllerDelegate?
    
    var image: UIImage!
    
    private var colors = [
        #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), #colorLiteral(red: 0.7490196078, green: 0.3529411765, blue: 0.9490196078, alpha: 1), #colorLiteral(red: 1, green: 0.2156862745, blue: 0.3725490196, alpha: 1), #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1), #colorLiteral(red: 1, green: 0.8392156863, blue: 0.03921568627, alpha: 1), #colorLiteral(red: 0.6745098039, green: 0.5568627451, blue: 0.4078431373, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), #colorLiteral(red: 1, green: 0.2705882353, blue: 0.2274509804, alpha: 1), #colorLiteral(red: 0.1960784314, green: 0.8431372549, blue: 0.2941176471, alpha: 1), #colorLiteral(red: 0.03921568627, green: 0.5176470588, blue: 1, alpha: 1),
        #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1), #colorLiteral(red: 0.09019608051, green: 0, blue: 0.3019607961, alpha: 1), #colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1), #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1), #colorLiteral(red: 0.5058823824, green: 0.3372549117, blue: 0.06666667014, alpha: 1), #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1), #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1), #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1), #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1), #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1), #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}

// MARK: - Navigation

extension MarkupViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segueIdentifier(for: segue) else { return }
        
        switch identifier {
        case .drawPopover:
            let popoverViewController = segue.destination as! DrawPopoverController
            popoverViewController.popoverPresentationController!.delegate = self
            popoverViewController.index = Int(drawableView.brush.width / 5 - 1)
            popoverViewController.value = Float(drawableView.brush.alpha)
            popoverViewController.modalPresentationStyle = .popover
            popoverViewController.indexChanged = { sender in
                self.drawableView.brush.width = CGFloat(5 * (sender.selectedSegmentIndex + 1))
            }
            popoverViewController.valueChanged = { sender in
                self.drawableView.brush.alpha = CGFloat(sender.value)
            }
        case .textPopover:
            let popoverViewController = segue.destination as! TextPopoverController
            popoverViewController.popoverPresentationController!.delegate = self
            popoverViewController.value = Float(textView.font!.pointSize)
            popoverViewController.modalPresentationStyle = .popover
            popoverViewController.valueChanged = { sender in
                self.currentTextView.font = UIFont.systemFont(ofSize: CGFloat(sender.value))
                self.textViewDidChange(self.currentTextView)
            }
        default:
            break
        }
    }
    
}

// MARK: - UIPopoverPresentationControllerDelegate

extension MarkupViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

// MARK: - UICollectionViewDataSource

extension MarkupViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: ColorCell.self, for: indexPath)
        cell.color = colors[indexPath.item]
        return cell
    }
    
}

// MARK: - UICollectionViewDelegate

extension MarkupViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedColor = colors[indexPath.item]
    }
    
}

// MARK: - Private

private extension MarkupViewController {
    
    func setup() {
        sizeBarButtonItem.isEnabled = false
        drawableView.brush.color = selectedColor
        drawableView.backgroundImageView.image = image
        
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .right)
    }
    
    func addTextView() {
        setupTextView()
        setupResizableView()
        addGestureRecognizer()
    }
    
    func setupTextView() {
        textView = UITextView(frame: CGRect(x: 0, y: 0, width: 120, height: 70))
        textView.text = "Text"
        textView.tintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        textView.delegate = self
        textView.textAlignment = .center
        textView.isScrollEnabled = false
        textView.backgroundColor = .clear
        textView.textColor = selectedColor
        textView.isUserInteractionEnabled = false
        textView.font = UIFont.systemFont(ofSize: 30)
        textView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        currentTextView = textView
    }
    
    func setupResizableView() {
        resizableView = ResizableView(frame: currentTextView.bounds)
        resizableView.contentView = textView
        resizableView.center = view.center
        resizableView.delegate = self
        resizableView.editing = true
        
        view.addSubview(resizableView)
        
        currentEditingView = resizableView
    }
    
    func addGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(showMenu))
        resizableView.addGestureRecognizer(tap)
    }
    
    func setupMenu() {
        let menu = UIMenuController.shared
        menu.setTargetRect(currentEditingView.frame, in: view)
        
        let deleteMenuItem = UIMenuItem(title: "Delete", action: #selector(deleteText))
        let editMenuItem   = UIMenuItem(title: "Edit", action: #selector(editingText))
        
        menu.menuItems = [deleteMenuItem, editMenuItem]
        menu.setMenuVisible(true, animated: true)
    }
    
    func screenshot() -> UIImage {
        let rect = drawableView.backgroundImageView.contentClippingRect
        let size = CGSize(width: rect.width, height: rect.height - 1)
        let bounds = CGRect(origin: rect.origin, size: size)
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            let ctx = rendererContext.cgContext
            ctx.translateBy(x: 0, y: 0)
            view.layer.render(in: ctx)
        }
    }
    
    func updateUI() {
        if drawableView.isEditing {
            drawBarButtonItem.isEnabled = true
            sizeBarButtonItem.isEnabled = false
        } else {
            drawBarButtonItem.isEnabled = false
            sizeBarButtonItem.isEnabled = true
        }
    }
    
    func endEditing() {
        view.subviews.forEach {
            if let resizableView = $0 as? ResizableView {
                resizableView.editing = false
            }
        }
        view.endEditing(true)
    }
    
    
}

// MARK: - Action

extension MarkupViewController {
    
    @IBAction func undo(_ sender: UIBarButtonItem) {
        drawableView.undo()
    }
    
    @IBAction func draw(_ sender: UIBarButtonItem) {
        drawableView.isEditing = true
        performSegue(withIdentifier: .drawPopover)
        updateUI()
    }
    
    @IBAction func type(_ sender: UIBarButtonItem) {
        drawableView.isEditing = false
        addTextView()
        updateUI()
    }
    
    @IBAction func size(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: .textPopover)
    }
    
    @IBAction func didTap(_ sender: UITapGestureRecognizer) {
        drawableView.isEditing = true
        endEditing()
        updateUI()
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        collectionView.isHidden = true
        endEditing()
        delegate?.markupViewController(self, photoDidEndEditing: screenshot())
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        delegate?.markupViewControllerDidCancel(self)
    }
    
}

// MARK: - ResizableViewDelegate

extension MarkupViewController: ResizableViewDelegate {
    
    func resizableViewDidBeginEditing(_ resizableView: ResizableView) {
        UIMenuController.shared.isMenuVisible = false
        currentEditingView = resizableView
    }
    
    func resizableViewDidMove(_ resizableView: ResizableView) {
        textViewDidChange(currentTextView)
    }
    
    func resizableViewDidEndEditing(_ resizableView: ResizableView) {
    
    }
    
}

// MARK: - Menu

@objc
extension MarkupViewController {
    
    func showMenu(_ sender: UITapGestureRecognizer) {
        if let resizableView = sender.view as? ResizableView {
            currentEditingView.becomeFirstResponder()
            currentEditingView = resizableView
            currentEditingView.editing = true
            
            if let textView = currentEditingView.contentView as? UITextView {
                textViewDidChange(textView)
            }
            drawableView.isEditing = false
            setupMenu()
            updateUI()
        }
    }
    
    func editingText() {
        if let textView = currentEditingView.contentView as? UITextView {
            textView.becomeFirstResponder()
            currentTextView = textView
        }
    }
    
    func deleteText() {
        currentEditingView.removeFromSuperview()
    }
    
}

// MARK: - UITextViewDelegate

extension MarkupViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        currentTextView = textView
    }
    
    func textViewDidChange(_ textView: UITextView) {
        currentTextView = textView
        
        let offset: CGFloat = 20
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: .greatestFiniteMagnitude))
        textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        let size = CGSize(width: currentEditingView.frame.width, height: newSize.height + offset)
        let frame = CGRect(origin: currentEditingView.frame.origin, size: size)
        
        currentEditingView.setFrame(frame)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        currentTextView = textView
    }
    
}

