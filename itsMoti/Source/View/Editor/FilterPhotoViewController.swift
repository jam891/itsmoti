//
//  FilterPhotoViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/20/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

protocol FilterPhotoViewControllerDelegate: class {
    func filterPhotoControllerDidCancel(_ controller: FilterPhotoViewController)
    func filterPhotoController(_ controller: FilterPhotoViewController, photoDidEndEditing filteredImage: UIImage)
}

class FilterPhotoViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageView: UIImageView!
    
    private var filters = Filter.allCases
    
    var filterDidTap: ((_ controller: FilterPhotoViewController, _ image: UIImage) -> Void)?
    var cropDidTap: ((_ controller: FilterPhotoViewController, _ image: UIImage) -> Void)?
    var textDidTap: ((_ controller: FilterPhotoViewController, _ image: UIImage) -> Void)?
    
    weak var delegate: FilterPhotoViewControllerDelegate?
    
    var image: UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.hairline?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.hairline?.isHidden = false
    }
    
    func filterAction() {
        if let image = imageView.image {
            self.delegate?.filterPhotoController(self, photoDidEndEditing: image)
        }
    }
  
}

//MARK: - Private

private extension FilterPhotoViewController {
    
    func setup() {
        guard let image = image else { return }
        imageView.image = image
        
        let indexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: indexPath, animated: false, scrollPosition: .right)
    }
    
    func configure(_ cell: FilterCell, at indexPath: IndexPath) {
        let filter = filters[indexPath.item]
        cell.filterLabel.text = filter.name
        
        if indexPath.item == 0 {
            cell.imageView.image = image
        } else {
            DispatchQueue.global(qos: .default).async {
                let filtered = self.image?.applyFilter(name: filter.rawValue)
                DispatchQueue.main.async {
                    cell.imageView.image = filtered
                }
            }
        }
    }
    
}

// MARK: - Action

extension FilterPhotoViewController {
    
    @IBAction func crop(_ sender: UIBarButtonItem) {
        if let image = imageView.image {
            cropDidTap?(self, image)
        }
    }
    
    @IBAction func filter(_ sender: UIBarButtonItem) {
        if let image = imageView.image {
            filterDidTap?(self, image)
        }
    }
    
    @IBAction func text(_ sender: UIBarButtonItem) {
        if let image = imageView.image {
            textDidTap?(self, image)
        }
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        if let image = imageView.image {
            delegate?.filterPhotoController(self, photoDidEndEditing: image)
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        delegate?.filterPhotoControllerDidCancel(self)
    }
    
}

// MARK: - UICollectionViewDataSource

extension FilterPhotoViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: FilterCell.self, for: indexPath)
        configure(cell, at: indexPath)
        return cell
    }
    
}

// MARK: - UICollectionViewDelegate

extension FilterPhotoViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        let filter = filters[indexPath.item]
        
        if indexPath.item == 0 {
            imageView?.image = image
        } else {
            imageView?.image = image?.applyFilter(name: filter.rawValue)
        }
    }
    
}
