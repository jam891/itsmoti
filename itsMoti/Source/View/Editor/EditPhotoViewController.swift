//
//  EditPhotoViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/19/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit
import IGRPhotoTweaks

protocol EditPhotoViewControllerDelegate: class {
    func editPhotoControllerDidCancel(_ controller: EditPhotoViewController)
    func editPhotoController(_ controller: EditPhotoViewController, photoDidEndEditing photo: UIImage)
}

class EditPhotoViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    weak var delegate: EditPhotoViewControllerDelegate?
    var image: UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.hairline?.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.hairline?.isHidden = false
    }
    
}

// MARK: - Navigation

extension EditPhotoViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segueIdentifier(for: segue) else { return }
        switch identifier {
        case .cropPhoto:
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! CropPhotoViewController
            controller.image = sender as? UIImage
            controller.delegate = self
            configure(controller)
        case .filterPhoto:
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! FilterPhotoViewController
            controller.image = sender as? UIImage
            controller.delegate = self
            configure(controller)
        case .textPhoto:
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! MarkupViewController
            controller.image = sender as? UIImage
            controller.delegate = self
//            configure(controller)
        default:
            break
        }
    }
    
}

// MARK: - Private

private extension EditPhotoViewController {
    
    func setup() {
        guard let image = image else { return }
        imageView.image = image
    }
    
    func configure(_ controller: CropPhotoViewController) {
        controller.filterDidTap = { controller, image in
            controller.dismiss(animated: false) {
                self.imageView.image = image
                self.performSegue(withIdentifier: .filterPhoto, sender: image)
            }
        }
        controller.cropDidTap = { controller, image in
            controller.dismiss(animated: false) {
                self.imageView.image = image
            }
        }
        controller.textDidTap = { controller, image in
            controller.dismiss(animated: false) {
                self.imageView.image = image
                self.performSegue(withIdentifier: .textPhoto, sender: image)
            }
        }
    }
    
    func configure(_ controller: FilterPhotoViewController) {
        controller.cropDidTap = { controller, image in
            controller.dismiss(animated: false) {
                self.imageView.image = image
                self.performSegue(withIdentifier: .cropPhoto, sender: image)
            }
        }
        controller.filterDidTap = { controller, image in
            controller.dismiss(animated: false) {
                self.imageView.image = image
            }
        }
        controller.textDidTap = { controller, image in
            controller.dismiss(animated: false) {
                self.imageView.image = image
                self.performSegue(withIdentifier: .textPhoto, sender: image)
            }
        }
    }
    
}

// MARK: - Action

extension EditPhotoViewController {
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        delegate?.editPhotoControllerDidCancel(self)
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        if let image = imageView.image {
            delegate?.editPhotoController(self, photoDidEndEditing: image)
        }
    }
    
    @IBAction func crop(_ sender: UIBarButtonItem) {
        if let image = imageView.image {
            performSegue(withIdentifier: .cropPhoto, sender: image)
        }
        
    }
    
    @IBAction func filter(_ sender: UIBarButtonItem) {
        if let image = imageView.image {
            performSegue(withIdentifier: .filterPhoto, sender: image)
        }
    }
    
    @IBAction func text(_ sender: UIBarButtonItem) {
        if let image = imageView.image {
            performSegue(withIdentifier: .textPhoto, sender: image)
        }
    }
    
}

// MARK: - IGRPhotoTweakViewControllerDelegate

extension EditPhotoViewController: IGRPhotoTweakViewControllerDelegate {

    func photoTweaksControllerDidCancel(_ controller: IGRPhotoTweakViewController) {
        controller.dismiss(animated: false) {
            self.delegate?.editPhotoControllerDidCancel(self)
        }
    }
    
    func photoTweaksController(_ controller: IGRPhotoTweakViewController, didFinishWithCroppedImage croppedImage: UIImage) {
        imageView.image = croppedImage
        
        controller.dismiss(animated: false) {
            self.delegate?.editPhotoController(self, photoDidEndEditing: croppedImage)
        }
    }
    
}

// MARK: - FilterPhotoViewControllerDelegate

extension EditPhotoViewController: FilterPhotoViewControllerDelegate {
    
    func filterPhotoControllerDidCancel(_ controller: FilterPhotoViewController) {
        controller.dismiss(animated: false) {
            self.delegate?.editPhotoControllerDidCancel(self)
        }
    }
    
    func filterPhotoController(_ controller: FilterPhotoViewController, photoDidEndEditing filteredImage: UIImage) {
        imageView.image = filteredImage
        
        controller.dismiss(animated: false) {
            self.delegate?.editPhotoController(self, photoDidEndEditing: filteredImage)
        }
    }
    
}

// MARK: - TextPhotoViewControllerDelegate

extension EditPhotoViewController: MarkupViewControllerDelegate {
    
    func markupViewControllerDidCancel(_ controller: MarkupViewController) {
        controller.dismiss(animated: false) {
            self.delegate?.editPhotoControllerDidCancel(self)
        }
    }
    
    func markupViewController(_ controller: MarkupViewController, photoDidEndEditing textImage: UIImage) {
        imageView.image = textImage
        
        controller.dismiss(animated: false) {
            self.delegate?.editPhotoController(self, photoDidEndEditing: textImage)
        }
    }
    
}

