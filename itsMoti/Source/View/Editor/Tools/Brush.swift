//
//  Brush.swift
//  MarkupDemo
//
//  Created by Vitaliy Delidov on 1/11/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

struct Brush {
    var color: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    var width: CGFloat = 5
    var alpha: CGFloat = 1
    var blendMode: CGBlendMode = .normal
    var isEraser: Bool = false
}
