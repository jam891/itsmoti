//
//  Filter.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/19/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

enum Filter: String, CaseIterable {
    case normal   = "No Filter"
    case schrome  = "CIPhotoEffectChrome"
    case fade     = "CIPhotoEffectFade"
    case instant  = "CIPhotoEffectInstant"
    case mono     = "CIPhotoEffectMono"
    case noir     = "CIPhotoEffectNoir"
    case process  = "CIPhotoEffectProcess"
    case tonal    = "CIPhotoEffectTonal"
    case transfer = "CIPhotoEffectTransfer"
    case tone     = "CILinearToSRGBToneCurve"
    case linear   = "CISRGBToneCurveToLinear"
    case sepia    = "CISepiaTone"
    
    var name: String {
        switch self {
        case .normal:   return "Normal"
        case .schrome:  return "Chrome"
        case .fade:     return "Fade"
        case .instant:  return "Instant"
        case .mono:     return "Mono"
        case .noir:     return "Noir"
        case .process:  return "Process"
        case .tonal:    return "Tonal"
        case .transfer: return "Transfer"
        case .tone:     return "Tone"
        case .linear:   return "Linear"
        case .sepia:    return "Sepia"
        }
    }
}
