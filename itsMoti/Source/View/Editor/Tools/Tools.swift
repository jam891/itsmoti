//
//  Tools.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/19/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class Tools {

}

extension Tools {
    
    static func clampImage() -> UIImage {
        var clampImage: UIImage!
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 22, height: 16), false, 0)
        
        let outerBox = UIColor(red: 1, green: 1, blue: 1, alpha: 0.773)
        let innerBox = UIColor(red: 1, green: 1, blue: 1, alpha: 0.773)
    
        let rectanglePath = UIBezierPath(rect: CGRect(x: 0, y: 3, width: 13, height: 13))
        UIColor.white.setFill()
        rectanglePath.fill()
        
        let topPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 22, height: 2))
        outerBox.setFill()
        topPath.fill()
    
        let sidePath = UIBezierPath(rect: CGRect(x: 19, y: 2, width: 3, height: 14))
        outerBox.setFill()
        sidePath.fill()

        let rectangle2Path = UIBezierPath(rect: CGRect(x: 14, y: 3, width: 4, height: 13))
        innerBox.setFill()
        rectangle2Path.fill()
        
        
        clampImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return clampImage
    }
    
    static func rotateImage() -> UIImage {
        var rotateImage: UIImage!
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 18, height: 21), false, 0)

        let rectangle2Path = UIBezierPath(rect: CGRect(x: 0, y: 9, width: 12, height: 12))
        UIColor.white.setFill()
        rectangle2Path.fill()
        
        let rectangle3Path = UIBezierPath()
        rectangle3Path.move(to: CGPoint(x: 5, y: 3))
        rectangle3Path.addLine(to: CGPoint(x: 10, y: 6))
        rectangle3Path.addLine(to: CGPoint(x: 10, y: 0))
        rectangle3Path.addLine(to: CGPoint(x: 5, y: 3))
        rectangle3Path.close()
        UIColor.white.setFill()
        rectangle3Path.fill()
        
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 10, y: 3))
        bezierPath.addCurve(to: CGPoint(x: 17.5, y: 11),
                            controlPoint1: CGPoint(x: 15, y: 3),
                            controlPoint2: CGPoint(x: 17.5, y: 5.91))
        
        UIColor.white.setStroke()
        bezierPath.lineWidth = 1
        bezierPath.stroke()
        
        rotateImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return rotateImage
    }
    
}
