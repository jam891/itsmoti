//
//  Session.swift
//  MarkupDemo
//
//  Created by Vitaliy Delidov on 1/11/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class Session: NSObject {
    private let maxSessionSize = 50
    private var undoSessionList = [Drawing]()
    private var redoSessionList = [Drawing]()
    private var backgroundSession: Drawing?
    
    var canUndo: Bool {
        return undoSessionList.count > 0
    }
    
    var canRedo: Bool {
        return redoSessionList.count > 0
    }
    
    var canReset: Bool {
        return canUndo || canRedo
    }
    
}
    
private extension Session {
    
    func appendUndo(_ session: Drawing?) {
        if session == nil { return }
        if undoSessionList.count >= maxSessionSize {
            undoSessionList.removeFirst()
        }
        undoSessionList.append(session!)
    }
    
    func appendRedo(_ session: Drawing?) {
        if session == nil { return }
        if redoSessionList.count >= maxSessionSize {
            redoSessionList.removeFirst()
        }
        redoSessionList.append(session!)
    }
    
    func resetUndo() {
        undoSessionList.removeAll()
    }
    
    func resetRedo() {
        redoSessionList.removeAll()
    }
    
}

extension Session {

    func lastSession() -> Drawing? {
        if undoSessionList.last != nil {
            return undoSessionList.last
        } else if backgroundSession != nil {
            return backgroundSession
        }
        return nil
    }
    
    func appendBackground(_ session: Drawing?) {
        if session != nil {
            backgroundSession = session
        }
    }
    
    func append(_ session: Drawing?) {
        appendUndo(session)
        resetRedo()
    }
    
    func undo() {
        let lastSession = undoSessionList.last
        if lastSession != nil {
            appendRedo(lastSession!)
            undoSessionList.removeLast()
        }
    }
    
    func redo() {
        let lastSession = redoSessionList.last
        if lastSession != nil {
            appendUndo(lastSession!)
            redoSessionList.removeLast()
        }
    }
    
    func clear() {
        resetUndo()
        resetRedo()
    }
    
}
