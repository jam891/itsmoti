//
//  CropPhotoViewController.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/19/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import IGRPhotoTweaks
import HorizontalDial

class CropPhotoViewController: IGRPhotoTweakViewController {

    @IBOutlet weak var angleLabel: UILabel!
    @IBOutlet weak var clampButton: UIButton!
    @IBOutlet weak var rotateButton: UIButton!
    @IBOutlet weak var horizontalDial: HorizontalDial! {
        didSet { horizontalDial.migneticOption = .none }
    }
    
    var filterDidTap: ((_ controller: CropPhotoViewController, _ image: UIImage) -> Void)?
    var cropDidTap: ((_ controller: CropPhotoViewController, _ image: UIImage) -> Void)?
    var textDidTap: ((_ controller: CropPhotoViewController, _ image: UIImage) -> Void)?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func willTransition(to newCollection: UITraitCollection,
                                 with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        coordinator.animate(alongsideTransition: { context in
            self.view.layoutIfNeeded()
        })
    }

}

// MARK: - Private

private extension CropPhotoViewController {
    
    func setup() {
        rotateButton.setImage(Tools.rotateImage(), for: .normal)
        clampButton.setImage(Tools.clampImage(), for: .normal)
    }
    
    func updateLabel(radians: CGFloat) {
        let intDegrees = Int(IGRRadianAngle.toDegrees(radians))
        angleLabel?.text = "\(intDegrees)°"
    }
    
}

// MARK: - Action

extension CropPhotoViewController {
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismissAction()
    }
    
    @IBAction func done(_ sender: UIBarButtonItem) {
        cropAction()
    }
    
    @IBAction func filter(_ sender: UIBarButtonItem) {
        if let image = cropImage() {
            filterDidTap?(self, image)
        }
    }
    
    @IBAction func crop(_ sender: UIBarButtonItem) {
        if let image = cropImage() {
            cropDidTap?(self, image)
        }
    }
    
    @IBAction func text(_ sender: UIBarButtonItem) {
        if let image = cropImage() {
            textDidTap?(self, image)
        }
    }
    
    
    @IBAction func reset(_ sender: UIButton) {
        horizontalDial.value = 0
        updateLabel(radians: 0)
        resetView()
    }
    
    @IBAction func rotate(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5) {
            self.horizontalDial.value -= 90
        }
        
        if horizontalDial.value < -360 {
            horizontalDial.value = -90
        }
    }
    
    @IBAction func clamb(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Original", style: .default) { (action) in
            self.resetAspectRect()
        })
        actionSheet.addAction(UIAlertAction(title: "Squere", style: .default) { (action) in
            self.setCropAspectRect(aspect: "1:1")
        })
        actionSheet.addAction(UIAlertAction(title: "2:3", style: .default) { (action) in
            self.setCropAspectRect(aspect: "2:3")
        })
        actionSheet.addAction(UIAlertAction(title: "3:5", style: .default) { (action) in
            self.setCropAspectRect(aspect: "3:5")
        })
        actionSheet.addAction(UIAlertAction(title: "3:4", style: .default) { (action) in
            self.setCropAspectRect(aspect: "3:4")
        })
        actionSheet.addAction(UIAlertAction(title: "5:7", style: .default) { (action) in
            self.setCropAspectRect(aspect: "5:7")
        })
        actionSheet.addAction(UIAlertAction(title: "9:16", style: .default) { (action) in
            self.setCropAspectRect(aspect: "9:16")
        })
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        actionSheet.view.layoutIfNeeded()
        present(actionSheet, animated: true, completion: nil)
    }
    
}

// MARK: - HorizontalDialDelegate

extension CropPhotoViewController: HorizontalDialDelegate {
    
    func horizontalDialDidValueChanged(_ horizontalDial: HorizontalDial) {
        let degrees = horizontalDial.value
        let radians = IGRRadianAngle.toRadians(CGFloat(degrees))
        
        updateLabel(radians: radians)
        changedAngle(value: radians)
    }
    
    func horizontalDialDidEndScroll(_ horizontalDial: HorizontalDial) {
        stopChangeAngle()
    }
    
}


