//
//  TextPopoverController.swift
//  MarkupDemo
//
//  Created by Vitaliy Delidov on 1/14/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class TextPopoverController: UIViewController {
    
    @IBOutlet weak var slider: UISlider!
    
    var valueChanged: ((_ sender: UISlider) -> Void)?
    var value: Float = 1
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}

private extension TextPopoverController {
    
    func setup() {
        slider.value = value
    }
    
}

extension TextPopoverController {
    
    @IBAction func valueChanged(_ sender: UISlider) {
        valueChanged?(sender)
    }
    
}
