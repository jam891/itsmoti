//
//  DrawPopoverController.swift
//  MarkupDemo
//
//  Created by Vitaliy Delidov on 1/14/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class DrawPopoverController: UIViewController {
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var indexChanged: ((_ sender: UISegmentedControl) -> Void)?
    var valueChanged: ((_ sender: UISlider) -> Void)?
    
    var value: Float = 1
    var index: Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
}

private extension DrawPopoverController {
    
    func setup() {
        slider.value = value
        segmentedControl.selectedSegmentIndex = index
    }
    
}

extension DrawPopoverController {
    
    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        indexChanged?(sender)
    }
    
    @IBAction func valueChanged(_ sender: UISlider) {
        valueChanged?(sender)
    }
    
}
