//
//  FilterCell.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/19/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class FilterCell: UICollectionViewCell {
    @IBOutlet var filterLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                imageView.layer.borderWidth = 4
                imageView.layer.borderColor = #colorLiteral(red: 0.5607843137, green: 0.8117647059, blue: 0.7176470588, alpha: 1)
                filterLabel.textColor = #colorLiteral(red: 0.5607843137, green: 0.8117647059, blue: 0.7176470588, alpha: 1)
            } else {
                imageView.layer.borderWidth = 0
                imageView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                filterLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
        }
    }
}

