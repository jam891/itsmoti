//
//  ColorCell.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 12/20/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import UIKit

class ColorCell: UICollectionViewCell {
    
    @IBOutlet weak var colorView: UIView!
    
    var color: UIColor! {
        didSet { colorView.backgroundColor = color }
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                let previouTransform =  colorView.transform
                UIView.animate(withDuration: 0.2, animations: {
                    self.colorView.transform = self.colorView.transform.scaledBy(x: 1.3, y: 1.3)
                    self.colorView.layer.borderWidth = 3
                }, completion: { _ in
                    UIView.animate(withDuration: 0.2) {
                        self.colorView.transform = previouTransform
                    }
                })
            } else {
                colorView.layer.borderWidth = 1
            }
        }
    }
    
}
