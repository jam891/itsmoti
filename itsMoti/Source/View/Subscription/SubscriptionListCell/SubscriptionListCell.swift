//
//  SubscriptionListCell.swift
//  itsMoti
//
//  Created by Александр Васильченко on 2/7/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class SubscriptionListCell: UITableViewCell {
    @IBOutlet var subscriptionCodeLabel: UILabel!
    @IBOutlet var buyDateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
