//
//  SubscriptionFactory.swift
//  itsMoti
//
//  Created by Александр Васильченко on 2/7/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

struct SubscriptionFactory {
    private init() {}
    
    static func make() -> UIViewController {
        return makeSubscriptionListViewController()
    }
}

private extension SubscriptionFactory {
    static func makeSubscriptionListViewController() -> UIViewController {
        let storyBoard = UIStoryboard(name: "Subscription", bundle: nil)
        
        guard let view = storyBoard.instantiateInitialViewController() as? SubscriptionListController else {
            return UIViewController()
        }
        
        let presenter = SubscriptionListPresenter(view: view)
        view.presenter = presenter
        
        return view
    }
}
