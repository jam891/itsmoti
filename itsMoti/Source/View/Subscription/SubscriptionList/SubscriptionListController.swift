//
//  SubscriptionListController.swift
//  itsMoti
//
//  Created by Александр Васильченко on 2/7/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import UIKit

class SubscriptionListController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var presenter: SubscriptionListPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        tableView.register(UINib(nibName: "SubscriptionListCell", bundle: nil), forCellReuseIdentifier: "SubscriptionListCell")
        // Do any additional setup after loading the view.
    }
    
}

// MARK: - TableViewDelegate

extension SubscriptionListController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

// MARK: - TableViewDelegate

extension SubscriptionListController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let subscriptionListCell = tableView.dequeueReusableCell(withClass: SubscriptionListCell.self, for: indexPath)
        subscriptionListCell.subscriptionCodeLabel?.text = "Code Label: - \(indexPath.row)"
        subscriptionListCell.buyDateLabel?.text = "Date label: - \(indexPath.row)"
//        searchCell
        return subscriptionListCell
    }
}

// MARK: - SubscriptionListView

extension SubscriptionListController: SubscriptionListView {
    
}
