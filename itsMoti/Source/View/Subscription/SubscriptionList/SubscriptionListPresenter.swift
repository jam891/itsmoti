//
//  SubscriptionListPresenter.swift
//  itsMoti
//
//  Created by Александр Васильченко on 2/7/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import Foundation

protocol SubscriptionListViewPresenter {
    init(view: SubscriptionListView)
}

class SubscriptionListPresenter: SubscriptionListViewPresenter {
    unowned let view: SubscriptionListView

    required init(view: SubscriptionListView) {
        self.view = view
    }
}
