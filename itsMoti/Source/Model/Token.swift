//
//  Token.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct Token: Codable {
    var accessToken: String!
}
