//
//  SubscriptionModel.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 2/8/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct SubscriptionModel: Codable {
    var id: String!
    var title: String
    var price: Double
    var description: String
    
    var priceString: String {
        let dollarFormatter = NumberFormatter()
        dollarFormatter.minimumFractionDigits = 2
        dollarFormatter.maximumFractionDigits = 2
        return dollarFormatter.string(from: NSNumber(value: price))!
    }
    
}

