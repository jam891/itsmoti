//
//  Badge.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 2/6/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct Badge {
    var value: Int
}
