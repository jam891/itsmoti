//
//  LocationModel.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct LocationModel: Codable {
    var title: String?
    var subtitle: String?
}
