//
//  MediaModel.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

enum MediaType: String, Codable {
    case video
    case audio
    case voice
    case photo
}

struct MediaModel: Codable {
    var type: MediaType
    var resource: String?
    var thumbnail: String?
    var size: String?
}
