//
//  User.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Locksmith
import DefaultsKit

struct User: Codable {
    var id: String
    var firstName: String?
    var lastName: String?
    var username: String
    var phoneNumber: String?
    var email: String
    var avatar: String?
    var birthday: String?
    var gender: String?
    var location: String?
    var accessToken: String?
}

extension User {
   
    static var current: User! {
        get {
            return Defaults().get(for: .userKey) ?? nil
        }
        set {
            if let user = newValue {
                Defaults().set(user, for: .userKey)
            } else {
                Defaults().clear(.userKey)
            }
        }
    }
    
    static var accessToken: String! {
        get {
            guard let dictionary = Locksmith.loadDataForUserAccount(userAccount: .accessToken) else { return nil }
            return dictionary[.accessToken] as? String
        }
        set {
            if let accessToken = newValue {
                do {
                    try Locksmith.saveData(data: [.accessToken: accessToken], forUserAccount: .accessToken)
                } catch {
                    print("Unable to save data")
                }
            } else {
                do {
                    try Locksmith.deleteDataForUserAccount(userAccount: .accessToken)
                } catch {
                    print("Unable to remove data")
                }
            }
        }
    }
    
    static var deviceToken: String! 
    
    static var hasRunBefore: Bool! {
        get {
            return Defaults().get(for: .runKey) ?? false
        }
        set {
            if let hasRunBefore = newValue {
                Defaults().set(hasRunBefore, for: .runKey)
            }
        }
    }
    
}
