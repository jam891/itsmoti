//
//  Video.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 1/10/19.
//  Copyright © 2019 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct Video {
    var url: URL
    var title: String
}
