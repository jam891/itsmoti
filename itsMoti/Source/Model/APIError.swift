//
//  APIError.swift
//  itsMoti
//
//  Created by Vitaliy Delidov on 9/26/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct APIError: Error, Decodable {
    private var message: String
    var localizedDescription: String {
        return message
    }
}
