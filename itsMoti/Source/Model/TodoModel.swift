//
//  TodoModel.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

struct TodoModel: Codable {
    var name: String
    var completed: Bool
    var createDate: Date
}
