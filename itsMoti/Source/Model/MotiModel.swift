//
//  MotiModel.swift
//  itsMotiDemo
//
//  Created by Vitaliy Delidov on 11/25/18.
//  Copyright © 2018 Vitaliy Delidov. All rights reserved.
//

import Foundation

enum Statement: String {
    case create
    case update
    case delete
}

struct MotiModel: Codable, Equatable {
    var id: String
    var title: String
    var body: String

    var created: Date
    var modified: Date

    var startDate: Date
//    var endDate: Date
    
    var recurrenceRule: String
    var snooze: String
    
    var media: MediaModel?
    var location: LocationModel?
    var todoList: [TodoModel]?
    var owner: User?
}

extension MotiModel {
    static func == (lhs: MotiModel, rhs: MotiModel) -> Bool {
        return lhs.id == rhs.id
    }
}
